<%@page import="com.lawmart.bean.Services"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Extra Details</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="assets/vendor/Registration/jquery-ui/jquery-ui.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="assets/css/Registration/style.css">
    
    
    <script src="assets/js/jquery-3.5.1.min.js"></script>
<script>
<!-- services and there fees and description -->
$(document).ready(function() {
	  //set initial state.

	$('#1').click(function(){
        if($(this).prop("checked") == true){
            <!--alert("Checkbox is checked.");-->
  		  $('.1').removeAttr('disabled');

        }
        else if($(this).prop("checked") == false){
        <!--    alert("Checkbox is unchecked.");-->
  		  $('.1').attr('disabled','disabled');

        }
        
    });
	  
	$('#2').click(function(){
        if($(this).prop("checked") == true){
            <!--alert("Checkbox is checked.");-->
  		  $('.2').removeAttr('disabled');

        }
        else if($(this).prop("checked") == false){
        <!--    alert("Checkbox is unchecked.");-->
  		  $('.2').attr('disabled','disabled');

        }
        
        
    });
	
	$('#3').click(function(){
        if($(this).prop("checked") == true){
            <!--alert("Checkbox is checked.");-->
  		  $('.3').removeAttr('disabled');

        }
        else if($(this).prop("checked") == false){
        <!--    alert("Checkbox is unchecked.");-->
  		  $('.3').attr('disabled','disabled');

        }
        
        
    });
	
});

</script>
    
    
    
</head>
<body>

    <span class="Signup-form-title">
        Extra Details
    </span>    <div class="main">

        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                
                    <form method="POST" id="signup-form" class="signup-form" action="getLFExtraDetails">
               <!--          <div class="form-row">  -->
                            <div class="form-group">
                                <label for="reg_no">Registration Number</label>
                                <input type="text" class="form-input" name="reg_no" id="reg_no" required="required"/>
                            </div>
                     
                             <div class="form-group">
                            <label for="lfdesc">Your firm Description</label>
                            <textarea rows="3" cols="60"  required="required" class="form-input" name="lfdesc" id="lfdesc"></textarea>
                        </div>
                            
                            
                            <div class="form-row">
                                <label for="choose services">Choose Services You Provide</label>
                                <%List<Services> servicesList=(List)request.getAttribute("services"); %>
                            
                            <%for(Services s:servicesList){%>
                                
                                <input type="checkbox" value="<%=s.getServiceId()%>" class="form-input" name="services" id="<%=s.getServiceId()%>" ><%=s.getServiceName() %>
								<%} %>
							                            
                            </div>
                            
                            <div class="form-group">
                            
                             <%for(Services s:servicesList){%>
                            
                            <input type="number" name="sfees" disabled="disabled" class="<%=s.getServiceId()%>" placeholder="<%=s.getServiceName()+" fees"%>">  
								<textarea rows="4" cols="7" name="sdesc" class="<%=s.getServiceId()%>" disabled="disabled" placeholder="<%=s.getServiceName()+" description"%>"></textarea><br>
                            
                            <%} %>

                            </div>
                            
                            <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Submit"/>
                        </div>
                        
                        </div>
                        
                        
                        

                        
                    </form>
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="assets/vendor/Registration/jquery/jquery.min.js"></script>
    <script src="assets/vendor/Registration/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="assets/js/Registrationjs/main.js"></script>
    
    <!-- selecting area -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

	<script type="text/javascript">
    $(document).ready(function () {
     $('select').selectize({
         sortField: 'text'
     });
 });
    </script>
    
    
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>