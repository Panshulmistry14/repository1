<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.LFServices"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head class="no-js">
<meta charset="ISO-8859-1">
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Client-Service</title>

<!-- input type file css -->
<link rel="stylesheet" type="text/css" href="assets/css/component.css" />



<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">


<style>
.b1 {
	width: auto;
	background: #1bbd36;
	color: #fff;
	text-transform: uppercase;
	font-weight: 900;
	padding: 16px 50px;
	border: none;
	margin-top: 37px;
	cursor: pointer;
	text-decoration: none;
}

.b1:hover {
	background: #333333;
	color: #fff;
}

.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>



</head>
<body>
	<%@ include file="ClientHeader.jsp"%>

	<%
		HttpSession httpSession = request.getSession(false);
	User user = (User) httpSession.getAttribute("userisclient");
	%>

	<%if(user==null){ %>
              <%response.sendRedirect("Login.jsp");%>
              <%} else{%>
	

	<%
		String sno = request.getParameter("srno");
	int srno = Integer.parseInt(sno);
	LawMartService ls = new LawMartServiceImpl();
	LFServices lfs = ls.getSingleLFService(srno);
	%>


	<%
		String sID = String.valueOf(lfs.getsId());
	%>
	<%
		Services services = ls.fetchServices(sID);
	%>
	
	
	<br>
	<br>
	<br>
	   

	
	<section class="success"
		style="margin-bottom: 15px; padding: 4px 12px; background-color: #ddffdd; border-left: 6px solid #4CAF50;">
		<p align="center" style="font-size: 18px">
			<strong>Note :- Please read all the information carefully
				before taking the appointment!</strong>
		</p>
	</section>
	
	<%String str=(String)request.getAttribute("error"); %>
	<% if(str!=null)
	{%>
			<div class="alert" align="center">
			  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
			  <strong>Problem!</strong><%=str %> 
			</div>
	<% }%>
	


	<section id="about-us" class="about-us">
		<div class="container" data-aos="fade-up">


			<div class="section-title">

				<h2>
					INSTRUCTION</strong>
				</h2>

				<ul style="list-style: none;" align="left">

					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						Once you upload the PDF file of your required Documents,you need
						to choose Payment option for the service either Online through the
						System or at the time of Appointment at the chosen Firm.</li>
					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						Firm will allot you a time for the Appointment. At that time you
						need to visit the Firm in person for completing the final
						procedure of your Service.</li>

					<%
						if (services.getServiceName().equals("Rent Agreement")) {
					%>

					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						<strong>Documents required for Rent Agreement </strong> <%
 	}
 %> <%
 	if (services.getServiceName().equals("Power of Attorney")) {
 %>
					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						<strong>Documents required for Power of Attorney </strong> <%
 	}
 %> <%
 	if (services.getServiceName().equals("Agreement of Sale")) {
 %>
					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						<strong>Documents required for Agreement of Sale </strong> <%
 	}
 %> <br> <br> Any ID proof.(Aadhar Card is preferred) of both
						the Parties.<br> <br> One Passport size photo of both
						the Parties.<br> <br> Properties Index or Ownership
						proof.<br> <br> Two Witnesses ID Proofs.<br> <br>
						<strong>Payment Structure Explained</strong><br> Rs 300 Stamp
						Duty + Rs 300 Typing Charge + Rs 200 to Rs 400 Notary Charge +
						Legal Charges Charged by the Law Firm.<strong>(All these
							Charges are inclusive in the fees mentioned)</strong> <br></li>




					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						Please submit all the Documents mentioned above correctly in order
						to prevent any future problems.</li>

					<li><i class="ri-check-double-line" style="color: #1bbd36;"></i>
						Once you Submit your request for Appointment you will be able to
						see the status and its related details under the Booking
						Consultations Tab.</li>
				</ul>

				<br>

			</div>


			<br> <br>
			<div class="section-title">



				<h2><%=services.getServiceName()%></strong>
				</h2>

				<p><%=lfs.getsDesc()%></p>
				<br>

				<%
					LawFirm lf = ls.getSingleLF(lfs.getlId());
				%>
				<h5 align="left">
					Service provided by <strong><%=lf.getUserName().toUpperCase()%>
					</strong>
				</h5>
				<br>
				<h5 align="left">
					Fees for the service will be <strong><%=lfs.getsFees()%>
						Rs</strong>
				</h5>
				<br>

				<form action="getAppointmentClientData" method="post"
					enctype="multipart/form-data">

					<div class="container">
						<div class="content">
							<h5 align="left" style="position: fixed;">
								Upload the Documents <strong>PDF File</strong> here
							</h5>
							<h5>
								<input required="required" style="display: none;" type="file"
									name="file" id="file-3" class="inputfile inputfile-3"
									data-multiple-caption="{count} files selected" multiple /> <label
									style="color: #1bbd36;" for="file-3"><svg
										xmlns="http://www.w3.org/2000/svg" width="20" height="17"
										viewBox="0 0 20 17">
										<path
											d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
									<span>Upload a File&hellip;</span><br> <span>(File
										size should not exceed 5MB)</span> </label>
							</h5>
						</div>




						<%
							String cID = String.valueOf(user.getUserId());
						%>
						<%
							Client client = ls.fetchClientDetails(cID);
						%>
						<input type="hidden" readonly="readonly" name="cid"
							value="<%=client.getClientId()%>"> <input type="hidden"
							readonly="readonly" name="srno" value="<%=lfs.getSrNo()%>">
						<input type="hidden" readonly="readonly" name="lfid"
							value="<%=lf.getLfId()%>"> <input type="hidden"
							readonly="readonly" name="bookamt" value="<%=lfs.getsFees()%>">


						<input type="radio" name="payment" checked="checked" value="Pay now">Pay now!
						<input type="radio" name="payment" value="Pay Offline">Pay Offline<br>
						



						<input type="submit" class="b1" value=" Proceed">
				</form>

			
			</div>
			<br> <br> <br>



		</div>
	</section>
	<!-- End About Us Section -->






	<%@ include file="HomePageFooter.jsp"%>
	<script src="assets/js/custom-file-input.js"></script>
	<script>
		(function(e, t, n) {
			var r = e.querySelectorAll("html")[0];
			r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
		})(document, window, 0);
	</script>

<%} %>
</body>
</html>