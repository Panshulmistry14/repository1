<%@page import="com.lawmart.bean.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>LawMart-Law Firm</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  
  <!-- =======================================================
  * Template Name: Company - v2.1.0
  * Template URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

<%@ include file="LawfirmHeader.jsp" %>


  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

      <div class="carousel-inner" role="listbox">

        <!-- Slide 1 -->
        <div class="carousel-item active" style="background-image: url(assets/img/slide/lawfirmHomePage.jpeg);">
          <div class="carousel-container">
            <div class="carousel-content animate__animated animate__fadeInUp">
            <%HttpSession httpSession=request.getSession(false);
              User user =(User)httpSession.getAttribute("userislawfirm"); %>
              <h2 align="center">Welcome <span><%=user.getUserName().toUpperCase()%></span></h2>
             </div>
          </div>
        </div>


  </section><!-- End Hero -->

  <main id="main">

  
   
   
    
  </main><!-- End #main -->
<%@ include file="HomePageFooter.jsp" %>
</body>
</html>