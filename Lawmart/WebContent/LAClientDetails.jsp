<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.bean.Payment"%>
<%@page import="org.eclipse.jdt.internal.compiler.ast.Clinit"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Client Details</title>

<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet" href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">




</head>
<%@ include file="LawfirmHeader.jsp" %>
<%List<LegalAdvice> laList = (List)request.getAttribute("laDetails"); %>
<%LawMartService ls = new LawMartServiceImpl(); %>
<body>

<br><br><br>


<div>

<div class="content mt-3">
<div class="animated fadeIn">
<div class="row">
<div class="col-md-12">
<div class="card">
 

<div class="card-header">
<strong class="card-title">Legal Advice Client Details Table</strong><br>
</div>
<div class="card-body">

<table id="bootstrap-data-table" class="table table-striped table-bordered">
<thead>
<tr>
<th>Client's Name</th>
<th>Phone</th>
<th>Address</th>
<th>Email </th>
<th>Order Id</th>
<th>Bank Name</th>
<th>Taxation Id</th>
<th>Payment Mode</th>
<th>Bank Taxation Id</th>

</tr>
</thead>
<tbody>

<%if(laList!=null) {%>
<%for(LegalAdvice la : laList) {%>
<tr>

<% User user = ls.getUser(la.getUser_id());%>


<%Payment p = ls.getPaymentDetails(la.getOrderId());%>


<% if(user.getUserName()!=null) {%>
<td><%=user.getUserName() %></td>
<%} else { %>
<td>Client Name Not Found!</td>
<%} %>

<% if(user.getUserPhone()!=null) {%>
<td><%=user.getUserPhone() %></td>
<%} else { %>
<td>Client Phone Number Not Found!</td>
<%} %>

<% if(user.getUserAddress()!=null) {%>
<td><%=user.getUserAddress() %></td>
<%} else { %>
<td>Client Address Not Found!</td>
<%} %>

<% if(user.getUserEmail()!=null) {%>
<td><%=user.getUserEmail() %></td>
<%} else { %>
<td>Client Email Not Found!</td>
<%} %>

<% if(la.getOrderId()!=null) {%>
<td><%=la.getOrderId() %></td>
<%} else { %>
<td>Order Id is not registered Yet!</td>
<%} %>

<% if(p.getBankName()!=null) {%>
<td><%=p.getBankName() %></td>
<%} else { %>
<td>Bank Name Not Found!</td>
<%} %>

<% if(p.getTxnId()!=null) {%>
<td><%=p.getTxnId() %></td>
<%} else { %>
<td>Taxation Id Not Found!</td>
<%} %>

<% if(p.getPayMode()!=null) {%>
	<%if(p.getPayMode().equals("NB")) {%>
<td>Net Banking</td>
<%} %>
	<%if(p.getPayMode().equals("CC")) {%>
	<td>Credit Card</td>
	<%} %>
	<%if(p.getPayMode().equals("DC")) {%>
	<td>Debit Card</td>
	<%} %>
<%} else {%>
<td>Pay mode not Found!</td>
<%} %>

<% if(p.getBankId()!=null) {%>
<td><%=p.getBankId() %></td>
<%} else { %>
<td>Bank Id Not Found!</td>
<%} %>

</tr>
<%} %>
<%} %>

</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script src="assets/css/table/datatables.min.js"></script>
<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
<script src="assets/css/table/datatables-init.js"></script>

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>






</body>
</html>