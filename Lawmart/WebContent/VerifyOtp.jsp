<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Forgot Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="assets/img/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
<!--===============================================================================================-->

<script src="assets/js/jquery-3.5.1.min.js"></script>


</head>
<body>

	<%LawMartService ls=new LawMartServiceImpl(); %>
	<%String otp=(String)request.getAttribute("otp");%>
	<%String email=(String)request.getAttribute("email");%>
	
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form p-l-55 p-r-55 p-t-178" method="get" action="SendOtp">
					<span class="login100-form-title">
						Verify OTP
					</span>
					
					
					<div class="flex-col-c p-t-2 p-b-4">
						<span class="txt1 p-b-9">
							Please enter the OTP sent to your verified email address .
						</span>
						</div>
					
					
						<%String msg=(String)request.getAttribute("msg"); %>
					
					<%if(msg!=null){ %>
					
				  		<div class="flex-col-c p-t-20 p-b-30">
							<span class="txt1 p-b-9" id="msg" style="color: red;">
							<%=msg %>
							</span>
						</div>	
					<%} %>  

					
					<div class="wrap-input100 validate-input m-b-16" data-validate="Please enter OTP">
						<input class="input100" type="text" name="checkotp" placeholder="Please enter OTP">
						<input type="hidden" name="email" value="<%=email%>">
						
						<%String encryptOtp=ls.encrypt(otp);%>
						<input type="hidden" name="originalotp" value="<%=encryptOtp%>">
						
						<span class="focus-input100"></span>
					</div>
					<br><br>
					



					<div class="container-login100-form-btn">
						<button class="login100-form-btn" >
							Verify OTP
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<script src="assets/vendor/Login/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/bootstrap/js/popper.js"></script>
	<script src="assets/vendor/Login/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/daterangepicker/moment.min.js"></script>
	<script src="assets/vendor/Login/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="assets/js/loginjs/main.js"></script>

</body>
</html>