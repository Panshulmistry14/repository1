<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.Area"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update LawFirm</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="assets/vendor/Registration/jquery-ui/jquery-ui.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="assets/css/Registration/style2.css"> 
 
 <style type="text/css">
 
 .cancel{
 
  width: auto;
  background: gray;
  color: #fff;
  text-transform: uppercase;
  font-weight: 900;
  padding: 16px 50px;
  float: right;
  border: none;
  margin-top: 37px;
  cursor: pointer;
 }
 
.cancel:hover{
color: white;
} 
 
 .form-submit:hover{
 
 background: #57b846;
 
 
 }
 
 </style>
   
      
</head>
<body>
<%@ include file="Servicesheader.jsp" %>

<br><br><br><br>
    <h4 class="Signup-form-title" style="position: absolute;left: 45%">
        UPDATE LAW FIRM
    </h4>  
       <div class="main">
		<%LawFirm lawFirm = (LawFirm)request.getAttribute("lawfirm"); %>
        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container11">
                <div class="signup-content">
                    <form method="POST" action="AdminEditData?Page2=lawfirm" id="signup-form" class="signup-form">
                        <!--  <div class="form-row"> -->
                            <div class="form-group">
                            
                            <!--<label for="lawfirm_id">Lawfirm ID</label>-->
                                <input type="hidden" class="form-input" name="lawfirm_id" id="lawfirm_id" value="<%=lawFirm.getUserId()%>"/>
                        
                                <label for="firm_name">Firm name</label>
                                <input type="text" class="form-input" name="firm_name" id="firm_name" required="required" value="<%=lawFirm.getUserName()%>"/>
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Phone number</label>
                                <input type="number" class="form-input" name="phone_number" id="phone_number" value="<%=lawFirm.getUserPhone()%>"/>
                            </div>
                        <div class="form-row">
<!--                             <div class="form-group"> -->
<!--                                 <label for="password">Password</label> -->
<!--                             </div> -->
<!--                             <div class="form-group"> -->
<!--                                 <label for="re_password">Confirm your password</label> -->
<%--                                 <input type="password" class="form-input" name="re_password" id="re_password" value="<%=lawFirm.getUserPassword()%>"/> --%>
<!--                             </div> -->
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" readonly="readonly" class="form-input" name="email" id="email" required="required" value="<%=lawFirm.getUserEmail()%>"/>
                        </div>

                        <div class="form-group">
                            <label for="Address">Address</label>
                            <textarea rows="3" cols="60"  required="required" class="form-input" name="address" id="address"><%=lawFirm.getUserAddress()%></textarea>
                        </div>
                          
                           <div class="form-group">
                                <label for="reg_no">Registration Number</label>
                                <input type="text" class="form-input" name="reg_no" id="reg_no" required="required" value="<%=lawFirm.getLfregNo()%>"/>
                            </div>
                     
                             <div class="form-group">
                            <label for="lfdesc">Your firm Description</label>
                            <textarea rows="3" cols="60"  required="required" class="form-input" name="lfdesc" id="lfdesc"><%=lawFirm.getLfDesc()%></textarea>
                        </div>
							
							<div class="form-group">
						 <label for="Area">Area</label>
					
							<%List<Area> areaList=(List)request.getAttribute("areas"); %>
							
							<select class="form-control" id="select-area" name="area" required="required">
 								
								<%int i=1; %>
								<%for(Area a: areaList){ %>
									<%if(lawFirm.getAreaId()==i)
									{
									%>
										<option selected="selected" value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
									<%}%>
									<option value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
 								<%i++; %> 
  								<%} %>
						  		</select>
							
							
							
						</div>
    				                     
                        <div class="form-group">
                         	<a href="AdminManage?Page1=lawfirm" class="cancel" style="position: absolute;right: 30%">Cancel</a>
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="assets/vendor/Registration/jquery/jquery.min.js"></script>
    <script src="assets/vendor/Registration/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="assets/js/Registrationjs/main.js"></script>
    
    <!-- selecting area -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

	<script type="text/javascript">
    $(document).ready(function () {
     $('select').selectize({
         sortField: 'text'
     });
 });
    </script>
    <%@ include file="HomePageFooter.jsp" %>    
    
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>