<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.LFServices"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.BookingConsultation"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin-Booking Table</title>

<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet"
	href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">


<style>
</style>




</head>
<body>

	<%
		HttpSession httpSession = request.getSession(false);
	User user = (User) httpSession.getAttribute("userisadmin");
	%>

	<%
		if (user == null) {
	%>
	<%
		response.sendRedirect("Login.jsp");
	%>
	<%
		} else {
	%>


	<%@include file="Servicesheader.jsp"%>


	<br>
	<br>
	<br>


	<div>

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">


							<div class="card-header">
								<strong class="card-title">Booking Consultations Table</strong>
							</div>
							<div class="card-body">
								<table id="bootstrap-data-table"
									class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Client Name</th>
											<th>Service Name</th>
											<th>LawFirm Name</th>

											<th>Fees</th>
											<th>Booking Date</th>
											<th>Booking Status</th>
											<th>Payment Status</th>
											<th>Order ID</th>
											<th>Appointment Response</th>

										</tr>
									</thead>

									<%
										List<BookingConsultation> bookList = (List) request.getAttribute("book");
									%>
									<%
										LawMartService ls = new LawMartServiceImpl();
									%>

									<tbody>

										<%
											if (bookList != null) {
										%>
										<%
											for (BookingConsultation bc : bookList) {
										%>
										<tr>

											<%
												Client client = ls.getSingleClient(bc.getClientId());
											%>

											<%
												if (client.getUserName() != null) {
											%>
											<td><%=client.getUserName()%></td>
											<%
												} else {
											%>
											<td>Client name is not Available!</td>
											<%
												}
											%>

											<%
												LFServices lfs = ls.getSingleLFService(bc.getSrNo());
											Services services = ls.fetchServices(String.valueOf(lfs.getsId()));
											%>

											<%
												if (services.getServiceName() != null) {
											%>
											<td><%=services.getServiceName()%></td>
											<%
												} else {
											%>
											<td>Service name is not Available!</td>
											<%
												}
											%>


											<%
												LawFirm lf = ls.getSingleLF(bc.getLfId());
											%>

											<%
												if (lf.getUserName() != null) {
											%>
											<td><%=lf.getUserName()%></td>
											<%
												} else {
											%>

											<td>Firm's name not found!</td>
											<%
												}
											%>


											<%
												if (bc.getBookAmt() != 0) {
											%>
											<td><%=bc.getBookAmt()%></td>
											<%
												} else {
											%>
											<td>Amount not Available!</td>
											<%
												}
											%>



											<%
												if (bc.getBookDate() != null) {
											%>
											<td><%=bc.getBookDate()%></td>
											<%
												} else {
											%>
											<td>Date is not Available!</td>
											<%
												}
											%>


											<%
												if (bc.getBookStatus() != null) {
											%>
											<%
												if (bc.getBookStatus().equals("success")) {
											%>
											<td style="color: #1bbd36"><%=bc.getBookStatus()%></td>
											<%
												} else {
											%>
											<td style="color: red;"><%=bc.getBookStatus()%></td>

											<%
												}
											%>
											<%
												} else {
											%>
											<td>Status not Available!</td>
											<%
												}
											%>


											<%
												if (bc.getPayStatus() != null) {
											%>
											<%
												if (bc.getPayStatus().equals("pending")) {
											%>
											<td style="color: red;" ss><%=bc.getPayStatus()%></td>

											<%
												} else if (bc.getPayStatus().equals("at Firm")) {
											%>
											<td style="color: orange;" ss><%=bc.getPayStatus()%></td>

											<%
												} else if (bc.getPayStatus().equals("success")) {
											%>
											<td style="color: #1bbd36;" ss><%=bc.getPayStatus()%></td>
											<%
												} else if (bc.getPayStatus().equals("failure")) {
											%>

											<td style="color: red;" ss><%=bc.getPayStatus()%></td>

											<%
												}
											%>



											<%
												} else {
											%>
											<td>Status not Available!</td>
											<%
												}
											%>

											<%
												if (bc.getOrderId() != null && bc.getPayStatus().equals("success")) {
											%>

											<td><strong><%=bc.getOrderId()%></strong></td>

											<%
												} else {
											%>

											<td>Order Id is not registered Yet!</td>

											<%
												}
											%>


											<%
												if (bc.getBookAppointment() != null) {
											%>
											<td><strong><%=bc.getBookAppointment()%></strong></td>
											<%
												} else {
											%>
											<td>LawFirm has not responded yet!</td>
											<%
												}
											%>
										</tr>
										<%
											}
										%>
										<%
											}
										%>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>





	<script src="assets/css/table/datatables.min.js"></script>
	<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
	<script src="assets/css/table/datatables-init.js"></script>

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>


	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>


	<%
		}
	%>
</body>
</html>