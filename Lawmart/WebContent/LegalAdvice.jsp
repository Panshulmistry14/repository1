<%@page import="com.lawmart.bean.User"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Legal Advice</title>


<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">

<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/contact/bootstrap.min.css">

<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="assets/css/contact/util.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/contact/main.css">
<!--===============================================================================================-->



<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">

<style type="text/css">
.button11 {
	min-width: 193px;
	height: 50px;
	border-radius: 25px;
	background: #57b846;
	font-family: Montserrat-Bold;
	font-size: 15px;
	line-height: 1.5;
	color: #fff;
	display: -webkit-box;
	display: -webkit-flex;
	display: -moz-box;
	display: -ms-flexbox;
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 0 25px;
	-webkit-transition: all 0.4s;
	-o-transition: all 0.4s;
	-moz-transition: all 0.4s;
	transition: all 0.4s;
}

.button11:hover {
	background: #333333;
	color: #fff;
}
</style>


</head>
<body>
	<%
		HttpSession httpSession = request.getSession(false);
	User user = (User) httpSession.getAttribute("userisclient");
	%>

	<%
		if (user == null) {
	%>
	<%
		response.sendRedirect("Login.jsp");
	%>
	<%
		} else {
	%>


	<%@ include file="ClientHeader.jsp"%>


	<%
		LawMartService ls = new LawMartServiceImpl();
	List<LawFirm> lawFirmList = new ArrayList<LawFirm>();
	lawFirmList = ls.getLawfirms();
	%>

	<div class="contact1" style="background-color: white;">
		<div class="container-contact1">
			<div class="contact1-pic js-tilt" data-tilt>
				<img src="assets/img/legaladvicelogo.png" alt="IMG">


			</div>

			<form class="contact1-form validate-form" action="LAInsert"
				method="post">
				<span class="contact1-form-title"> LEGAL ADVICE </span>




				<%--	<%String msg=(String)request.getAttribute("msg"); %>
							<span id="confirmation" class="contact1-form-title" style="color: black; background-color:transparent ;">	
   								<%if(msg!=null){%>
								<%=msg%>							
							<%}%>
						</span> --%>



				<!-- 	<div class="wrap-input1 validate-input" data-validate = "Name is required">
					<input class="input1" type="text" name="name" placeholder="Name">
					<span class="shadow-input1"></span>
				</div>

				<div class="wrap-input1 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<input class="input1" type="text" name="email" placeholder="Email">
					<span class="shadow-input1"></span>
				</div>

				<div class="wrap-input1 validate-input" data-validate = "Subject is required">
					<input class="input1" type="text" name="subject" placeholder="Subject">
					<span class="shadow-input1"></span>
				</div>   
				 -->

				<!-- <span class="contact1-form-title" style="font-size: 20px;">
					Select the lawfirm from the list
				</span>  -->


				<%
					System.out.println("legalAdvice userID:" + user.getUserId());
				%>



				<!--<label>User Id</label>-->
				<input type="hidden" class="form-input" name="user_id" id="user_id"
					value="<%=user.getUserId()%>" /> 
			<!-- 		<span class="contact1-form-title"
					style="font-size: 18px; float: left; position: absolute; top: 201px;">
					 Select the LawFirm </span>   -->

				<%
					String l = request.getParameter("selfirm");
				%>
				<%
					if (l != null) {
					int lID = Integer.parseInt(l);
				%>

				<select class="wrap-input1 validate-input" required="required"
					id="select-lawfirm" name="lawfirmselction"
					style="background-color: #E8E8E8; border: none; height: 40px; border-radius: 30px">
					<!-- 	<option value=""> </option>   -->
					<%
						int i = 1;
					%>
					<%
						for (LawFirm a : lawFirmList) {
					%>
					<%
						if (a.getLfId() == lID) {
					%>
					<option class="contact1-form-title" selected="selected"
						value="<%=a.getLfId()%>" style="font-size: 15px"><%=a.getUserName()%></option>

					<%
						}
					%>


					<%
						i++;
					%>
					<%
						}
					%>
				</select>

				<%
					} else {
				%>
				<select class="wrap-input1 validate-input" required="required"
					id="select-lawfirm" name="lawfirmselction"
					style="background-color: #E8E8E8; border: none; height: 40px; border-radius: 30px">
					<!-- 	<option value=""> </option>   -->
					<%
						int i = 1;
					%>
					<%
						for (LawFirm a : lawFirmList) {
					%> if()
					<option class="contact1-form-title" value="<%=a.getLfId()%>"
						style="font-size: 15px"><%=a.getUserName()%></option>
					<%
						i++;
					%>
					<%
						}
					%>
				</select>
				<%
					}
				%>



				<div class="wrap-input1 validate-input"
					data-validate="issue is required">
					<textarea class="input1" name="issue" placeholder="Your issue"
						required="required"></textarea>
					<span class="shadow-input1"></span>
				</div>



				<div class="container-contact1-form-btn">
					<input type="submit" class="contact1-form-btn"
						value="Submit the request">
			
				</div>
				<br>
				<br>
				<br>
				<br>
			</form>
			<%
				String status = (String) request.getAttribute("lastatus");
			%>

			<%
				if (status != null) {
			%>
			<span
				style="background-color: #4CAF50; color: white; padding: 8px; font-family: Arial; align-self: center;">
				<%=status%>
			</span>
			<%
				}
			%>


			<div class="contact1-form-title"
				style="font-size: 22px; margin-bottom: 15px; padding: 4px 12px; background-color: #ddffdd; border-left: 6px solid #4CAF50;"">
				Note:-You need to choose the LawFirm from whom you want to get the
				Legal Advice .Then in short time the firm will respond to you
				regarding your issue and they will also mention there fees at the
				time of response.Then you can whether accept or reject the proposal
				accordingly.</div>
		</div>
		<a style="position: absolute; left: 60.5%; top: 61.5%;"
			href="ClientLAData" class="button11"> View Response</a>
		<!--<a style="position: absolute;left: 35%;top:67%;"href="#" class="button11"> View/Provide Feedback</a>  -->



	</div>

	<%@ include file="HomePageFooter.jsp"%>

	<%
		}
	%>

</body>

<!--===============================================================================================-->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
<script src="assets/vendor/bootstrap/js/popper.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/vendor/bootstrap/js/tilt.jquery.min.js"></script>
<script>
	$('.js-tilt').tilt({
		scale : 1.1
	})
</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
 -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"
	integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg="
	crossorigin="anonymous" />


<script type="text/javascript">
	$(document).ready(function() {
		$('select').selectize({
			sortField : 'text'
		});
	});
</script>

<script>
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'UA-23581568-13');
</script>

<!--===============================================================================================-->
<script src="assets/js/contact/main.js"></script>

</html>