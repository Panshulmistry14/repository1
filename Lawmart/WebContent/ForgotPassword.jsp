<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Forgot Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="assets/img/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/Login/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
<!--===============================================================================================-->

<script src="assets/js/jquery-3.5.1.min.js"></script>

<script>
	$(document).ready(function(){
		
		$(".login100-form-btn").attr('disabled','disabled');
		$("input[type=email]").mouseleave(function (){
			$("#msg").empty();
			var str=$(".input100").val();
			if(str.length==0)
				{
			
				}
			else
				{
					$.get( "CheckEmail", { email: str } )
					  .done(function( data ) {
					  if(data=='true')
						  {
							$(".login100-form-btn").removeAttr('disabled','disabled');
						  	$("#msg").empty();

						  }
					  else
						  {
						  	$(".login100-form-btn").attr('disabled','disabled');
						  	$("#msg").append("Email id does not exists");
						  	
						  }
					  });
				}
		});
	});
</script>	


</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form p-l-55 p-r-55 p-t-178" method="post" action="SendOtp">
					<span class="login100-form-title">
						Forgot Password?
					</span>
					
					
					<div class="flex-col-c p-t-2 p-b-4">
						<span class="txt1 p-b-9">
							Please enter the registered email id .
						</span>
						</div>
					
				 	<div class="flex-col-c p-t-20 p-b-30">
						<span class="txt1 p-b-9" id="msg" style="color: red;">
						</span>
						</div>	 

					<div class="wrap-input100 validate-input m-b-16" data-validate="Please enter username">
						<input class="input100" type="email" name="email" placeholder="email">
						<span class="focus-input100"></span>
					</div>

				<!-- 	<div class="wrap-input100 validate-input" data-validate = "Please enter password">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
					</div>  -->

				<!-- 	<div class="text-right p-t-13 p-b-23">
						<span class="txt1">
							Forgot
						</span>

						<a href="#" class="txt2">
							Username / Password?
						</a>
					</div>  -->

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" >
							Send OTP
						</button>
					</div>
					
					
					

				<!-- 	<div class="flex-col-c p-t-170 p-b-40">
						<span class="txt1 p-b-9">
							Don't have an account?
						</span>

						<a href="GetAreas?Page1=client" class="txt1 p-b-9">
							Sign up now as client
						</a>
						
						
						<a href="GetAreas?Page2=lawfirm" class="txt1 p-b-9">
							Sign up now as law firm
						</a>
					</div>  -->
				</form>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<script src="assets/vendor/Login/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/bootstrap/js/popper.js"></script>
	<script src="assets/vendor/Login/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/daterangepicker/moment.min.js"></script>
	<script src="assets/vendor/Login/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/Login/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="assets/js/loginjs/main.js"></script>

</body>
</html>