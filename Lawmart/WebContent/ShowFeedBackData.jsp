<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.LFServices"%>
<%@page import="com.lawmart.bean.BookingConsultation"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.Feedback"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>Feedback</title>
</head>
<body>
	<br>
	<br>
	<br>

	<%
		String fbUser = request.getParameter("fbuser");
	%>

<br>
	<div class="section-title">
		<h2>
			<strong> FEEDBACK</strong>



		</h2>

		<%
			if (fbUser.equals("client")) {
		%>

		<p>The Feedback you give appears here.</p>

		<%
			} else if (fbUser.equals("lawfirm")) {
		%>

		<p>The Feedback for your Firm appears here.</p>

		<%
			} else if (fbUser.equals("visitor")) {
		%>

		<p>The Feedback of Clients for the Firms appears here.</p>

		<%
			}
		%>
	</div>



	<%--<h1><%=fbUser%></h1>   --%>


	<%
		if (fbUser.equals("client")) {
	%>

	<%@ include file="ClientHeader.jsp"%>

	<%
		} else if (fbUser.equals("lawfirm")) {
	%>

	<%@ include file="LawfirmHeader.jsp"%>

	<%
		} else if (fbUser.equals("visitor")) {
	%>

	<%@ include file="HomePageHeader.jsp"%>

	<%
		}
	%>


	<%
		List<Feedback> fbListLA = (List) request.getAttribute("laclientfb");
	List<Feedback> fbListBC = (List) request.getAttribute("laclientbc");
	%>




	<%
		int bcFlag = 0;
	int laFlag = 0;
	int flag = 0;
	%>

	<!-- ======= Feedback Section ======= -->
	<section id="blog" class="blog">
		<div class="container">

			<div class="row">

				<div class="col-lg-12 entries">

					<%
						if (fbUser.equals("client") || fbUser.equals("lawfirm")) {
					%>

					<%
						if (fbListLA != null) {

						for (Feedback f : fbListLA) {
					%>

					<%
						laFlag = 1;
					%>


					<article class="entry" data-aos="fade-up"
						style="align-items: center;">

						<%
							LawMartService ls = new LawMartServiceImpl();
						LegalAdvice la = ls.getLA(f.getsId());

						String userId = String.valueOf(la.getUser_id());
						Client client = ls.fetchClientDetails(userId);

						LawFirm lf = ls.getSingleLF(la.getLf_id());
						%>


						<div class="entry-meta">
							<ul>
								<li class="d-flex align-items-center"><i
									class="icofont-user"></i><%=client.getUserName()%></li>
								<li class="d-flex align-items-center"><i class="bx bx-arch"></i><%=lf.getUserName()%></li>
								<li class="d-flex align-items-center"><i
									class="icofont-wall-clock"></i> <time><%=f.getfDate()%></time></li>

							</ul>
						</div>

						<div class="entry-content">
							<h4>LEGAL ADVICE</h4>


							<%
								if (f.getFrate().equals("very good") || f.getFrate().equals("good")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: #1bbd36;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>

							<%
								} else if (f.getFrate().equals("mediocre")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: orange;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>

							<%
								} else if (f.getFrate().equals("very bad") || f.getFrate().equals("bad")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: red;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>


							<%
								}
							%>


							<p><%=f.getFeedback()%>
							</p>
							
						</div>

					</article>
					<!-- End feedback entry -->

					<%
						}
					%>
					<%
						} else {

					laFlag = 1;
					}
					%>



					<%
						if (fbListBC != null) {

						System.out.println("feedback .... ");

						for (Feedback f : fbListBC) {
					%>

					<%
						bcFlag = 1;
					%>

					<%
						System.out.println("feedback" + f.getFeedback());
					%>



					<article class="entry" data-aos="fade-up"
						style="align-items: center;">


						<%
							LawMartService ls = new LawMartServiceImpl();

						BookingConsultation bc = ls.getBookDetails(f.getsId());

						Client client = ls.getSingleClient(bc.getClientId());
						LawFirm lf = ls.getSingleLF(bc.getLfId());
						%>


						<div class="entry-meta">
							<ul>
								<li class="d-flex align-items-center"><i
									class="icofont-user"></i><%=client.getUserName()%></li>
								<li class="d-flex align-items-center"><i class="bx bx-arch"></i><%=lf.getUserName()%></li>
								<li class="d-flex align-items-center"><i
									class="icofont-wall-clock"></i> <time datetime="2020-01-01"><%=f.getfDate()%></time></li>
							</ul>
						</div>

						<div class="entry-content">
							<%
								LFServices lfs = ls.getSingleLFService(bc.getSrNo());
							%>

							<%
								Services services = ls.fetchServices(String.valueOf(lfs.getsId()));
							%>

							<h4><%=services.getServiceName()%></h4>

							<%
								if (f.getFrate().equals("very good") || f.getFrate().equals("good")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: #1bbd36;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>

							<%
								} else if (f.getFrate().equals("mediocre")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: orange;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>

							<%
								} else if (f.getFrate().equals("very bad") || f.getFrate().equals("bad")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: red;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>


							<%
								}
							%>

							<p><%=f.getFeedback()%>
							</p>
							
						</div>

					</article>
					<!-- End feedback entry -->

					<%
						}
					%>
					<%
						} else {
					%>

					<h1>in else</h1>

					<%
						bcFlag = 1;
					System.out.println("in bc if");
					%>
					<%
						}
					%>

					<%
						if (bcFlag == 0 && laFlag == 0) {
					%>


					<img alt="No Content Found" src="assets/img/NoContent.png"
						height="400px" width="550px"
						style="position: relative; left: 30%;">
					<%
						}
					%>


					<%
						System.out.println("bcflag " + bcFlag + " laflag " + laFlag);
					%>




					<%
						} else if (fbUser.equals("visitor")) {

					List<Feedback> fbList = (List) request.getAttribute("fbvisitor");
					%>

					<%
						for (Feedback f : fbList) {

						flag = 1;

						Client client = null;
						LawFirm lf = null;
						BookingConsultation bc = null;
						LawMartService ls = null;
					%>

					<%
						if (f.getsType().equals("la")) {
					%>

					<%
						ls = new LawMartServiceImpl();
					LegalAdvice la = ls.getLA(f.getsId());

					String userId = String.valueOf(la.getUser_id());
					client = ls.fetchClientDetails(userId);

					lf = ls.getSingleLF(la.getLf_id());
					%>


					<%
						} else if (f.getsType().equals("bc")) {
					%>



					<%
						ls = new LawMartServiceImpl();

					bc = ls.getBookDetails(f.getsId());

					client = ls.getSingleClient(bc.getClientId());
					lf = ls.getSingleLF(bc.getLfId());
					%>



					<%
						}
					%>



					<article class="entry" data-aos="fade-up"
						style="align-items: center;">


						<div class="entry-meta">
							<ul>
								<li class="d-flex align-items-center"><i
									class="icofont-user"></i> <%=client.getUserName()%></li>
								<li class="d-flex align-items-center"><i class="bx bx-arch"></i><%=lf.getUserName()%></li>
								<li class="d-flex align-items-center"><i
									class="icofont-wall-clock"></i> <time datetime="2020-01-01"><%=f.getfDate()%></time></li>

							</ul>
						</div>

						<div class="entry-content">

							<%
								if (f.getsType().equals("bc")) {
							%>
							<%
								LFServices lfs = ls.getSingleLFService(bc.getSrNo());
							%>

							<%
								Services services = ls.fetchServices(String.valueOf(lfs.getsId()));
							%>

							<h4><%=services.getServiceName()%></h4>

							<%
								} else if (f.getsType().equals("la")) {
							%>

							<h4>Legal Advice</h4>

							<%
								}
							%>

							<%
								if (f.getFrate().equals("very good") || f.getFrate().equals("good")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: #1bbd36;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>

							<%
								} else if (f.getFrate().equals("mediocre")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: orange;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>

							<%
								} else if (f.getFrate().equals("very bad") || f.getFrate().equals("bad")) {
							%>

							<h5>
								<strong>Rating:</strong>
								<h5 style="color: red;"><%=f.getFrate().toUpperCase()%></h5>
							</h5>
							<br>


							<%
								}
							%>

							<p><%=f.getFeedback()%>
							</p>

					<!-- 		<button type="button" class="btn btn-secondary mb-1"
								data-toggle="modal" data-target="#staticModal">Static</button>


							<div class="modal fade" id="staticModal" tabindex="-1" 
								role="dialog" aria-labelledby="staticModalLabel"
								aria-hidden="true" style="  z-index: 9999 ;" >
								<div class="modal-dialog modal-sm" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="staticModalLabel">Static
												Modal</h5>
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<p>This is a static modal, backdrop click will not close
												it.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary"
												data-dismiss="modal">Cancel</button>
											<button type="button" class="btn btn-primary">Confirm</button>
										</div>
									</div>
								</div>
							</div>    -->




						</div>

					</article>
					<!-- End feedback entry -->




					<%
						}
					%>
					<!-- End feedback entry -->


					<%
						if (flag == 0) {
					%>

					<img alt="No Content Found" src="assets/img/NoContent.png">

					<%
						}
					%>

					<%
						}
					%>
				</div>
				<!-- End blog entries list -->



				<!-- </div>End sidebar tags -->

			</div>
			<!-- End sidebar -->

		</div>
		<!-- End blog sidebar -->

		</div>

		</div>
	</section>
	<!-- End Blog Section -->





	<%@ include file="HomePageFooter.jsp"%>

</body>
</html>