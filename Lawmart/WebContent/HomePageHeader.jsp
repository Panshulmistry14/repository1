<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
<link
	href="assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.eot"
	rel="stylesheet">
<link href="assets/fonts/font-awesome-4.7.0/css/font-awesome.css"
	rel="stylesheet">


<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">




<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center">

		<h1 class="logo mr-auto">
			<a href="index.jsp"><span>Law</span>Mart</a>
		</h1>
		<!-- Uncomment below if you prefer to use an image logo -->
		<!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="Logo of LawMart" class="img-fluid" height="50px" width="50px"></a> -->

		<nav class="nav-menu d-none d-lg-block">
			<ul>
				<li class="active"><a href="index.jsp">Home</a></li>

				<li class="drop-down"><a href="">Sign up</a>
					<ul>
						<li><a href="GetAreas?Page1=client">Sign up as Client</a></li>
						<li><a href="GetAreas?Page2=lawfirm">Sign up as Law Firm</a></li>
					</ul></li>

				<li><a href="Login.jsp">Sign In</a></li>



				<li><a href="GeneralServices.jsp">Services</a></li>
				<li><a href="HomePageLawFirms.jsp">Law Firms</a></li>
				<li><a href="SelectFeedbackData?fbuser=visitor">Feedback</a></li>
				<li><a href="Contact.jsp">Contact</a></li>
				<li><a href="AboutUs.jsp?abtuser=visitor">About Us</a></li>


			</ul>
		</nav>
		<!-- .nav-menu -->



	</div>
</header>
<!-- End Header -->