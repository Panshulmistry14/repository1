<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.Area"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <title>Law Firm Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">



  <style>

  input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

  </style>

 <!--  <link href="assets/profilecss/bootstrap2.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="assets/profilecss/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
   -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

 <script src="assets/profilejs/bootstrap2.min.js"></script>
  <script src="assets/profilejs/jquery2.min.js"></script>
   <script src="assets/profilejs/bootstrap.min.js"></script>
 <script src="assets/profilejs/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



	<style type="text/css">
	
	.h{
	
	font-size: 20px;
	
	
	}
	
	
	
	</style>

 </head>



<body style="background-color: #f7f7f7;">
 <%LawFirm lawFirm=(LawFirm)request.getAttribute("lawfirm"); %>   
  <%@ include file="LawfirmHeader.jsp" %>  
  <%LawMartService ls = new LawMartServiceImpl(); %>
  <%int lfId = lawFirm.getLfId(); %>
  <%System.out.println("The lawfirm id for profile is:"+lfId); %>
  <%String la = ls.countLawfirmLA(lfId); %>
  <%String cases = ls.countLawfirmCases(lfId); %>
  <%int totcases=Integer.parseInt(la)+Integer.parseInt(cases); %>
  <br>
<br>
<br>
<br>
<br>
<br>
<!-- Feedback Lawfirm BC and LA -->
<%String fbc = ls.countLawfirmFeedbackBC(lfId); %>
<%String fla = ls.countLawfirmFeedbackLA(lfId); %>
<%int totfb = Integer.parseInt(fbc) + Integer.parseInt(fla);%>
<div class="container bootstrap snippet">

    <div class="row">
  		<div class="col-sm-10"><h1 style="font-size: 30px;"><%=lawFirm.getUserName() %></h1></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
          
          
          <ul class="list-group">
            <li class="list-group-item text-muted" style="font-size: 20px;">Activity <i class="fa fa-dashboard fa-1x"></i></li>
            <%if(totcases!=0) { %>
             <li class="list-group-item text-right"><span class="pull-left"><strong>Total Clients</strong></span><%=totcases %></li>
            <%} else { %>
			 <li class="list-group-item text-right"><span class="pull-left"><strong>Total Clients</strong></span>0</li>            
            <%} %>
            
            <%if(la!=null) { %>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Legal Advices Given</strong></span> <%=la %></li>
            <%} else { %>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Legal Advices Given</strong></span>0</li>
            <%} %>
            
            <%if(totfb!=0) { %>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Feedbacks Received</strong></span> <%=totfb %></li>
          	<%} else { %>
          	<li class="list-group-item text-right"><span class="pull-left"><strong>Feedbacks Received</strong></span>0</li>
          	<%} %>
          </ul> 
               
         
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><h2 style="font-size: 30px;">Edit Profile</h2></li>
            </ul> 

              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                  <form class="form" action="EditStakeholder?Page1=lawfirm" method="post" id="registrationForm">
                      <div class="form-group">
                        
                           <!--<label for="lawfirm_id">Lawfirm ID</label>-->
                        
                          
                          <div class="col-xs-6">
                              <label for="firm_name"><h4 class="h">Firm name</h4></label>
                              <input type="text" style="font-size: 20px;" class="form-control" name="firm_name"  id="firm_name" placeholder="first name" required="required" value="<%=lawFirm.getUserName()%>">
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4 style="font-size: 20px;">Phone</h4></label>
                              <input style="font-size: 20px;" type="number" class="form-control" name="phone" id="phone" placeholder="enter phone" required="required" value="<%=lawFirm.getUserPhone()%>">
                          </div>
                      </div>
                      
                      
                       <div class="form-group">
                          
                        <div class="col-xs-6">
                          <label for="email"><h4 style="font-size: 20px;">Email</h4></label>
                          <input type="email" style="font-size: 20px;" class="form-control" name="email" id="email" placeholder="you@email.com" required="required" value="<%=lawFirm.getUserEmail()%>">
                      </div>
                    </div>  
          
                     
                      <div class="form-group">
                          
                        <div class="col-xs-6">
                          <label for="password2"><h4 class="h" >Password</h4></label>
                            <input type="password"  style="font-size: 20px;" class="form-control" name="password1" id="password1" placeholder="enter password" required="required" value="<%=lawFirm.getUserPassword()%>">
                        </div>
                      </div>
                      
                      
                       <div class="form-group">
                          
                      <div class="col-xs-6">
                        <label for="last_name"><h4 style="font-size: 20px;">Registration Number</h4></label>
                          <input type="text" style="font-size: 20px;" class="form-control" name="regno" id="regno" placeholder="Registration number" required="required" value="<%=lawFirm.getLfregNo()%>">
                      </div>
                  </div>
                      
                      

                     
                    
                     
                    <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="areapin"><h4 style="font-size: 20px;">Area Pincode</h4></label>
                                <%List<Area> areaList=(List)request.getAttribute("areas"); %> 
								<select style="font-size: 20px;" class="form-control" id="areapin" name="areapin">
 								
<!-- 								<select id="select-area"  name="area" required="required"> -->
<!-- 								<option value=""> </option> -->
								<%int i=1; %>
								<%for(Area a: areaList){ %>
									<%if(lawFirm.getAreaId()==i)
									{
									%>
										<option selected="selected" value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
									<%}%>
									<option value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
 								<%i++; %> 
  								<%} %>
						  		</select>
                           </div>
                      </div> 


						 <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h4 class="h" >Address</h4></label>
                              <!-- <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any."> -->
                            <textarea style="font-size: 20px;"  class="form-control" name="address" id="address" required="required"><%=lawFirm.getUserAddress()%></textarea>
                            </div>
                      </div>

					 	
						
                      <div class="form-group">
                        <div class="col-xs-6">
                           <label for="mobile"><h4 style="font-size: 20px;">Law Firm Description</h4></label>
                          <textarea style="font-size: 20px;" class="form-control" name="lf_desc" id="lf_desc" required="required"><%=lawFirm.getLfDesc()%></textarea>
                          </div>
                    </div>
                      
                  <!-- <div class="form-group" > -->
 
                     


                   
                  
                  <input type="hidden" class="form-input" name="lawfirm_id" id="lawfirm_id" value="<%=lawFirm.getUserId()%>"/>
                  
                       
                       
                      <div class="form-group">
                           <div class="col-xs-12" style="position: absolute;top: 100%;">
                                <br>
                              	<button style="font-size: 20px;" class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button style="font-size: 20px;" class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
              
              
             </div><!--/tab-pane-->
             
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
    
   </div>
    <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<%@ include file="HomePageFooter.jsp" %>

</body>
</html>                                