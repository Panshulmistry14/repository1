<%@page import="com.lawmart.bean.Services"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Services Table</title>
<meta name="description" content="Sufee Admin - HTML5 Admin Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="stylesheet" href="assets/css/table/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/font-awesome.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/all.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/dataTables.bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/style.css"> -->

<!-- Favicons -->
<!--   <link href="assets/img/favicon.png" rel="icon"> -->
<!--   <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon"> -->

  <!-- Google Fonts -->
<!--   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->

<!-- Template Main CSS File -->
<!--   <link href="assets/css/style.css" rel="stylesheet"> -->
<%@ include file="Servicesheader.jsp" %>
</head>
<body> 

<div id="right-panel" class="right-panel">
<div class="content mt-3">
<div class="animated fadeIn">
<div class="row">
<div class="col-md-12">
<div class="card">
<br>
<br>
<div class="card-header">
<strong class="card-title">Services Table</strong>
</div>
<div class="card-body">
<%List<Services> servicesList = (List)request.getAttribute("Services"); %>
<table id="bootstrap-data-table" class="table table-striped table-bordered">
<thead>
<tr>
<th>Sr No.</th>
<th>Name</th>
<th>Description</th>
<th>Edit</th>
<th>Delete</th>
</tr>
</thead>
<tbody>

<%
int sId=0;
for(Services services : servicesList){ %>
<tr>
<td><%=services.getServiceId() %></td>
<td><%=services.getServiceName() %></td>
<td><%=services.getServiceDesc() %></td>
<td><a href="AdminEditData?Page1=serv&serviceId=<%=services.getServiceId()%>"><i class="fas fa-edit"></i></a></td>
<td><button onclick="document.getElementById('<%=services.getServiceId()%>').style.display='block'" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></td>
</tr>


<div id="<%=services.getServiceId()%>" class="modal" style="position: fixed; ;top: 47%;height: 150px;width: 400px;left: 37%;border: 2px solid black;background: white;">
  <form class="modal-content"> <!-- action="/action_page.php" -->
    <div class="container" style="background-color:activeborder ;color: black; border-radius: 20px; ">
      <h1>Delete <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <a href="AdminManage?Page1=services" class="cancelbtn" style="color: black;"> <span aria-hidden="true">&times;</span> </a>
        </button></h1>
       
      <p>Are you sure you want to delete <%=services.getServiceName()%>?</p>

      <div class="clearfix" style="position: relative;">
     <a style="position: absolute;left: 50%;color: black;border: 1px solid black;background-color : lightgray;padding-top : 3px;padding-bottom : 3px;padding-right : 5px;padding-left : 5px;" href="AdminManage?Page1=services" class="cancelbtn"> Cancel</a>
      <a style="position: absolute;left: 33%;color: black;border: 1px solid black;background-color : lightgray;padding-top : 3px;padding-bottom : 3px;padding-right : 5px;padding-left : 5px;" href="RemoveService?serviceId=<%=services.getServiceId()%>" class="deletebtn"> Delete</a>  
     
      </div>
    </div>
  </form>
</div>

<%} %>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
 <script src="assets/css/table/datatables.min.js"></script>
<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
<script src="assets/css/table/datatables-init.js"></script>

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>
</html>