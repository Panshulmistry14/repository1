<!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Hiren Raval & Associates</h3>
            <p> 506, Venus Benecia, Nr. Pakwan Dining,<br>
               Bodakdev,Ahmedabad.<br>
              India.<br><br>
              <strong>Phone:</strong> +91 9824 6445 43<br>
              <strong>Email:</strong> : haraval70@yahoo.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.jsp">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="AboutUs.jsp">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="GeneralServices.jsp">Services</a></li>
             <!--  <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li> -->
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Rent Agreement</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Power Of Attorney</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Sale Deed</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Legal Advice</a></li>
             <!--  <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li> -->
            </ul>
          </div>

  <!--         <div class="col-lg-4 col-md-6 footer-newsletter"> -->      <!--      USEFUL FOR GETTING FEEDBACK FROM CLIENTS -->
       <!--      <h4>Feedback</h4>
            <p>If you like the service then please give your valuable Feedback</p>
            <form action="" method="post">
              <textarea rows="2" cols="30" placeholder="Your feedback" required="required"></textarea><br>
              <input type="email" name="email" placeholder="email" required="required"><br>
              <input type="submit" value="feedback">
            </form>
          </div>   --> 

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Company</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/company-free-html-bootstrap-template/ -->
          Created by <a href="#">SHAIL DAVE PANSHUL MISTRY KAMIT RAVAL</a>
        </div>
      </div>
     
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/js/plugins.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
