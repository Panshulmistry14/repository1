<%@page import="com.lawmart.bean.LFServices"%>
<%@page import="com.lawmart.bean.BookingConsultation"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.Feedback"%>
<%@page import="com.lawmart.servlet.GetAreas"%>
<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>FeedBack Table</title>
<meta name="description" content="Sufee Admin - HTML5 Admin Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="stylesheet" href="assets/css/table/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/font-awesome.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/all.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/dataTables.bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/style.css"> -->

<!-- Favicons -->
<!--   <link href="assets/img/favicon.png" rel="icon"> -->
<!--   <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon"> -->

<!-- Google Fonts -->
<!--   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->

<!-- Vendor CSS Files -->
<!--   <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/venobox/venobox.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/aos/aos.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet"> -->

<!-- Template Main CSS File -->
<!--   <link href="assets/css/style.css" rel="stylesheet"> -->




</head>
<body>
	<%@ include file="Servicesheader.jsp"%>
	<div id="right-panel" class="right-panel">

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<br> <br>
							<div class="card-header">
								<strong class="card-title">FeedBack Table</strong>
							</div>
							<div class="card-body">
								<table id="bootstrap-data-table"
									class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Client Name</th>
											<th>Lawfirm Name</th>
											<th>Client's FeedBack</th>
											<th>FeedBack Date</th>
											<th>Service Type</th>
											<th>FeedBack Rate</th>
											<th>Delete</th>
										</tr>
									</thead>

									<%
										List<Feedback> fbList = (List) request.getAttribute("feedback");
									%>
									<tbody>
									<%   if(fbList!=null) {%>
										<tr>
											<%
												for (Feedback f : fbList) {
											%>
											<%
													LawMartService ls = new LawMartServiceImpl();
													Client client=null;
        											LawFirm lf=null;
       												BookingConsultation bc =null;
       										
													bc = ls.getBookDetails(f.getsId());

					 								client = ls.getSingleClient(bc.getClientId());
  					 								lf = ls.getSingleLF(bc.getLfId());  

											%>
											
											<%if(client.getUserName()!=null) {%>
											<td><%=client.getUserName() %></td>
											<%} else { %>
											<td>Client Name Not Found!</td>
											<%} %>
											
											
											<%if(lf.getUserName()!=null) {%>
											<td><%=lf.getUserName() %></td>
											<%} else { %>
											<td>Lawfirm Name Not Found!</td>
											<%} %>
											
											
											<%if(f.getFeedback()!=null) { %>
											<td><%=f.getFeedback()%></td>
											<%} else { %>
											<td>Client's Feedback Not Found!</td>
											<%} %>
											
											
											<%if(f.getfDate()!=null){ %>
											<td><%=f.getfDate()%></td>
											<%} else { %>
											<td>FeedBack Date Not found!</td>
											<%} %>
											
										
											<%if(f.getsType()!=null){ %>
												<%if(f.getsType().equals("bc")) {%>
												<%
													LFServices lfs = ls.getSingleLFService(bc.getSrNo());
							
													Services services = ls.fetchServices(String.valueOf(lfs.getsId()));
												%>
													<%if(services.getServiceName()!=null) {%>
												 		<td><%=services.getServiceName() %></td>
												 	<%} else {%>
												 		<td>Services Name Not Found!</td>
												 	<%} %>
												<%} %>
												<%if(f.getsType().equals("la")) {%>
													<td>Legal Advice</td>
												<%} %>
											<%} else { %>
											<td>Service type Not Found!</td>
											<%} %>
											
											
											<%if(f.getFrate()!=null) { %>
												<%if (f.getFrate().equals("very good") || f.getFrate().equals("good")) { %>
													<td style="color: #1bbd36;"><%=f.getFrate()%></td>
												<%} else if (f.getFrate().equals("mediocre")) {%>
													<td style="color: orange;"><%=f.getFrate()%></td>
												<%} else if (f.getFrate().equals("very bad") || f.getFrate().equals("bad")) { %>
													<td style="color: red;"><%=f.getFrate() %></td>
												<%} %>						
											<%} else { %>
											<td>FeedBack Rate Not Found!</td>
											<%} %>
											
											
											<td><button
													onclick="document.getElementById('<%=f.getfId()%>').style.display='block'"
													class="btn btn-danger">
													<i class="fas fa-trash-alt"></i>
												</button></td>
										</tr>
										<div id="<%=f.getfId()%>" class="modal"
											style="position: fixed;; top: 47%; height: 150px; width: 400px; left: 37%; border: 2px solid black; background: white;">
											<form class="modal-content">
												<div class="container"
													style="background-color: activeborder; color: black; border-radius: 20px;">
													<h1>
														Delete
														<button type="button" class="close" data-dismiss="modal"
															aria-label="Close">
															<a href="AdminManage?Page1=feedback" class="cancelbtn"
																style="color: black;"> <span aria-hidden="true">&times;</span>
															</a>
														</button>
													</h1>

													<p>
														Are you sure you want to delete
<%-- 														<%=c.getUserName()%>? --%>
													</p>

													<div class="clearfix" style="position: relative;">
														<a
															style="position: absolute; left: 50%; color: black; border: 1px solid black; background-color: lightgray; padding-top: 3px; padding-bottom: 3px; padding-right: 5px; padding-left: 5px;"
															href="AdminManage?Page1=feedback" class="cancelbtn">
															Cancel</a> <a
															style="position: absolute; left: 33%; color: black; border: 1px solid black; background-color: lightgray; padding-top: 3px; padding-bottom: 3px; padding-right: 5px; padding-left: 5px;"
															href="RemoveFeedback?fbId=<%=f.getfId()%>"
															class="deletebtn"> Delete</a>
													</div>
												</div>
											</form>
										</div>
										<%
											}
										%>
									<%} %>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="assets/css/table/datatables.min.js"></script>
	<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
	<script src="assets/css/table/datatables-init.js"></script>

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>


</body>
</html>