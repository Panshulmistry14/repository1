<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.dao.impl.LawMartDaoImpl"%>
<%@page import="com.lawmart.dao.LawMartDao"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Client-View Response</title>


<%
	HttpSession httpSession = request.getSession(false);
User user = (User) httpSession.getAttribute("userisclient");
%>

<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet"
	href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Feedback link -->
<link
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	rel="stylesheet">

<style>
<!--
Feedback -->.modal-dialog {
	height: 50%;
	width: 50%;
	margin: auto
}

.modal-header {
	color: white;
	background-color: #1bbd36
}

textarea {
	border: none;
	box-shadow: none !important;
	-webkit-appearance: none;
	outline: 0px !important
}

.openmodal {
	margin-left: 35%;
	width: 25%;
	margin-top: 25%
}

.icon1 {
	color: #1bbd36
}

#cancel {
	background-color: #1bbd36;
	color: white;
	position: absolute;
	left: 50%;
	bottom: 3%;
	border-color: #1bbd36;
}

#submit {
	/* margin: auto; */
	position: absolute;
	right: 50%;
	border-color: #1bbd36;
	bottom: 3%;
}

input:checked {
	color: #1bbd36;
}

#cancel:hover {
	background-color: #1bbd36;
}
</style>


</head>
<body>
	<%@ include file="ClientHeader.jsp"%>

	<br>
	<br>
	<br>

	<div>

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<section class="success"
								style="margin-bottom: 15px; padding: 4px 12px; background-color: #ddffdd; border-left: 6px solid #4CAF50;">
								<p style="font-size: 18px">
									<strong>Note!</strong> <strong>Once you accept the
										proposal of the law firm,you need to whether accept it or
										reject it. If you accept,then you will be redirected to
										payment page.If you reject,then your entire histroy of legal
										advice will be removed from the system!</strong>
								</p>
							</section>

							<div class="card-header">
								<strong class="card-title">Legal Advice Table</strong><br>
							</div>
							<div class="card-body">

								<table id="bootstrap-data-table"
									class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Law Firm's Name</th>
											<th>Law Firm's<br> Phone number
											</th>
											<th>Your issue</th>
											<th>Payment Status</th>
											<th>Firm's Response</th>
											<th>Fees</th>
											<th>Order ID</th>
											<th>Payment Date</th>
											<th>Accept</th>
											<th>Reject</th>
											<th>Give Response</th>

										</tr>
									</thead>

									<tbody>

										<%
											LawMartService ls = new LawMartServiceImpl();
										%>

										<%
											List<LegalAdvice> laList = (List) request.getAttribute("ladata");
										if (laList != null) {

											for (LegalAdvice la : laList) {
										%>

										<tr>
											<%
												LawFirm lf = ls.getSingleLF(la.getLf_id());
											%>

											<%
												if (lf.getUserName() != null) {
											%>
											<td><%=lf.getUserName()%></td>
											<%
												} else {
											%>

											<td>Firm's name not found!</td>
											<%
												}
											%>

											<%
												if (lf.getUserPhone() != null) {
											%>
											<td><%=lf.getUserPhone()%></td>
											<%
												} else {
											%>

											<td>Firm's phone number not found!</td>
											<%
												}
											%>

											<td><%=la.getAdv_issue()%></td>
											<td><%=la.getPayment_status()%></td>

											<%
												if (la.getFirm_responses() != null) {
											%>
											<td><%=la.getFirm_responses()%></td>
											<%
												} else {
											%>
											<td>Firm has not responded yet!</td>
											<%
												}
											%>

											<%
												if (la.getAdv_fees() != 0) {
											%>
											<td><%=la.getAdv_fees()%></td>
											<%
												} else {
											%>
											<td>Firm has not mentioned yet!</td>
											<%
												}
											%>

											<%
												if (la.getOrderId() != null) {
											%>
											<td><%=la.getOrderId()%></td>
											<%
												} else {
											%>
											<td>Order Id is not registered.</td>
											<%
												}
											%>

											<%
												if (la.getPayDate() != null) {
											%>
											<td><%=la.getPayDate()%></td>
											<%
												} else {
											%>
											<td>Payment is not done yet!</td>
											<%
												}
											%>

											<%
												if (la.getPayment_status().equals("SUCCESS") || la.getAdv_issue() == null || la.getAdv_fees() == 0
													|| la.getPayment_status().equals("At Appointment Time")) {
											%>
											<td>

												<button class="btn btn-success" data-toggle="modal"
													onclick="document.getElementById('<%=la.getAdv_id()%>').style.display='block'"
													disabled>
													<i class="fas fa-check"></i>
												</button>
											</td>
											<%
												} else {
											%>
											<td><button class="btn btn-success" data-toggle="modal"
													onclick="document.getElementById('<%=la.getAdv_id()%>').style.display='block'">
													<i class="fas fa-check"></i>
												</button></td>
											<%
												}
											%>
											<%
												if (la.getPayment_status().equals("SUCCESS") || la.getPayment_status().equals("At Appointment Time")) {
											%>
											<td>
												<button class="btn btn-danger"
													onclick="document.getElementById('<%=la.getAdv_id() + user.getUserId()%>').style.display='block'"
													disabled>
													<i class="fa fa-times" aria-hidden="true"></i>
												</button>
											</td>
											<%
												} else {
											%>
											<td>
												<button class="btn btn-danger"
													onclick="document.getElementById('<%=la.getAdv_id() + user.getUserId()%>').style.display='block'">
													<i class="fa fa-times" aria-hidden="true"></i>
												</button>
											</td>
											<%
												}
											%>

											<%
												int present = ls.CheckFB(la.getAdv_id());
											%>

											<%
												System.out.println(" present " + present + " bookid " + la.getAdv_id());
											%>

											<%
												if (present == 0) {
											%>
											<td>
												<!--Modal Launch Button--> <a href=""
												style="height: 50px; width: 50px; background-color: #1bbd36; border-color: #1bbd36;"
												class="btn btn-info btn-lg openmodal" id="modalbtn"
												data-toggle="modal"
												data-target="#myModal<%=la.getAdv_id()%>"><i
													class="fas fa-comments"></i> </a> <!--Division for Modal-->
												<div id="myModal<%=la.getAdv_id()%>" class="modal fade"
													role="dialog">
													<!--Modal-->
													<form method="post" action="GetFeedbackData">
														<div class="modal-dialog">
															<!--Modal Content-->
															<div class="modal-content">
																<!-- Modal Header-->
																<div class="modal-header">
																	<h3>Feedback Request</h3>
																	<!--Close/Cross Button-->
																	<button type="button" class="close"
																		data-dismiss="modal" style="color: white;">&times;</button>
																</div>
																<!-- Modal Body-->
																<div class="modal-body text-center">


																	<h3>Your opinion matters</h3>
																	<h5>
																		Help us improve our services? <strong>Give us
																			your feedback.</strong>
																	</h5>
																	<hr>
																	<h6>Your Rating</h6>
																	<input type="hidden" readonly="readonly" name="sid"
																		value="<%=la.getAdv_id()%>">
																	<input type="hidden" value="la" name="stype">

																</div>
																<!-- Radio Buttons for Rating-->
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" checked="checked"
																		value="very good"> <label class="ml-3">Very
																		good</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="good">
																	<label class="ml-3">Good</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="mediocre">
																	<label class="ml-3">Mediocre</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="bad">
																	<label class="ml-3">Bad</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="very bad">
																	<label class="ml-3">Very Bad</label>
																</div>
																<!--Text Message-->
																<div class="text-center">
																	<h4>Your Feedback</h4>
																</div>
																<textarea type="textarea" required="required"
																	placeholder="Your Message" rows="3" name="feedmsg"></textarea>
																<!-- Modal Footer-->
																<div class="modal-footer">

																	<input style="background-color: #1bbd36;" type="submit"
																		value="Send" id="submit" class="btn btn-primary">
																	<a style="" id="cancel" href=""
																		class="btn btn-outline-primary" data-dismiss="modal">Cancel</a>
																</div>
															</div>
														</div>

													</form>

												</div>





											</td>
											<%
												} else if (present == 1) {
											%>

											<td><strong>Thanks for Your valuable Feedback!</strong></td>
											<%
												}
											%>






											<!--  data-toggle="modal2" data-target="#modal1" -->
										</tr>
										<!-- Modal accept -->
										<div id="<%=la.getAdv_id()%>" class="modal"
											style="position: fixed;; top: 47%; height: 150px; width: 400px; left: 37%; border: 2px solid black; background: white;">
											<form class="modal-content">
												<div class="container"
													style="background-color: activeborder; color: black; border-radius: 20px;">
													<h1>
														Payment
														<button type="button" class="close" data-dismiss="modal"
															aria-label="Close">
															<a href="ClientLAData" class="cancelbtn"
																style="color: black;"> <span aria-hidden="true">&times;</span>
															</a>
														</button>
													</h1>

													<p>Are you sure you want to pay?</p>

													<div class="clearfix" style="position: relative;">
														<a
															style="position: absolute; left: 50%; color: black; border: 1px solid black; background-color: lightgray; padding-top: 3px; padding-bottom: 3px; padding-right: 5px; padding-left: 5px;"
															href="ChangeAppStatus?advId=<%=la.getAdv_id()%>"
															class="cancelbtn">Appointment</a> <a
															style="position: absolute; left: 33%; color: black; border: 1px solid black; background-color: lightgray; padding-top: 3px; padding-bottom: 3px; padding-right: 5px; padding-left: 5px;"
															href="ResponseServlet?advId=<%=la.getAdv_id()%>"
															class="deletebtn">PayTm</a>
													</div>
												</div>
											</form>
										</div>
										<!-- End of Modal accept -->

										<!-- Modal reject -->
										<div id="<%=la.getAdv_id() + user.getUserId()%>" class="modal"
											name="reject"
											style="position: fixed;; top: 47%; height: 150px; width: 400px; left: 37%; border: 2px solid black; background: white;">
											<form class="modal-content">
												<div class="container"
													style="background-color: activeborder; color: black; border-radius: 20px;">
													<h1>
														Reject Response
														<button type="button" class="close" data-dismiss="modal"
															aria-label="Close">
															<a href="ClientLAData" class="cancelbtn"
																style="color: black;"> <span aria-hidden="true">&times;</span>
															</a>
														</button>
													</h1>

													<p>Are you sure you want to reject the response?</p>

													<div class="clearfix" style="position: relative;">
														<a
															style="position: absolute; left: 50%; color: black; border: 1px solid black; background-color: lightgray; padding-top: 3px; padding-bottom: 3px; padding-right: 5px; padding-left: 5px;"
															href="ClientLAData" class="cancelbtn">Cancel</a> <a
															style="position: absolute; left: 33%; color: black; border: 1px solid black; background-color: lightgray; padding-top: 3px; padding-bottom: 3px; padding-right: 5px; padding-left: 5px;"
															href="DeleteLAClient?advId=<%=la.getAdv_id()%>"
															class="deletebtn">Delete</a>
													</div>
												</div>
											</form>
										</div>

										<!-- End of Modal reject -->

										<%
											}
										%>
										<%
											}
										%>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>





	<script src="assets/css/table/datatables.min.js"></script>
	<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
	<script src="assets/css/table/datatables-init.js"></script>

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>


	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>


	<!-- Feedback Links -->

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>




</body>
</html>