<%@page import="com.lawmart.bean.Area"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Law Firm Sign Up Form</title>

<!-- Font Icon -->
<link rel="stylesheet"
	href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
<link rel="stylesheet"
	href="assets/vendor/Registration/jquery-ui/jquery-ui.min.css">

<!-- Main css -->
<link rel="stylesheet" href="assets/css/Registration/style.css">

<style type="text/css">
body {
	background: transparent;
}

.Signup-form-title {
	background-color: transparent;
	color: black;
}
</style>

<script src="assets/js/jquery-3.5.1.min.js"></script>

<script>
	$(document).ready(function() {

		$(".form-submit").attr('disabled', 'disabled');
		$("input[type=email]").blur(function() {
			//	alert("hey there");
			$("#msg").empty();
			var str = $("#email").val();
			if (str.length == 0) {
			} else {
				$.get("CheckEmail", {
					email : str
				}).done(function(data) {
					if (data == 'true') {
						//	alert("email already exists");
						$(".form-submit").attr('disabled', 'disabled');

						$("#msg").append("Email already exists");
					} else {
						$(".form-submit").removeAttr('disabled', 'disabled');
						$("#msg").empty();

					}
				});
			}
		});
	});
</script>


</head>
<body>

	<span class="Signup-form-title"> Sign Up </span>
	<div class="main">

		<section class="signup">
			<!-- <img src="images/signup-bg.jpg" alt=""> -->
			<div class="container">
				<div class="signup-content">

					<form method="POST" id="signup-form" class="signup-form"
						action="GetLawFirmReg">
						<!--          <div class="form-row">  -->
						<div class="form-group">
							<label for="firm_name">Firm name</label> <input type="text"
								class="form-input" name="firm_name" id="firm_name"
								required="required" />
						</div>
						<!--      <div class="form-group">
                                <label for="firm_reg_no">Firm Registration Number</label>
                                <input type="text" class="form-input" name="firmregno" id="firmregno" required="required" />
                            </div> -->
						<!--        </div> -->
						<div class="form-row">


							<div class="form-group">

								<label for="Area">Area</label>

								<%
									List<Area> areaList = (List) request.getAttribute("areas");
								%>
								<select id="select-area" name="area" required="required">
									<option value=""></option>
									<%
										int i = 1;
									%>
									<%
										for (Area a : areaList) {
									%>
									<option value="<%=a.getAreaId()%>"><%=a.getAreaName() + "   " + a.getAreaPin()%></option>
									<%
										i++;
									%>
									<%
										}
									%>
								</select>

							</div>


							<div class="form-group">
								<label for="phone_number">Phone number</label><span id="msg2"
									style="color: red"></span> <input type="number"
									class="form-input" name="phone_number" id="phone_number">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group">
								<label for="password" style="text-align: right;">Password</label>
								<input type="password" class="form-input" name="password"
									id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
								<span id="msg1" style="color: red"></span>
							</div>
							<div class="form-group">
								<label for="re_password">Confirm your password</label> <input
									type="password" class="form-input" name="re_password"
									id="re_password" />
							</div>
						</div>
						<div class="form-group">
							<label for="email">Email</label> <span id="msg"
								style="color: red"></span> <input type="email"
								class="form-input" name="email" id="email" required="required" />
						</div>

						<div class="form-group">
							<label for="Address">Address</label>
							<textarea rows="3" cols="60" required="required"
								class="form-input" name="address" id="address"></textarea>
						</div>






						<!-- <div class="form-text">
                            <a href="#" class="add-info-link"><i class="zmdi zmdi-chevron-right"></i>Additional info</a>
                            <div class="add_info">
                               
                                <div class="form-group">
                                    <label for="city">City</label>
                                    <div class="select-list">
                                        <select name="city" id="city" required>
                                            <option value="NY">NewYork</option>
                                            <option value="IC">IceLand</option>
                                            <option value="canada">Canada</option>
                                        </select>
                                    </div>
                                </div> 
                            </div>
                        </div> -->
						<div class="form-group">
							<input type="submit" name="submit" id="submit"
								class="form-submit" value="Submit" />
						</div>
					</form>
				</div>
			</div>
		</section>

	</div>

	<!-- JS -->
	<script src="assets/vendor/Registration/jquery/jquery.min.js"></script>
	<script src="assets/vendor/Registration/jquery-ui/jquery-ui.min.js"></script>
	<script
		src="assets/vendor/Registration/jquery-validation/dist/jquery.validate.min.js"></script>
	<script
		src="assets/vendor/Registration/jquery-validation/dist/additional-methods.min.js"></script>
	<script src="assets/js/Registrationjs/main.js"></script>

	<!-- selecting area -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"
		integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk="
		crossorigin="anonymous"></script>
	<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"
		integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg="
		crossorigin="anonymous" />

	<script type="text/javascript">
		$(document).ready(function() {
			$('select').selectize({
				sortField : 'text'
			});
		});
	</script>



	<script>
		var myInput = document.getElementById("password");

		// When the user clicks on the password field, show the message box
		myInput.onfocus = function() {
			//alert("focus on pass field");
			// document.getElementById("message").style.display = "block";
		}

		// When the user clicks outside of the password field, hide the message box
		myInput.onblur = function() {

			// Validate length
			if (myInput.value.length < 8) {
				//alert("password length should be greater than 8 characters");
				document.getElementById("msg1").innerHTML = "Password length should be of 8 to 15 letters";
			} else {

			}

		}

		// When the user starts to type something inside the password field
		myInput.onkeyup = function() {
			// Validate lowercase letters
			var lowerCaseLetters = /[a-z]/g;

			// Validate capital letters
			var upperCaseLetters = /[A-Z]/g;

			// Validate numbers
			var numbers = /[0-9]/g;
			if (myInput.value.match(numbers)
					&& myInput.value.match(lowerCaseLetters)
					&& myInput.value.match(upperCaseLetters)) {
				document.getElementById("msg1").innerHTML = "";

			} else {
				//alert("Invalid password");
				document.getElementById("msg1").innerHTML = "Password must contain atleast one Digit,UpperCase,LowerCase Letters";

			}

		}
	</script>

	<script>
		//phone number validation
		var myPhone = document.getElementById("phone_number");

		var a = document.getElementById("phone_number").value;

		myPhone.onblur = function() {

			if (myPhone.value.length == 10) {
				//alert("length is 10");
				document.getElementById("msg2").innerHTML = "";

				if (myPhone.value.charAt(0) != 9
						&& myPhone.value.charAt(0) != 7
						&& myPhone.value.charAt(0) != 8
						&& myPhone.value.charAt(0) != 6) {

					//alert("first no should 6 7 8 9");
					document.getElementById("msg2").innerHTML = "Phone number should start from 6,7,8 or 9";

					document.getElementById("submit").disabled = true;

				} else {
					document.getElementById("msg2").innerHTML = "";
					document.getElementById("submit").disabled = false;

				}

			}

			else {
				document.getElementById("msg2").innerHTML = "Phone number should be of 10 Digits";
				document.getElementById("submit").disabled = true;

			}

		}
	</script>




</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>