<%@page import="com.lawmart.bean.LFServices"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.BookingConsultation"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Client-Booking Consultations</title>

<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet"
	href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Feedback link -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">


<style>
.alert {
	padding: 20px;
	background-color: #1bbd36;
	color: white;
	font-size: 25px;
}

.closebtn {
	margin-left: 15px;
	color: white;
	font-weight: bold;
	float: right;
	font-size: 22px;
	line-height: 20px;
	cursor: pointer;
	transition: 0.3s;
}

.closebtn:hover {
	color: black;
}


<!-- Feedback -->

        .modal-dialog {
    height: 50%;
    width: 50%;
    margin: auto
}

.modal-header {
    color: white;
    background-color: #1bbd36
}

textarea {
    border: none;
    box-shadow: none !important;
    -webkit-appearance: none;
    outline: 0px !important
}

.openmodal {
    margin-left: 35%;
    width: 25%;
    margin-top: 25%
}

.icon1 {
    color: #1bbd36
}

#cancel{
            background-color: #1bbd36;
            color:white;
            position: absolute;
            
            left: 50%;
            bottom: 3%;
            
            border-color: #1bbd36;
            
    
} 

#submit{
   /* margin: auto; */
   position: absolute;
   right: 50%;
   border-color: #1bbd36;
               bottom: 3%;
   
   
    

}

input:checked {
    color: #1bbd36;
}

#cancel:hover{
    background-color: #1bbd36;
} 

</style>


</head>
<body>

	<%
		HttpSession httpSession = request.getSession(false);
	User user = (User) httpSession.getAttribute("userisclient");
	%>

	<%
		if (user == null) {
	%>
	<%
		response.sendRedirect("Login.jsp");
	%>
	<%
		} else {
	%>


	<%@ include file="ClientHeader.jsp"%>


	<br>
	<br>
	<br>
	
	<%
		String str = (String) request.getAttribute("rply2");
	%>
	<%
		if (str != null) {
	%>
	<div class="alert" align="center">
		<span class="closebtn"
			onclick="this.parentElement.style.display='none';">&times;</span> <strong>Success!</strong>
		<%=str%>
		
		
		<br><br>
		
		
		
	</div>

	<%
		}
	%>
	<div>

		<div class="content mt-3">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-md-12">
						<div class="card">


							<div class="card-header">
								<strong class="card-title">Booking Consultations Table</strong>
							</div>
							<div class="card-body">
								<table id="bootstrap-data-table"
									class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>LawFirm Name</th>
											<th>LawFirm Number</th>
											<th>Service Name</th>
											<th>Fees</th>
											<th>Booking Date</th>
											<th>Booking Status</th>
											<th>Payment Status</th>
											<th>Order ID</th>
											<th>View Document</th>
											<th>Appointmemt Response</th>
											<th>Give Feedback</th>
										</tr>
									</thead>


									<%
										LawMartService ls = new LawMartServiceImpl();
									List<BookingConsultation> bcList = (List) request.getAttribute("bcdata");
									%>
									<tbody>

										<%
											if (bcList != null) {
										%>
										<%
											for (BookingConsultation bc : bcList) {
										%>
										<tr>


											<%
												LawFirm lf = ls.getSingleLF(bc.getLfId());
											%>

											<%
												if (lf.getUserName() != null) {
											%>
											<td><%=lf.getUserName()%></td>
											<%
												} else {
											%>
											<td>LawFirm name is not Available!</td>
											<%
												}
											%>

											<%
												if (lf.getUserPhone() != 0) {
											%>
											<td><%=lf.getUserPhone()%></td>
											<%
												} else {
											%>
											<td>LawFirm number is not Available!</td>
											<%
												}
											%>

											<%
												LFServices lfs = ls.getSingleLFService(bc.getSrNo());
											Services services = ls.fetchServices(String.valueOf(lfs.getsId()));
											%>

											<%
												if (services.getServiceName() != null) {
											%>
											<td><%=services.getServiceName()%></td>
											<%
												} else {
											%>
											<td>Service name is not Available!</td>
											<%
												}
											%>



											<%
												if (bc.getBookAmt() != 0) {
											%>
											<td><%=bc.getBookAmt()%></td>
											<%
												} else {
											%>
											<td>Amount not Available!</td>
											<%
												}
											%>



											<%
												if (bc.getBookDate() != null) {
											%>
											<td><%=bc.getBookDate()%></td>
											<%
												} else {
											%>
											<td>Date is not Available!</td>
											<%
												}
											%>


											<%
												if (bc.getBookStatus() != null) {
											%>
											<%
												if (bc.getBookStatus().equals("success")) {
											%>
											<td style="color: #1bbd36"><%=bc.getBookStatus()%></td>
											<%
												} else {
											%>
											<td style="color: red;"><%=bc.getBookStatus()%></td>

											<%
												}
											%>
											<%
												} else {
											%>
											<td>Status not Available!</td>
											<%
												}
											%>


											<%
												if (bc.getPayStatus() != null) {
											%>
											<%
												if (bc.getPayStatus().equals("pending")) {
											%>
											<td style="color: red;" ss><%=bc.getPayStatus()%></td>

											<%
												} else if (bc.getPayStatus().equals("at Firm")) {
											%>
											<td style="color: orange;" ss><%=bc.getPayStatus()%></td>

											<%
												} else if (bc.getPayStatus().equals("success")) {
											%>
											<td style="color: #1bbd36;" ss><%=bc.getPayStatus()%></td>
											<%
												}else if(bc.getPayStatus().equals("failure")){
											%>
											<td style="color: red;" ss><%=bc.getPayStatus()%></td>
											
											
											<%} %>



											<%
												} else {
											%>
											<td>Status not Available!</td>
											<%
												}
											%>
											
											<% if(bc.getOrderId()!=null && bc.getPayStatus().equals("success")){ %>
											
											<td><%= bc.getOrderId()%></td>
											
											<%} else {%>
											
											<td> Order Id is not registered Yet!</td>
											
											<%} %>
											
											<%
												if (bc.getDocId() != 0) {
											%>
											<td><a href="GetPdfDoc?docid=<%=bc.getDocId()%>"><i
													class="fa fa-eye" aria-hidden="true"></i></a></td>
											<%
												} else {
											%>
											<td>Document not Available!</td>
											<%
												}
											%>

											<%
												if (bc.getBookAppointment() != null) {
											%>
											<td><%=bc.getBookAppointment()%></td>
											<%
												} else {
											%>
											<td>LawFirm has not mentioned yet!</td>
											<%
												}
											%>
											
											<%int present=ls.CheckFB(bc.getBookId());%>
											
											<%System.out.println(" present "+present+" bookid "+bc.getBookId()); %>

											  <%if(present==0){ %>   
											<td>
												<!--Modal Launch Button--> <a href=""
												style="height: 50px; width: 50px; background-color: #1bbd36; border-color: #1bbd36;"
												class="btn btn-info btn-lg openmodal" id="modalbtn"
												data-toggle="modal"
												data-target="#myModal<%=bc.getBookId()%>"><i
													class="fas fa-comments"></i> </a> <!--Division for Modal-->
												<div id="myModal<%=bc.getBookId()%>" class="modal fade"
													role="dialog">
													<!--Modal-->
													<form method="post" action="GetFeedbackData">
														<div class="modal-dialog">
															<!--Modal Content-->
															<div class="modal-content">
																<!-- Modal Header-->
																<div class="modal-header">
																	<h3>Feedback Request</h3>
																	<!--Close/Cross Button-->
																	<button type="button" class="close"
																		data-dismiss="modal" style="color: white;">&times;</button>
																</div>
																<!-- Modal Body-->
																<div class="modal-body text-center">


																	<h3>Your opinion matters</h3>
																	<h5>
																		Help us improve our services? <strong>Give us
																			your feedback.</strong>
																	</h5>
																	<hr>
																	<h6>Your Rating</h6>
																	<input type="hidden" readonly="readonly" name="sid"
																		value="<%=bc.getBookId() %>">
																	<input type="hidden" value="bc" name="stype">

																</div>
																<!-- Radio Buttons for Rating-->
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" checked="checked"
																		value="very good"> <label class="ml-3">Very
																		good</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="good">
																	<label class="ml-3">Good</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="mediocre">
																	<label class="ml-3">Mediocre</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="bad">
																	<label class="ml-3">Bad</label>
																</div>
																<div class="form-check mb-4">
																	<input name="feedback" type="radio" value="very bad">
																	<label class="ml-3">Very Bad</label>
																</div>
																<!--Text Message-->
																<div class="text-center">
																	<h4>Your Feedback</h4>
																</div>
																<textarea type="textarea" required="required"
																	placeholder="Your Message" rows="3" name="feedmsg"></textarea>
																<!-- Modal Footer-->
																<div class="modal-footer">

																	<input style="background-color: #1bbd36;" type="submit"
																		value="Send" id="submit" class="btn btn-primary">
																	<a style="" id="cancel" href=""
																		class="btn btn-outline-primary" data-dismiss="modal">Cancel</a>
																</div>
															</div>
														</div>

													</form>

												</div>





											</td>
											<%} else if(present==1){%>
											
											<td><strong>Thanks for Your valuable Feedback!</strong></td>
											<%} %>    
											
											

										</tr>
										<%
											}
										%>
										<%
											}
										%>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>





	<script src="assets/css/table/datatables.min.js"></script>
	<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
	<script src="assets/css/table/datatables-init.js"></script>

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>
	
	<!-- Feedback Links -->
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	


	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>


	<%
		}
	%>
</body>
</html>