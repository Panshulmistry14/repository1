<!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">



  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html"><span>Law</span>Mart</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="Logo of LawMart" class="img-fluid" height="50px" width="50px"></a> -->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="LawFirmHomePage.jsp">Home</a></li>

          <li><a href="EditStakeholder?Page1=lawfirm">Profile</a></li>


          <li><a href="SingleLFServices.jsp">Services</a></li>
          

          <li class="drop-down"><a href="#">Client Details</a>
          
          <ul>
          
              <li><a href="LFlaClients">Legal Advice Clients</a></li>
              <li><a href="LFBookingDetails">Booking Consultation Clients</a></li>
           </ul>
          
          </li>
          <li><a href="GetLFBookingConsultationData">Booking Consultations</a></li>
          <li><a href="GetLAData">Legal Advice</a></li>
          <li><a href="SelectFeedbackData?fbuser=lawfirm">Feedback</a></li>
          <li><a href="AboutUs.jsp?abtuser=lawfirm">About Us</a></li>
          <li><a href="Logout">Sign out</a></li>
          


        </ul>
      </nav><!-- .nav-menu -->

      

    </div>
  </header><!-- End Header -->