<%@page import="com.lawmart.bean.Payment"%>
<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="utils.PaytmConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,com.paytm.pg.merchant.CheckSumServiceHelper"%>
<%
Enumeration<String> paramNames = request.getParameterNames();

Map<String, String[]> mapData = request.getParameterMap();
TreeMap<String,String> parameters = new TreeMap<String,String>();
String paytmChecksum =  "";
while(paramNames.hasMoreElements()) {
	String paramName = (String)paramNames.nextElement();
	if(paramName.equals("CHECKSUMHASH")){
		paytmChecksum = mapData.get(paramName)[0];
	}else{
		parameters.put(paramName,mapData.get(paramName)[0]);
	}
}
boolean isValideChecksum = false;
String outputHTML="";
try{
	isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(PaytmConstants.MERCHANT_KEY,parameters,paytmChecksum);
	if(isValideChecksum && parameters.containsKey("RESPCODE")){
		if(parameters.get("RESPCODE").equals("01")){
			outputHTML = parameters.toString();
			System.out.println("Outputhtml:"+outputHTML);
		}else{
			outputHTML="<b>Payment Failed.</b>";
		}
	}else{
		outputHTML="<b>Checksum mismatched.</b>";
	}
}catch(Exception e){
	outputHTML=e.toString();
}

System.out.println("Parameters:"+parameters.toString());
String p=String.valueOf(parameters);
%>
<%LawMartService ls=new LawMartServiceImpl(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment Status</title>

<style>

.b1{
 
  width: auto;
  background: #1bbd36;
  color: #fff;
  text-transform: uppercase;
  font-weight: 900;
  padding: 16px 50px;
  border: none;
  margin-top: 37px;
  cursor: pointer;
  text-decoration: none;
 }
</style>
</head>
<body>
<%
String[] s=p.split(",");
String s1="",s2="",s3="",s4="",s5="",s6="",s7="";
String s8="",s9="",s10="",s11="",s12="";
String status="",amtt="",amtt1="",bn="";
int amt=0;
int cnt=0,index=0;
for(int i=0;i<s.length;i++)
{
	cnt++;
	System.out.println("S:"+s[i]);
	if(cnt==1)
	{
		s3=s[i];
	}
	if(cnt==2)
	{
		s4=s[i];
	}
	if(cnt==3)
	{
		s6=s[i];
	}
	if(cnt==4)
	{
		s7=s[i];
	}
	if(cnt==5)
	{
		s8=s[i];
	}
	
	if(cnt==6)
	{
		s1=s[i];
	}
	
	if(cnt==7)
	{
		s9=s[i];
	}
	if(cnt==8)
	{
		s10=s[i];
	}
	if(cnt==9)
	{
		s11=s[i];
	}
	
		
	if(cnt==10)
	{
		s5=s[i];
	}
	if(cnt==11)
	{
		amtt=s[i];
	}
	if(cnt==12)
	{
		s2=s[i];
	}
	
	if(cnt==13)
	{
		s12=s[i];
	}
}
status=s5.substring(12, s5.length());
for(int k=0;k<amtt.length();k++)
{
	if(amtt.charAt(k)=='.')
	{
		index=k;
	}
}
//BankName
bn=s3.substring(10,s3.length());
System.out.println("bankname:"+bn);
//bank txn id
String banktxId=s4.substring(11,s4.length());
System.out.println("bank txn id:"+banktxId);
//currency
String curr=s6.substring(10,s6.length());
System.out.println("curr:"+curr);
//gateway name
String gwname=s7.substring(13,s7.length());
System.out.println("gateway name:"+gwname);
//merchant id
String mid=s8.substring(5,s8.length());
System.out.println("mid:"+mid);
//payment mode
String paymode=s9.substring(13,s9.length());
System.out.println("paymode:"+paymode);
//resp code
String respCode=s10.substring(10,s10.length());
System.out.println("respCode:"+respCode);
//resp message
String respmsg=s11.substring(9,s11.length());
System.out.println("respmesage:"+respmsg);
//txn id
String txnId=s12.substring(7,s12.length()-1);
System.out.println("txn id:"+txnId);
//Txn Amount
amtt1=amtt.substring(11,index);
amt=Integer.parseInt(amtt1);
System.out.println("amt is:"+amt);
%>
<% if(status.equals("SUCCESS")){%>
<h1 align="center">YOUR TRANSACTION IS DONE SUCCESSFULLY.</h1>
<img src="assets/img/success-icon.png" style="position: absolute; left: 45%;top:37%" height="150px" width="150px" alt="payment done img">
<%} else { %>
<h1 align="center">YOUR TRANSACTION IS FAILED.</h1>
<img src="assets/img/faliure-icon.png" style="position: absolute; left: 45%;top:37%" height="150px" width="150px" alt="payment done img">

<%} %>
<br>
<br>
<% 
//status
System.out.println("Status:"+status);
//orderid
String odId=s1.substring(9, s1.length());
System.out.println("Order id:"+odId);
//date
String orDate=s2.substring(9, 19);
System.out.println("Order Date:"+orDate);

Payment payment = new Payment();
payment.setOrderId(odId);
payment.setBankId(banktxId);
payment.setBankName(bn);
payment.setCategory("Legal Advice");
payment.setCurr(curr);
payment.setGateName(gwname);
payment.setMerId(mid);
payment.setPayMode(paymode);
payment.setPayStatus(status);
payment.setRespCode(Integer.parseInt(respCode));
payment.setRespMsg(respmsg);
payment.setTxnAmt(amt);
payment.setTxnId(txnId);
payment.setTxnDate(orDate);


String msg1=ls.insertPayDetails(payment);
System.out.println("pay details message:"+msg1);

String msg=ls.updateStatus(odId, status, orDate);
System.out.println("Updated status:"+msg); 

%>

<a class="b1" href="ClientLAData" style="position: absolute;left: 42%;top:70%">Back to the site</a>
</body>
</html>