<%@page import="java.util.ArrayList"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>LawMart</title>
<meta content="" name="descriptison">
<meta content="" name="keywords">

<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">

<!-- =======================================================
  * Template Name: Company - v2.1.0
  * Template URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>



	<%

		String user = request.getParameter("abtuser");
	if (user!=null) {
		System.out.println("abtuser.... "+user);
	%>

	<%if(user.equals("visitor")){ %>
	
	<%@ include file="HomePageHeader.jsp"%>
	
	<%} else if(user.equals("client")){%>
	
	  <%@ include file="ClientHeader.jsp" %>
	
	<%} else if(user.equals("lawfirm")) {%>
	
	<%@ include file="LawfirmHeader.jsp" %>
	
	
	<%} %>

	<%
		}
	else
	{
		System.out.println("abtuser is null");
	}
	%>

	<main id="main">

		<!-- ======= About Us Section ======= -->
		<br> <br> <br>
		<section id="about-us" class="about-us">
			<div class="container" data-aos="fade-up">

				<div class="section-title">
					<h2>
						About Us</strong>
						
					</h2>
				</div>

				<div class="row content">
					<div class="col-lg-6" data-aos="fade-right">
						<h2>LawMart</h2>
						<h3>A portal where clients get connected with the Law Firms</h3>
					</div>
					<div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
						<p>Lawmart is a portal which provides its user to connect with
							the different law firms at a same place. So that the user will
							have a seamless experience of getting the law services (law
							Services like Rent Agreement, Power Of Attorney, Sale deed
							related advice and execution).</p>
						<ul>
							<li><i class="ri-check-double-line"></i> User will be able
								to save a lot of time.</li>
							<li><i class="ri-check-double-line"></i>Different Law Firms
								can provide there services online</li>
							<li><i class="ri-check-double-line"></i>Client gets more
								number of options</li>
							<li><i class="ri-check-double-line"></i> Law Firms can
								manage the service related documents easily</li>
						</ul>

					</div>
				</div>

			</div>
		</section>
		<!-- End About Us Section -->

	

	</main>
	<!-- End #main -->
	<%@ include file="HomePageFooter.jsp"%>
</body>
</html>