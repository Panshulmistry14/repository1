<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.bean.Area"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <title>Admin Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">



  <style>

  input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

  </style>

  <link href="profilecss/bootstrap2.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="profilecss/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

 <script src="profilejs/bootstrap2.min.js"></script>
  <script src="profilejs/jquery2.min.js"></script>
   <script src="profilejs/bootstrap.min.js"></script>
 <script src="profilejs/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 </head>



<body style="background-color: #f7f7f7;">

<%@ include file="Servicesheader.jsp" %>

<br>
<br>
<br>
<br>
<br>
<br> 

<%User user=(User)request.getAttribute("user"); %>   
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1 style="font-size: 30px;"><%=user.getUserName().toUpperCase() %> </h1></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
          
          
          <!-- <ul class="list-group">
            <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i></li>
             <li class="list-group-item text-right"><span class="pull-left"><strong>Total Clients</strong></span> 0</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Legal Advices Given</strong></span> 0</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Feedbacks Received</strong></span> 0</li>
          </ul>  -->
               
         
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><h2 style="font-size: 30px;">Edit Profile</h2></li>
            </ul> 
            
		<%String msg1=(String)request.getAttribute("msg1");%>  
		<%if(msg1!=null){ %>
		<span id="msg">msg1</span>
		<%} %>
              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                
                
                  <form class="form" action="EditStakeholder?Page1=admin" method="post" id="adminprofile">
                      <div class="form-group">
                        
                         <!--<label for="admin_id">Admin ID</label>-->
                                <input type="hidden" class="form-input" name="admin_id" id="admin_id" value="<%=user.getUserId()%>"/>
                         
                          <div class="col-xs-6">
                              <label for="first_name"><h4 style="font-size: 20px;">First name</h4></label>
                              <%String name=user.getUserName(); 
                              String fullname[]=name.split("\\s");
                              %>
                              <input style="font-size: 20px;" type="text" class="form-control" name="first_name"  id="first_name" placeholder="first name" required="required" value="<%=fullname[0]%>">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4 style="font-size: 20px;">Last name</h4></label>
                              <input type="text" style="font-size: 20px;" class="form-control" name="last_name" id="last_name" placeholder="last name" required="required" value="<%=fullname[1]%>">
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4 style="font-size: 20px;">Phone</h4></label>
                              <input type="number" style="font-size: 20px;" class="form-control" name="phone" id="phone" placeholder="enter phone" required="required" value="<%=user.getUserPhone()%>">
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h4 style="font-size: 20px;">Address</h4></label>
                              <!-- <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any."> -->
                            <textarea class="form-control" style="font-size: 20px;" name="address" id="address" required="required"><%=user.getUserAddress()%></textarea>
                            </div>
                      </div>
                      <div class="form-group">
                          
                        <div class="col-xs-6">
                          <label for="password2"><h4 style="font-size: 20px;">Password</h4></label>
                            <input type="password" style="font-size: 20px;" class="form-control" name="password1" id="password1" placeholder="enter password" required="required" value="<%=user.getUserPassword()%>">
                        </div>
                      </div>
                    
                      <div class="form-group">
                          
                        <div class="col-xs-6">
                          <label for="email"><h4 style="font-size: 20px;">Email</h4></label>
                          <input type="email"style="font-size: 20px;"  class="form-control" name="email" id="email" placeholder="you@email.com" required="required" value="<%=user.getUserEmail()%>">
                      </div>
                    </div>  
                    
                    
                     <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="areapin"><h4 style="font-size: 20px;">Area Pincode</h4></label>
                            	<%List<Area> areaList=(List)request.getAttribute("areas"); %> 
								<select style="font-size: 20px;height: 30px;" class="form-control" id="areapin" name="areapin">
 								
<!-- 								<select id="select-area"  name="area" required="required"> -->
<!-- 								<option value=""> </option> -->
								<%int i=1; %>
								<%for(Area a: areaList){ %>
									<%if(user.getAreaId()==i)
									{
									%>
										<option selected="selected" value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
									<%}%>
									<option value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
 								<%i++; %> 
  								<%} %>
						  		</select>
                            </div>
                      </div> 
                    
                     
                      <div class="form-group" style="position: absolute;top: 109%">
                           <div class="col-xs-12">
                                <br>
                              	<button style="font-size: 20px;" class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button style="font-size: 20px;" class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
              
              

              
             </div><!--/tab-pane-->
             
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
    </div>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br> 
<%@ include file="HomePageFooter.jsp" %>
</body>
</html>                                