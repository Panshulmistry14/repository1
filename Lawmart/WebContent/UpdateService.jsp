<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.Area"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Services</title>

<!--     Font Icon -->
    <link rel="stylesheet" href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="assets/vendor/Registration/jquery-ui/jquery-ui.min.css">

<!--     Main css -->
    <link rel="stylesheet" href="assets/css/Registration/style2.css">
 
 <style type="text/css">
 
 .cancel{
 
  width: auto;
  background: gray;
  color: #fff;
  text-transform: uppercase;
  font-weight: 900;
  padding: 16px 50px;
  float: right;
  border: none;
  margin-top: 37px;
  cursor: pointer;
 }
 
.cancel:hover{
color: white;
} 
 
 .form-submit:hover{
 
 background: #57b846;
 
 
 }
 
 </style>
 
        
</head>
<body>
<%@ include file="Servicesheader.jsp" %>
<br><br><br><br>
    <h4 class="Signup-form-title" style="position: absolute;left: 45%">
        UPDATE SERVICES
    </h4>  
    <%Services services=(Services)request.getAttribute("services");%>
       <div class="main">

        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                    <form method="POST" action="AdminEditData?Page2=service" id="signup-form" class="signup-form">
                        <!--  <div class="form-row"> -->
                            <div class="form-group">
                                <label for="service_name">Service Name</label>
                                <input type="text" required="required" class="form-input" name="service_name" id="service_name" value="<%=services.getServiceName()%>"/>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Service Description</label>
                                <textarea rows="5"  cols="55" required="required" class="form-input" name="service_desc" id="service_desc" value="<%=services.getServiceDesc()%>"><%=services.getServiceDesc()%></textarea>
                            </div>
                        <!-- </div> -->
                        <div class="form-group">
                                <!-- <label for="service_id">Service Id</label> -->
                                <input type="hidden" class="form-input" name="service_id" id="service_id" value="<%=services.getServiceId()%>"/>
                            </div>
                            
                        <div class="form-group">
                        	<a href="AdminManage?Page1=services" class="cancel" style="position: absolute;right: 20%">Cancel</a>
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="assets/vendor/Registration/jquery/jquery.min.js"></script>
    <script src="assets/vendor/Registration/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="assets/js/Registrationjs/main.js"></script>
    
    <!-- selecting area -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

	<script type="text/javascript">
    $(document).ready(function () {
     $('select').selectize({
         sortField: 'text'
     });
 });
    </script>
<%@ include file="HomePageFooter.jsp" %>    
    
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>