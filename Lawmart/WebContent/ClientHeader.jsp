<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center">

		<h1 class="logo mr-auto">
			<a href="#"><span>Law</span>Mart</a>
		</h1>

		<nav class="nav-menu d-none d-lg-block">
			<ul>
				<li class="active"><a href="ClientHomePage.jsp">Home</a></li>

				<li><a href="EditStakeholder?Page1=client">Profile</a></li>


				<li><a href="ClientServices.jsp">Services</a></li>



				<li><a href="ClientLawFirms.jsp">Law Firm</a></li>
				<li><a href="GetBookingConsultationdata">Booking
						Consultations</a></li>

				<li class="drop-down"><a href="">Legal Advice</a>
					<ul>
						<li><a href="LegalAdvice.jsp">Legal Advice</a></li>
						<li><a href="ClientLAData">View Response</a></li>
					</ul></li>


				<li><a href="SelectFeedbackData?fbuser=client">Feedback</a></li>
				<li><a href=AboutUs.jsp?abtuser=client>About Us</a></li>
				<li><a href="Logout">Sign Out</a></li>




			</ul>
		</nav>
		<!-- .nav-menu -->



	</div>
</header>
<!-- End Header -->
