<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin-Dashboard</title>
<%LawMartService ls = new LawMartServiceImpl(); %>
<%
String currDate = java.time.LocalDate.now().toString();
System.out.println("Current Date:"+currDate);
String[] curd = currDate.split("-");
System.out.println("Current month:"+curd[1]);
String a = curd[1];
%>
<%List<String> monthList = ls.getBookDates(a); %>
<%
List<String> one = new ArrayList<String>();
List<String> two = new ArrayList<String>();
List<String> three = new ArrayList<String>();
List<String> four = new ArrayList<String>();
List<String> five = new ArrayList<String>();
List<String> six = new ArrayList<String>();
int onel=0,twol=0,threel=0,fourl=0,fivel=0,sixl=0;

if(monthList!=null)
{
	for(String s1 : monthList)
	{
		System.out.println("Dashboard date:"+s1);
		String[] dates = s1.split("-");
		int d = Integer.parseInt(dates[2]);
		System.out.println("d:"+d);
		if(d>=1 && d<=5)
		{
			String o = String.valueOf(d);
			one.add(o);
		}
		else if(d>=6 && d<=10)
		{
			String o1 = String.valueOf(d);
			two.add(o1);
		}
		else if(d>=10 && d<=15)
		{
			String o2 = String.valueOf(d);
			three.add(o2);
		}
		else if(d>=16 && d<=20)
		{
			String o3 = String.valueOf(d);
			four.add(o3);
		}
		else if(d>=20 && d<=25)
		{
			String o4 = String.valueOf(d);
			five.add(o4);
		}
		else if(d>=25 && d<=30)
		{
			String o5 = String.valueOf(d);
			six.add(o5);
		}
	}
	onel = one.size();
	twol = two.size();
	threel = three.size();
	fourl = four.size();
	fivel = five.size();
	sixl = six.size();
}
else
{
	System.out.println("No DATES FOUND!");
}
%>
	
        <link rel="stylesheet" href="assets/admindashboard/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/admindashboard/style.css">
        
          <script src="https://cdn.anychart.com/releases/8.0.0/js/anychart-base.min.js"></script>
	
        <style>
            html, body, #container12 {
        width: 100%;
        height: 20%;
        margin: 0;
        padding: 0;
        }
        </style>

		
        <script>

            anychart.onDocumentReady(function() {
            
            // set the data
            var data = {
              header: ["Date", "Total Cases"],
              rows: [
                ["1-5", <%=onel%>],  //50                    //give the values here for the bar diagram
                ["6-10", <%=twol%>],//100
                ["11-15", <%=threel%>],//150
                ["16-20", <%=fourl%>],//200
                ["21-25", <%=fivel%>],//250
                ["26-30", <%=sixl%>],//300
            ]};
            
            // create the chart
            var chart = anychart.bar();
            
            // add data
            chart.data(data);
            
            // set the chart title
            chart.title("Booking Consultation Number of Cases this month");
            
            
            // draw
            chart.container("container12");
            chart.draw();
            });
            </script>
            
<!-- 	Pie chart calculated values             -->
		<%String totf = ls.countFeedBack(); %>
		<%int totfb = Integer.parseInt(totf); %>
		
		<%String vg = ls.countVeryGoodFB(); %>
		<%System.out.println("Very Good Feedback count:"+vg); %>
		<%int vg1 = Integer.parseInt(vg); %>
		<%int pervg = (vg1*100)/totfb; %>
		
		<%String g = ls.countGoodFB(); %>
		<%System.out.println("Good Feedback count:"+g); %>
		<%int g1 = Integer.parseInt(g); %>
		<%int perg = (g1*100)/totfb; %>
		
		<%String m = ls.countMediocreFB(); %>
		<%System.out.println("Mediocre Feedback count:"+m); %>
		<%int m1 = Integer.parseInt(m); %>
		<%int perm = (m1*100)/totfb; %>
		
		<%String vb = ls.countVeryBadFB(); %>
		<%System.out.println("Very Bad Feedback count:"+vb); %>
		<%int vb1 = Integer.parseInt(vb); %>
		<%int pervb = (vb1*100)/totfb; %>
		
		<%String b = ls.countBadFB(); %>
		<%System.out.println("Bad Feedback count:"+b); %>	
		<%int b1 = Integer.parseInt(b); %>
		<%int perb = (b1*100)/totfb; %>
		
		
<!-- Pie chart calulated values -->

        <script>
            window.onload = function () {
            
            var chart = new CanvasJS.Chart("chartContainer", {
                exportEnabled: true,
                animationEnabled: true,
                title:{
                    text: "Feedback Ratings given by Clients"
                },
                legend:{
                    cursor: "pointer",
                    itemclick: explodePie
                },
                data: [{
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{name}: <strong>{y}%</strong>",
                    indexLabel: "{name} - {y}%",
                    dataPoints: [
                        { y: <%=perm%>, name: "Mediocre" }, //27   //give the values here for the pie chart
                        { y: <%=pervb%>, name: "Very bad" },//27
                        { y: <%=pervg%>, name: "Very Good" },//27
                        { y: <%=perg%>, name: "Good" },//9
                        { y: <%=perb%>, name: "Bad"}//18
                    ]
                }]
            });
            chart.render();
            }
            
            function explodePie (e) {
                if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
                    e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
                } else {
                    e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
                }
                e.chart.render();
            
            }
            </script>
            
               <style>
        #div1{
            background-color: #48a848;
        }

        #div2{
            background-color: #ffdf00;
        }

         #div3{

            background-color: #8a2be2;
        }

        #div4{

            background-color: #FF1476;
        }

        .commondiv{
            height: 150px;
            width: 20%;
            margin: 20px 35px;
            color: white;
            font-size: 15px;
            padding-left: 10px;
            padding-top: 10px;
            display: inline-block;
            border-radius: 20px;

			border: 1px solid;
			box-shadow: 5px 10px #888888;

        }

    </style>
        
        


</head>
<body style="background-color: white;">

	<%@include file="Servicesheader.jsp"%>

	<%String role="Client"; %>
	<%String role2="Law Firm"; %>
	<%String ans = ls.countClients(role); %>
	<%String ans1 = ls.countLawfirm(role2); %>
	<%String ans2 = ls.countLA();%>
	<%String ans3 = ls.countBC(); %>
	
<!--	Starting Progress Bar legal advice-->
	
	<%String ol = ls.countOnlinePayment(); %>
	 <%int ol1 = Integer.parseInt(ol);%>
	 <%int totla = Integer.parseInt(ans2); %>
	 <%int perol =(ol1*100)/totla; %>
	 <%String ol2 = String.valueOf(perol); %>
	 <%String online = ol2+" % "+" Online Payment"; %>
	 
	 <%String of = ls.countOfflinePayment(); %>
	 <%int of1 = Integer.parseInt(of); %>
	 <%int perof = (of1*100)/totla; %>
	 <%String of2 = String.valueOf(perof); %>
	 <%String offline = of2+" % "+" Offline Payment"; %>
	  
	  <%String fail = ls.countFailure(); %>
	  <%int f = Integer.parseInt(fail); %>
	  <%int perfail = (f*100)/totla; %>
	  <%String fail1 = String.valueOf(perfail); %>
	  <%String failure = fail1+" % "+" Failed"; %>
	  
	  <%int persuc = 100-perfail; %>
	  <%String suc1 = String.valueOf(persuc); %>
	  <%String success = suc1+" % "+" Success"; %>
	  
<!-- 	  Ending progress bar legal advice -->

<!-- Starting progress bar Booking Consultation -->
	<% String obc = ls.countOnlineBC(); %>
	<%int obc1 = Integer.parseInt(obc);%>
	 <%int totbc = Integer.parseInt(ans3); %>
	 <%int perobc =(obc1*100)/totbc; %>
	 <%String obc2 = String.valueOf(perobc); %>
	 <%String onlinebc = obc2+" % "+" Online Payment"; %>
	 
	 <%String ofbc = ls.countOfflineBC(); %>
	<%int ofbc1 = Integer.parseInt(ofbc);%>
	 <%int perofbc =(ofbc1*100)/totbc; %>
	 <%String ofbc2 = String.valueOf(perofbc); %>
	 <%String offlinebc = ofbc2+" % "+" Offline Payment"; %>
	 
	  <%String failbc = ls.countFailureBC(); %>
	  <%int f1 = Integer.parseInt(failbc); %>
	  <%int perfailbc = (f1*100)/totbc; %>
	  <%String failbc1 = String.valueOf(perfailbc); %>
	  <%String failurebc = failbc1+" % "+" Failed"; %>
	  
	  <%int persucbc = 100-perfailbc; %>
	  <%String sucbc1 = String.valueOf(persucbc); %>
	  <%String successbc = sucbc1+" % "+" Success"; %>
	  
	  
<!-- Ending progress bar Booking Consultation -->

<!-- 	Boxes -->
			<br><br><br><br>
	  <div class="section-title">
          <h2>DASHBOARD</strong></h2>
        </div>
	
    <div class="main" style="position: relative;">
    
    <div id="div1" class="commondiv">
		<%if(ans!=null) {%>
        <h3><%=ans %></h3>
        <%} else { %>
        <h3>Unable to retrieve the total number of Clients!</h3>
		<% } %>
        <span>Total Clients</span>



    </div>

    <div id="div2" class="commondiv">
		<%if(ans1!=null) {%>
        <h3><%=ans1 %></h3>
		<%} else { %>
        <h3>Unable to retrieve the total number of Lawfirm!</h3>
		<% } %>
        <span>Total Law Firms</span>

    </div>

    <div id="div3" class="commondiv">
		<%if(ans2!=null) {%>
        <h3><%=ans2 %></h3>
		<%} else { %>
        <h3>Unable to retrieve the total number of Legal Advice!</h3>
		<% } %>
        
        <span>Total number of Legal Advices taken</span>

    </div>

    <div id="div4" class="commondiv">
		<%if(ans3!=null) {%>
        <h3><%=ans3 %></h3>
        <%} else { %>
        <h3>Unable to retrieve the total number of Booking Counsultation!</h3>
		<% } %>
        

        <span>Total number of Booking Consultations</span>

    </div>
    
    
    </div>

<br><br><br><br><br><br><br><br><br>
      


        <div style="position: relative;">

        <div class="card" style="width: 40%;left: 8%;">
            <div class="card-header">
            <h4>Mode of Payment for Booking Consultation</h4>
            </div>
            <div class="card-body">
            <p class="muted">Shows the percentage of Payment done using different modes</p>
             <div class="progress mb-2">
             <%if(perobc!=0) { %>
            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=perobc%>%" aria-valuenow="<%=perobc%>" aria-valuemin="0" aria-valuemax="100"><%=onlinebc%></div>
            <% } else { %>
            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
            <% } %>
            </div>
           
            <div class="progress mb-2">
            <%if(perofbc!=0) { %>
            <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=perofbc%>%" aria-valuenow="<%=perofbc%>" aria-valuemin="0" aria-valuemax="100"><%=offlinebc%></div>
            <% } else { %>
            <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
           <% } %> 
            </div>
          
            </div>
            </div>

            <div class="card" style="width: 40%;position: absolute;left: 52%;top: 1%;">
                <div class="card-header">
                <h4>Mode of Payment for Legal Advice</h4>
                </div>
                <div class="card-body">
                    <p class="muted">Shows the percentage of Payment done using different modes</p>
                    <div class="progress mb-2">
                    <%if(perol!=0){ %>
                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=perol%>%" aria-valuenow="<%=perol%>" aria-valuemin="0" aria-valuemax="100"><%=online%></div>
                <%} else { %>
                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
                <%} %>
                </div>
               
                <div class="progress mb-2">
                 <%if(perof!=0){ %>
                <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=perof%>%" aria-valuenow="<%=perof%>" aria-valuemin="0" aria-valuemax="100"><%=offline%></div>
                 <%} else { %>
                 <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
                <%} %>
                </div>
              
                </div>
                </div>

            </div>



            <div style="position: relative;">

                <div class="card" style="width: 40%;left: 8%;">
                    <div class="card-header">
                    <h4>Payment status for Booking Consultation</h4>
                    </div>
                    <div class="card-body">
                    <p class="muted">Shows the Payment status for the Payments done using Online payment mode through the System</p>
                    <div class="progress mb-2">
                    <%if(perfailbc!=0) {%>
                    <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=perfailbc%>%" aria-valuenow="<%=perfailbc%>" aria-valuemin="0" aria-valuemax="100"><%=failurebc%></div>
                    <%} else { %>
                    <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
                    <%} %>
                    </div>

                   
                   <div class="progress mb-2">
                   <%if(persucbc!=0) {%>
                    <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=persucbc%>%" aria-valuenow="<%=persucbc%>" aria-valuemin="0" aria-valuemax="100"><%=successbc %></div>
                    <% } else { %>
                     <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
                   <% } %>
                    </div>

                  
                    </div>
                    </div>
        
                    <div class="card" style="width: 40%;position: absolute;left: 52%;top: 1%;">
                        <div class="card-header">
                            <h4>Payment status for Legal Advice</h4>
                        </div>
                        <div class="card-body">
                            <p class="muted">Shows the Payment status for the Payments done using Online payment mode through the System</p>
                            <div class="progress mb-2">
                            <%if(perfail!=0) { %>
                                <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=perfail%>%" aria-valuenow="<%=perfail %>" aria-valuemin="0" aria-valuemax="100"><%=failure %></div>
                                <% } else { %>
                                <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
                                <%} %>
                                </div>
            
                               
                               <div class="progress mb-2">
                               <% if(persuc!=0) {%>
                                <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" style="width: <%=persuc%>%" aria-valuenow="<%=persuc%>" aria-valuemin="0" aria-valuemax="100"><%=success%></div>
                                <% } else { %>
                                <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">NO PERCENTAGE FOUND!</div>
                                <%} %>
                                </div>
                      
                        </div>
                        </div>
        
                    </div>


            <br><br><br><br>

            <div id="chartContainer" style="height: 370px; max-width: 520px; margin: 10px 480px;"></div>
            <script src="assets/admindashboard/canvasjs.min.js"></script>


            <br><br><br><br>

            <div id="container12"></div>
            
            	<%@ include file="HomePageFooter.jsp"%>
            
	


</body>
</html>