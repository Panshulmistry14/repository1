<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.User"%>
<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(1000000);
%>
<%
	LawMartService ls = new LawMartServiceImpl();
%>
<%
	LegalAdvice la = (LegalAdvice) request.getAttribute("legaladvice");
%>
<%
	User user = ls.getUser(la.getUser_id());
%>
<%
	LawFirm lf = ls.getSingleLF(la.getLf_id());
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Legal Advice Payment Details</title>

<style>
#divstyle {
	height: 50%;
	width: 25%;
	margin-left: 38%;
	margin-top: 5%;
	border: 1px solid #888888;
	box-shadow: 7px 12px #888888;
	background-color: white;
	border-radius: 20px;
}

body {
	background-color: #f7f7f7;
}

h1 {
	margin-top: 55px;
	font-family: sans-serif;
	text-align: center;
}

button {
	margin-top: 4%;
	font-family: sans-serif;
	margin-left: 38%;
	width: 25%;
	height: 30%;
	border-radius: 20px;
}

.p1 {
	/* padding:0 14%;  */
	vertical-align: top;
	font-family: sans-serif;
	font-size: 20px;
	font-weight: 500;
}

#innerdiv {
	margin-top: 20%;
	margin-bottom: 20%;
	padding-left: 10%;
}

button:hover {
	background-color: white;
}
</style>



</head>
<body>
		<h1>Checkout Page</h1>


	<%
		String or = "ORDS_" + randomInt;
	%>
	
	
	
	
	<div id="divstyle">

		<div id="innerdiv">

			<p class="p1" class="cmnp">
				Order id&emsp;&emsp;&emsp;
				<%=or%></p>



			<p class="p1" class="cmnp">
				Name &emsp; &emsp; &emsp;
				<%
					if (user.getUserName() != null) {
				%>
				<%=user.getUserName()%>
				<%
					} else {
				%>
				Client name not found!
				<%
					}
				%>
			</p>



			<p class="p1" class="cmnp">
				Firm &emsp; &emsp; &emsp; &ensp;
			<%
					if (lf.getUserName() != null) {
				%>
				<%=lf.getUserName()%>
				<%
					} else {
				%>
				Law firm name not found!
				<%
					}
				%>
			</p>


			<p class="p1" class="cmnp">
				Phone &emsp; &emsp; &emsp;
				<%
					if (user.getUserPhone() != null) {
				%>
				<%=user.getUserPhone()%>
				<%
					} else {
				%>
				User's phone number not found!
				<%
					}
				%>
			</p>


			<p class="p1" class="cmnp">
				Email &emsp; &emsp; &emsp;&nbsp;
				<%
					if (user.getUserEmail() != null) {
				%>
				<%=user.getUserEmail()%>
				<%
					} else {
				%>
				User's Email not found!
				<%
					}
				%>
			</p>
			<p class="p1" class="cmnp">
				Fees &emsp; &emsp; &emsp;&ensp;
				<%
					if (la.getAdv_fees() != 0) {
				%>
				<%=la.getAdv_fees()%> Rs
				<%
					} else {
				%>
				Fees not found!
				<%
					}
				%>
			</p>

		</div>

	</div>



	<form method="post" action="pageRedirect.jsp">

		<table>
			<tbody>
				<tr>


					<%
						String msg = ls.insertOrderId(la.getAdv_id(), or);
					%>
					<%
						if (or != null) {
					%>
					<td><input type=hidden id="ORDER_ID" tabindex="1"
						maxlength="20" size="20" name="ORDER_ID" autocomplete="off"
						value="<%=or%>"></td>
					<%
						} else {
					%>
					<td>Order Id not found!</td>
					<%
						}
					%>

					<%
						if (la.getUser_id() != 0) {
					%>
					<td><input type=hidden id="CUST_ID" tabindex="2"
						maxlength="30" size="12" name="CUST_ID" autocomplete="off"
						value="<%=la.getUser_id()%>"></td>
					<%
						} else {
					%>
					<td>Customer Id not found!</td>
					<%
						}
					%>

					<td><input type=hidden id="INDUSTRY_TYPE_ID" tabindex="4"
						maxlength="12" size="12" name="INDUSTRY_TYPE_ID"
						autocomplete="off" value="Retail"></td>

					<td><input type=hidden id="CHANNEL_ID" tabindex="4"
						maxlength="12" size="12" name="CHANNEL_ID" autocomplete="off"
						value="WEB"></td>
					<%
						if (la.getAdv_fees() != 0) {
					%>
					<td><input type=hidden title="TXN_AMOUNT" tabindex="10"
						type="text" name="TXN_AMOUNT" value="<%=la.getAdv_fees()%>">
					</td>
					<%
						} else {
					%>
					<td>Fees not found!</td>
					<%
						}
					%>
					<button>
						<strong>Pay <%=la.getAdv_fees()%> Rs
						</strong>
					</button>
				</tr>
			</tbody>
		</table>


	</form>


</body>
</html>