<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin - Legal Advice</title>




<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet" href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

</head>
<body>
<%@include file="Servicesheader.jsp"%>
<%List<LegalAdvice> laList=(List)request.getAttribute("laDataAdmin");%>
<br><br><br>


<div>

<div class="content mt-3">
<div class="animated fadeIn">
<div class="row">
<div class="col-md-12">
<div class="card">

<div class="card-header">
<strong class="card-title">Legal Advice Table</strong>
</div>
<div class="card-body">
<table id="bootstrap-data-table" class="table table-striped table-bordered">
<thead>
<tr>
<th>Client Name</th>
<th>Client issue</th>
<th>Lawfirm Name</th>
<th>Phone Number</th>
<th>Payment Status</th>
<th>Firm Response </th>
<th>Fees</th>
<th>Order Id</th>
<th>Payment Date</th>
<!-- <th>Give Response/<br>Mention Fees</th> -->

</tr>
</thead>

<tbody>
<% if(laList!=null){
for(LegalAdvice la:laList)
{
%>
<tr>
<%LawMartService ls=new LawMartServiceImpl();%>
<%User user=ls.getUser(la.getUser_id()); %>
<%LawFirm lf=ls.getSingleLF(la.getLf_id()); %>

<%if(user!=null){%>
<td><%=user.getUserName()%></td>	
<%}else{ %>
<td>User name not found!</td>
<%} %>

<%if(la.getAdv_issue()!=null) {%>
<td><%=la.getAdv_issue()%></td>
<%}else { %>
<td>Advice Issue not Found!</td>
<%} %>

<%if(lf.getUserName()!=null) {%>
<td><%=lf.getUserName()%></td>
<%}else { %>
<td>Lawfirm Name not Found!</td>
<%} %>

<%if(user!=null){%>
	<td><%=user.getUserPhone()%></td>
	
<%}else{ %>
<td>User phone number not found!</td>
<%} %>

<%if(la.getPayment_status()!=null) {%>
<td><%=la.getPayment_status()%></td>
<%} else { %>
<td>Payment Status not found!</td>
<%} %>

<%if(la.getFirm_responses()!=null){%>
	<td><%=la.getFirm_responses()%></td>
	
	
<%}else{ %>
<td>Firm response not found!</td>
<%} %>


<%if(la.getAdv_fees()!=0){%>
	<td><%=la.getAdv_fees()%></td>
	
	
<%}else{ %>
<td>Fees Not Found!</td>
<%} %>

<%if(la.getOrderId()!=null){%>
	<td><%=la.getOrderId()%></td>
	
	
<%}else{ %>
<td>Oder Id not found!</td>
<%} %>

<%if(la.getPayDate()!=null){%>
	<td><%=la.getPayDate()%></td>
	
	
<%}else{ %>
<td>Payment Date not found!</td>
<%} %>

</tr>
<%} %>
<%} %>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<script src="assets/css/table/datatables.min.js"></script>
<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
<script src="assets/css/table/datatables-init.js"></script>

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>





</body>
</html>
