
<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet"
	href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">

<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">



<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center">

		<h1 class="logo mr-auto">
			<a href="index.html"><span>Law</span>Mart</a>
		</h1>
		<!-- Uncomment below if you prefer to use an image logo -->
		<!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="Logo of LawMart" class="img-fluid" height="50px" width="50px"></a> -->

		<nav class="nav-menu d-none d-lg-block">
			<ul>
				<li class="active"><a href="AdminHomePage.jsp">Home</a></li>

				<li><a href="EditStakeholder?Page1=admin">Profile</a></li>


				<!--  <li><a href="#">Services</a></li> -->



				<li class="drop-down"><a href="">Manage</a>
					<ul>
						<li><a href="AdminManage?Page1=client">Manage Clients</a></li>
						<li><a href="AdminManage?Page1=lawfirm">Manage LawFirms</a></li>
						<li><a href="AdminManage?Page1=services">Manage Services</a></li>
						<li><a href="AdminManage?Page1=feedback">Manage Feedback</a></li>

						<!--   <li class="drop-down"><a href="#">Deep Drop Down</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul> 
              </li>-->
					</ul></li>


				<li class="drop-down"><a href="">View</a>
					<ul>
						<li><a href="AdminManage?Page1=book">View Booking Consultations</a></li>
						<li><a href="AdminManage?Page1=la">View Legal Advice</a></li>
					</ul></li>

				<li><a href="AdminDashboard.jsp">Dashboard</a></li>
				<li><a href="Logout">Sign out</a></li>




				<!-- <li><a href="portfolio.html">Portfolio</a></li> -->
				<!-- <li><a href="pricing.html">Pricing</a></li> -->



			</ul>
		</nav>
		<!-- .nav-menu -->



	</div>
</header>
<!-- End Header -->

