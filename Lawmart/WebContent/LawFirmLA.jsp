<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Law Firm-Legal Advice</title>




<link rel="stylesheet" href="assets/css/table/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/all.css">
<link rel="stylesheet" href="assets/css/table/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="assets/css/table/style.css">
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

</head>
<body>
<%@ include file="LawfirmHeader.jsp" %>

<br><br><br>


<div>

<div class="content mt-3">
<div class="animated fadeIn">
<div class="row">
<div class="col-md-12">
<div class="card">

<section class="success" style="margin-bottom: 15px;  padding: 4px 12px; background-color: #ddffdd;  border-left: 6px solid #4CAF50;">
  <p style="font-size: 18px"><strong>Note!</strong> <strong>Once you are done with responding the client,client will 
  accept or reject your proposal.If Client accepts then they can pay your fees through our system or any other  preferred way.If
  rejects then entire history of the Legal Advice will be removed from the system! </strong></p>
</section> 

<div class="card-header">
<strong class="card-title">Legal Advice Table</strong>
</div>
<div class="card-body">
<table id="bootstrap-data-table" class="table table-striped table-bordered">
<thead>
<tr>
<th>Client Name</th>
<th>Client issue</th>
<th>Phone Number</th>
<th>Payment Status</th>
<th>Your Response </th>
<th>Fees</th>
<th>Order Id</th>
<th>Payment Date</th>
<th>Give Response/<br>Mention Fees</th>

</tr>
</thead>

<tbody>
<%List<LegalAdvice> laList=(List)request.getAttribute("ladata");
if(laList!=null){


for(LegalAdvice la:laList)
{
%>
<tr>
<%LawMartService ls=new LawMartServiceImpl();%>
<%User user=ls.getUser(la.getUser_id()); %>
<%if(user!=null){%>
	<td><%=user.getUserName()%></td>
	
<%}else{ %>
<td>User name not found!</td>
<%} %>

<td><%=la.getAdv_issue()%></td>

<%if(user!=null){%>
	<td><%=user.getUserPhone()%></td>
	
<%}else{ %>
<td>User phone number not found!</td>
<%} %>


<td><%=la.getPayment_status()%></td>

<%if(la.getFirm_responses()!=null){%>
	<td><%=la.getFirm_responses()%></td>
	
	
<%}else{ %>
<td>You have not responded yet!</td>
<%} %>


<%if(la.getAdv_fees()!=0){%>
	<td><%=la.getAdv_fees()%></td>
	
	
<%}else{ %>
<td>Not mentioned yet!</td>
<%} %>

<%if(la.getOrderId()!=null){%>
	<td><%=la.getOrderId()%></td>
	
	
<%}else{ %>
<td>Payment not done yet!</td>
<%} %>

<%if(la.getPayDate()!=null){%>
	<td><%=la.getPayDate()%></td>
	
	
<%}else{ %>
<td>Payment not done yet!</td>
<%} %>



<% if(la.getFirm_responses()!=null || la.getAdv_fees()!=0){%>
<td> 
<%--       <a href="GetResponsedata?LAadvId=<%=la.getAdv_id()%>" disabled> <i class="fas fa-reply"></i></a>  --%>
			You Have Already Responded.
</td>

<%} else {%>
<td> 
      <a href="GetResponsedata?LAadvId=<%=la.getAdv_id()%>"> <i class="fas fa-reply"></i></a>  
</td>
<%} %>


</tr>
<%} %>
<%} %>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<script src="assets/css/table/datatables.min.js"></script>
<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
<script src="assets/css/table/datatables-init.js"></script>

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>





</body>
</html>