<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.lawmart.bean.Area"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.Client"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <title>Client Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">



  <style>

.form-radio {
  margin-bottom: 40px; }
  .form-radio input {
    width: 0;
    height: 0;
    position: absolute;
    left: -9999px; }
  .form-radio input + label {
    margin: 0px;
    padding: 12px 10px;
    width: 94px;
    height: 50px;
    box-sizing: border-box;
    position: relative;
    display: inline-block;
    text-align: center;
    border: 1px solid #ebebeb;
    background-color: #FFF;
    font-size: 14px;
    font-weight: 600;
    color: #888;
    text-align: center;
    text-transform: none;
    transition: border-color .15s ease-out,  color .25s ease-out,  background-color .15s ease-out, box-shadow .15s ease-out;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px; }
  .form-radio input:checked + label {
    background-color: #57b846;
    color: #FFF;
    border-color: #57b846;
    z-index: 1; }
  .form-radio input:focus + label {
    outline: none; }
  .form-radio input:hover {
    background-color: #1da0f2;
    color: #FFF;
    border-color: #1da0f2; }
    .ui-datepicker-trigger {
  position: absolute;
  right: 25px;
  top: 41px;
  color: #999;
  font-size: 18px;
  background: transparent;
  border: none;
  outline: none;
  cursor: pointer; }

  input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

  </style>

  <link href="profilecss/bootstrap2.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="profilecss/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

 <script src="profilejs/bootstrap2.min.js"></script>
  <script src="profilejs/jquery2.min.js"></script>
   <script src="profilejs/bootstrap.min.js"></script>
 <script src="profilejs/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 </head>



<body style="background-color: #f7f7f7;" >
<%@ include file="ClientHeader.jsp" %>
<%Client client=(Client)request.getAttribute("clients"); %>  
<%if(client==null){ %>
              <%response.sendRedirect("Login.jsp");%>
              <%} %>
<%LawMartService ls = new LawMartServiceImpl(); %>
 <%
 int userId=client.getUserId();
 System.out.println("Client User id for proile is:"+userId);
%>     
<%String la = ls.countClientLATaken(userId);%>
<%System.out.println("LA profile is:"+la); %>
<%int cId = client.getClientId(); %>
 <%System.out.println("Client id for profile is:"+cId); %>
 <%String cases = ls.countClientCases(cId); %>
<br>
<br>
<br>
<br>
<br>
<br>
<!-- Feedack BC and LA Count -->
<%int clId = client.getClientId(); %>
<%String fbc = ls.countClientFeedbackBC(clId); %>
<%String fla = ls.countClientFeedbackLA(userId);%>
<%int totfb = Integer.parseInt(fbc) + Integer.parseInt(fla); %>
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1 style="font-size: 30px;"><%=client.getUserName().toUpperCase() %> </h1></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
          
          <ul class="list-group">
            <li class="list-group-item text-muted" style="font-size: 20px;">Activity <i class="fa fa-dashboard fa-1x"></i></li>
            <%if(cases!=null) {%>
             <li class="list-group-item text-right"><span class="pull-left"><strong>Total Cases</strong></span> <%=cases%></li>
            <%} else {%>
			 <li class="list-group-item text-right"><span class="pull-left"><strong>Total Cases</strong></span>0</li>            
            <%} %>
            
            <%if(la!=null) {%>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Legal Advices Taken</strong></span> <%=la %></li>
            <%} else { %>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Legal Advices Taken</strong></span>0</li>
            <%} %>
            
            <%if(totfb!=0) {%>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Feedbacks Given</strong></span><%=totfb%></li>
          	<%} else { %>
			<li class="list-group-item text-right"><span class="pull-left"><strong>Feedbacks Given</strong></span>0</li>          	
          	<%} %>
          </ul> 
          
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><h2 style="font-size: 30px;">Edit Profile</h2></li>
            </ul>

              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                
                  <form class="form" action="EditStakeholder?Page1=client" method="post" id="registrationForm">
                      <div class="form-group">
                        
                          <!--<label for="client_id">Client ID</label>-->
                                <input type="hidden" class="form-input" name="client_id" id="client_id" value="<%=client.getUserId()%>"/>
                        
                        
                          <div class="col-xs-6">
                              <label for="first_name"><h4 style="font-size: 20px;">First name</h4></label>
                              
                              <%String name=client.getUserName(); 
                              String fullname[]=name.split("\\s");
                              %>
                    
                              <input type="text" style="font-size: 20px;" class="form-control" name="first_name"  id="first_name" placeholder="first name" required="required" value="<%=fullname[0]%>">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4 style="font-size: 20px;">Last name</h4></label>
                              <input type="text" style="font-size: 20px;" class="form-control" name="last_name" id="last_name" placeholder="last name" required="required" value="<%=fullname[1]%>">
                          </div>
                      </div>
                      	
          				
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4 style="font-size: 20px;">Phone</h4></label>
                              <input type="number" style="font-size: 20px;" class="form-control" name="phone" id="phone" placeholder="enter phone" required="required" value="<%=client.getUserPhone()%>">
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h4 style="font-size: 20px;">Address</h4></label>
                            <textarea class="form-control" style="font-size: 20px;" name="address" id="address" required="required"><%=client.getUserAddress() %></textarea>
                            </div>
                      </div>
                      <div class="form-group">
                          
                        <div class="col-xs-6">
                          <label for="password2"><h4 style="font-size: 20px;">Password</h4></label>
                            <input type="password" style="font-size: 20px;" class="form-control" name="password1" id="password1" placeholder="enter password" required="required" value="<%=client.getUserPassword()%>">
                        </div>
                      </div>
                      
 
                      <div class="form-group">

                        <input type="hidden" class="form-control">
                    </div>   

                    
                      <div class="form-group">
                          
                        <div class="col-xs-6">
                          <label for="email"><h4 style="font-size: 20px;">Email</h4></label>
                          <input type="email" style="font-size: 20px;" class="form-control" name="email" id="email" placeholder="you@email.com" required="required" value="<%=client.getUserEmail()%>">
                          
                        </div>
                    </div>  
                    <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="areapin"><h4 style="font-size: 20px;">Area Pincode</h4></label>
                            	<%List<Area> areaList=(List)request.getAttribute("areas"); %> 
								<select style="font-size: 20px;" class="form-control" id="areapin" name="areapin">
 								
<!-- 								<select id="select-area"  name="area" required="required"> -->
<!-- 								<option value=""> </option> -->
								<%int i=1; %>
								<%for(Area a: areaList){ %>
									<%if(client.getAreaId()==i)
									{
									%>
										<option selected="selected" value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
									<%}%>
									<option value="<%=a.getAreaId()%>"><%=a.getAreaName()+"   "+a.getAreaPin()%></option>
 								<%i++; %> 
  								<%} %>
						  		</select>
                            </div>
                      </div> 
						
			<%
			String dateStore=client.getClientDob();
			String clientDOB1="";
			DateFormat format = new SimpleDateFormat("mm - dd - yyyy", Locale.ENGLISH);
		    try {
				 Date date1 = format.parse(dateStore);
				 System.out.println("Date1;"+date1);
				 clientDOB1 = new SimpleDateFormat("yyyy-mm-dd").format(date1);
				 System.out.println("From clientprofile Clientdob1:"+clientDOB1);
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			%>
						
						
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="birth_date"><h4 style="font-size: 20px;">Birth date</h4></label><br>
                          <input style="font-size: 20px;" type="date" class="form-input" name="birth_date" id="birth_date" value="<%=clientDOB1%>" placeholder="DD-MM-YYYY" required="required" max="2004-01-01"/>
                    </div>
                  </div>
 
                      <div class="form-radio" >
                       
						  <div class="col-xs-6" style="position: absolute;top: 101%">
									
                          <label for="gender" style=""><h4 style="font-size: 20px;">Gender</h4></label><br>
                          <%
                          	if(client.getClientGender().equals("male"))
                           {	
                           %>
                            <input type="radio" name="gender" value="male" id="male" checked="checked" />
                            <label for="male">Male</label>
                             <input type="radio" name="gender" value="female" id="female" />
                            <label for="female">Female</label>
						   <%
						    }
    					    else
    						{%>
    						<input type="radio" name="gender" value="male" id="male"/>
                            <label for="male">Male</label>
                            <input type="radio" name="gender" value="female" id="female" checked="checked"  />
                            <label for="female">Female</label>
                            <%} %>
                        </div>
                    </div>

                      <div class="form-group">
                           <div class="col-xs-12" style="position: absolute;top: 125%;">
                                <br>
                              	<button style="font-size: 20px;" class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button style="font-size: 20px;" class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
              
              
              
             </div>
             <!-- </div>/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
<script src="assets/vendor/Registration/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/vendor/Registration/jquery-validation/dist/additional-methods.min.js"></script>
</div>  
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br> 
 <%@ include file="HomePageFooter.jsp" %> 
</body>
</html>                                