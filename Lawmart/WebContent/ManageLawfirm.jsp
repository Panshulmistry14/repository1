<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.servlet.GetAreas"%>
<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.Services"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Lawfirm Table</title>
<meta name="description" content="Sufee Admin - HTML5 Admin Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="stylesheet" href="assets/css/table/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/font-awesome.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/all.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/dataTables.bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="assets/css/table/style.css"> -->

  <!-- Favicons -->
<!--   <link href="assets/img/favicon.png" rel="icon"> -->
<!--   <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon"> -->

  <!-- Google Fonts -->
<!--   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->

  <!-- Vendor CSS Files -->
<!--   <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/venobox/venobox.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/aos/aos.css" rel="stylesheet"> -->
<!--   <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet"> -->

  <!-- Template Main CSS File -->
<!--   <link href="assets/css/style.css" rel="stylesheet"> -->




</head>
<body>
<%@ include file="Servicesheader.jsp" %> 
<div id="right-panel" class="right-panel">

<div class="content mt-3">
<div class="animated fadeIn">
<div class="row">
<div class="col-md-12">
<div class="card">
<br>
<br>
<div class="card-header">
<strong class="card-title">Lawfirm Table</strong>
</div>
<div class="card-body">
<table id="bootstrap-data-table" class="table table-striped table-bordered">
<thead>
<tr>
<th>Firm Name</th>
<th>Phone</th>
<th>Address</th>
<th>Area ID</th>
<th>Email</th>
<!-- <th>Password</th> -->
<th>Reg. no.</th>
<th>Description</th>
<th>Edit</th>
<th>Delete</th>
</tr>
</thead>

<%List<LawFirm> lfList = (List)request.getAttribute("lawfirm"); %>
<tbody>

<tr>
<%for(LawFirm l : lfList)
{
%>
<td><%=l.getUserName() %></td>
<td><%=l.getUserPhone() %></td>
<td><%=l.getUserAddress() %></td>
<td><%=l.getAreaId() %></td>
<td><%=l.getUserEmail() %></td>
<%-- <td><%=l.getUserPassword() %></td> --%>
<td><%=l.getLfregNo() %></td>
<td><%=l.getLfDesc() %></td>

<td><a href="AdminEditData?Page1=lawfirm&lawfirmId=<%=l.getUserId()%>"><i class="fas fa-edit"></i></a></td>
<td><button onclick="document.getElementById('<%=l.getUserId() %>').style.display='block'" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></td>
</tr>
<div id="<%=l.getUserId()%>" class="modal" style="position: fixed; ;top: 47%;height: 150px;width: 400px;left: 37%;border: 2px solid black;background: white;" aria-hidden="true">
  <form class="modal-content"> <!-- action="/action_page.php" -->
    <div class="container" style="background-color:activeborder ;color: black; border-radius: 20px; ">
      <h1>Delete <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <a href="AdminManage?Page1=lawfirm" class="cancelbtn" style="color: black;"> <span aria-hidden="true">&times;</span> </a>
        </button></h1>
       
      <p>Are you sure you want to delete <%=l.getUserName()%>?</p>

      <div class="clearfix" style="position: relative;">
     
     <a style="position: absolute;left: 50%;color: black;border: 1px solid black;background-color : lightgray;padding-top : 3px;padding-bottom : 3px;padding-right : 5px;padding-left : 5px;" href="AdminManage?Page1=lawfirm" class="cancelbtn"> Cancel</a>
      <a style="position: absolute;left: 33%;color: black;border: 1px solid black;background-color : lightgray;padding-top : 3px;padding-bottom : 3px;padding-right : 5px;padding-left : 5px;" href="RemoveLawfirm?lawfirmId=<%=l.getUserId()%>" class="deletebtn"> Delete</a>  
     
      </div>
    </div>
  </form>
</div>
<%}%>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
 <script src="assets/css/table/datatables.min.js"></script>
<script src="assets/css/table/dataTables.bootstrap.min.js"></script>
<script src="assets/css/table/datatables-init.js"></script>

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>
</html>