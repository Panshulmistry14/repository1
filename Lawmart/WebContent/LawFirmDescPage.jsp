<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.LFServices"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.LawFirm"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<title>Client-Law Firm</title>

<style>

.b1{
 
  width: auto;
  background: #1bbd36;
  color: #fff;
  text-transform: uppercase;
  font-weight: 900;
  padding: 16px 50px;
  border: none;
  margin-top: 37px;
  cursor: pointer;
  text-decoration: none;
 }
 
.b1:hover {
	background: #333333;
	color: #fff;
	
}
</style>



<style>
.header {
	padding: 60px;
	text-align: center;
	background: #8CDD81;
	color: #4d4643;
	font-size: 30px;
	width: 90%;
	margin-left: 5%;
	border-radius: 30px;
	box-shadow: 10px 20px #888888;
	font-family: sans-serif;
}

.header1 {
	padding: 30px;
	text-align: center;
	color: bla;
	font-size: 30px;
}
</style>
</head>
<body style="background-color: #f7f7f7;">
<%@ include file="ClientHeader.jsp" %>
<br><br><br><br>
<%String lfId=request.getParameter("lfid");%>
<%int lId=Integer.parseInt(lfId);%>
<%LawMartService ls=new LawMartServiceImpl(); %>
<%LawFirm lf=ls.getSingleLF(lId); %>
<div class="header">
  <h1><%=lf.getUserName().toUpperCase()%></h1>
  <p><%=lf.getLfDesc()%></p>
</div>


<%List<LFServices> lfsList=ls.getLFServices(lId); %>


<main id="main">

   <br>
   <br>

<!-- ======= Services Section ======= -->
 
 <%if(lfsList!=null){ %>  
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">
      
      <div class="section-title">
          <h2>SERVICES</strong></h2>
        </div>


        <div class="row">
        
						<%for(LFServices lfs:lfsList){ %>
						
						
        
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box iconbox-blue">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,521.0016835830174C376.1290562159157,517.8887921683347,466.0731472004068,529.7835943286574,510.70327084640275,468.03025145048787C554.3714126377745,407.6079735673963,508.03601936045806,328.9844924480964,491.2728898941984,256.3432110539036C474.5976632858925,184.082847569629,479.9380746630129,96.60480741107993,416.23090153303,58.64404602377083C348.86323505073057,18.502131276798302,261.93793281208167,40.57373210992963,193.5410806939664,78.93577620505333C130.42746243093433,114.334589627462,98.30271207620316,179.96522072025542,76.75703585869454,249.04625023123273C51.97151888228291,328.5150500222984,13.704378332031375,421.85034740162234,66.52175969318436,486.19268352777647C119.04800174914682,550.1803526380478,217.28368757567262,524.383925680826,300,521.0016835830174"></path>
                </svg>
                <i class="bx bx-file"></i>
              </div>
              
             <%String sID=String.valueOf(lfs.getsId()); %>
              <%Services services=ls.fetchServices(sID);%>
              <h4><a href="ClientServicesPage.jsp?srno=<%=lfs.getSrNo()%>"><%=services.getServiceName()%></a></h4>
              
              <p> <%=lfs.getsDesc() %></p><br>
              <h5>Fees <%=lfs.getsFees()%></h5>
              
            </div>
          </div>
          <%} %>
          
 <%}else{ %>
<p class="header1">Services not found!</p>   
<%} %>     
        
        </div>
    </section>
    
    <!-- End of Services Section -->
    
    
          <br><br>
          <div class="section-title">
          <h2>LEGAL ADVICE</strong></h2>
          <p>Legal advice is the giving of a professional or formal opinion regarding the substance or procedure 
          of the law in relation to a particular factual situation. The provision of legal advice will often involve analyzing a set of facts and advising a person
           to take a specific course of action based on the applicable law.</p><br><br>
           
           <a class="b1" href="LegalAdvice.jsp?selfirm=<%=lf.getLfId()%>">Get Legal Advice</a>
           
        </div>
        <br><br>
    
    
    </main>








<%@ include file="HomePageFooter.jsp" %>

</body>
</html>