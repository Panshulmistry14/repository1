<%@page import="com.lawmart.bean.Services"%>
<%@page import="com.lawmart.bean.LFServices"%>
<%@page import="com.lawmart.bean.Client"%>
<%@page import="com.lawmart.bean.BookingConsultation"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.lawmart.bean.User"%>
<%@page import="com.lawmart.service.impl.LawMartServiceImpl"%>
<%@page import="com.lawmart.service.LawMartService"%>
<%@page import="com.lawmart.bean.LegalAdvice"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lawfirm Appointment Time Response</title>


<!-- Font Icon -->
<link rel="stylesheet"
	href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
<link rel="stylesheet"
	href="assets/vendor/Registration/jquery-ui/jquery-ui.min.css">

<!-- Main css -->
<link rel="stylesheet" href="assets/css/Registration/style2.css">

<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">



<style type="text/css">
body {
	background: transparent;
}

.Signup-form-title {
	background-color: transparent;
	color: black;
}

.cancel {
	width: auto;
	background: gray;
	color: #fff;
	text-transform: uppercase;
	font-weight: 900;
	padding: 16px 50px;
	float: right;
	border: none;
	margin-top: 37px;
	cursor: pointer;
	text-decoration: none;
}

.form-submit {
	width: auto;
	background: #57b846;
	color: #fff;
	text-transform: uppercase;
	font-weight: 900;
	padding: 16px 50px;
	float: right;
	border: none;
	margin-top: 37px;
	cursor: pointer;
}

.cancel:hover {
	color: white;
}

.form-submit:hover {
	background: #57b846;
}
</style>



<script src="assets/js/jquery-3.5.1.min.js"></script>




</head>
<body>
	<%@ include file="LawfirmHeader.jsp"%>


	<%-- <%LegalAdvice la=(LegalAdvice)request.getAttribute("laresponsedata"); %> --%>
	<%
		BookingConsultation bc = (BookingConsultation) request.getAttribute("bookconsult");
	%>
	<%
		LawMartService ls = new LawMartServiceImpl();
	%>

	<br>
	<br>
	<br>
	<br>
	<h4 class="Signup-form-title" style="position: absolute; left: 45%">
		GIVE RESPONSE</h4>
	<div class="main">

		<section class="signup">
			<div class="container11">
				<div class="signup-content">
					<form method="POST" action="PutAppointmentTime" id="signup-form"
						class="signup-form">
						<div class="form-group">
							<%
								if (bc != null) {
							%>
							<label for="name">Client's Name</label>


							<%
								Client client = ls.getSingleClient(bc.getClientId());
							%>
							<input type="hidden" class="form-input"
								value="<%=bc.getBookId()%>" name="bookid" id="bookid" />


							<%
								if (client.getUserName() != null) {
							%>

							<input type="text" readonly="readonly" class="form-input"
								value="<%=client.getUserName()%>" name="client_name"
								id="client_name" />

							<%
								} else {
							%>
							<input type="text" readonly="readonly" class="form-input"
								value="User name not found!" name="client_name" id="client_name" />
							<%
								}
							%>
							<br> <br>
							<%
								LFServices lfs = ls.getSingleLFService(bc.getSrNo());
							Services services = ls.fetchServices(String.valueOf(lfs.getsId()));
							%>

							<label for="service_name">Service Name</label>

							<%
								if (services.getServiceName() != null) {
							%>
							<input type="text" readonly="readonly" class="form-input"
								value="<%=services.getServiceName()%>" name="service_name"
								id="service_name" />


							<%
								} else {
							%>
							<input type="text" readonly="readonly" class="form-input"
								value="Service name not found!" name="service_name"
								id="service_name" />
							<%
								}
							%>

							<br> <br> <label for="fees">Fees</label>

							<%
								if (bc.getBookAmt() != 0) {
							%>
							<input type="number" readonly="readonly" class="form-input"
								value="<%=bc.getBookAmt()%>" name="fees" id="fees" />


							<%
								} else {
							%>
							<input type="number" readonly="readonly" class="form-input"
								value="Booking Fees not found!" name="fees" id="fees" />
							<%
								}
							%>

							<br> <br> <label for="book_date">Booking Date</label>

							<%
								if (bc.getBookDate() != null) {
							%>
							<input type="text" readonly="readonly" class="form-input"
								value="<%=bc.getBookDate()%>" name="book_date" id="book_date" />


							<%
								} else {
							%>
							<input type="text" readonly="readonly" class="form-input"
								value="Client Booking date not found!" name="book_date"
								id="book_date" />
							<%
								}
							%>
						</div>

						<br> <br>

						<div class="form-group">
							<label for="appoint_time">Appointment Time</label>

							<%
								if (bc.getBookAppointment() != null) {
							%>

							<textarea rows="3" cols="55" required="required"
								class="form-input" name="appoint_time" id="appoint_time"><%=bc.getBookAppointment()%></textarea>

							<%
								} else {
							%>
							<textarea rows="3" cols="55" required="required"
								class="form-input" name="appoint_time" id="appoint_time"></textarea>
							<%
								}
							%>
						</div>
						<%
							}
						%>
						<br> <br>

						<div class="form-group">
							<a href="GetLFBookingConsultationData" class="cancel"
								style="position: absolute; right: 30%">Cancel</a> <input
								type="submit" name="submit" id="submit" class="form-submit"
								value="Submit" />
						</div>
					</form>
				</div>
			</div>
		</section>

	</div>

	<!-- JS -->
	<script src="assets/vendor/Registration/jquery/jquery.min.js"></script>
	<script src="assets/vendor/Registration/jquery-ui/jquery-ui.min.js"></script>
	<script
		src="assets/vendor/Registration/jquery-validation/dist/jquery.validate.min.js"></script>
	<script
		src="assets/vendor/Registration/jquery-validation/dist/additional-methods.min.js"></script>
	<script src="assets/js/Registrationjs/main.js"></script>




</body>
</html>