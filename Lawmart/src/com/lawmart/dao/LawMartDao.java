package com.lawmart.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.lawmart.bean.Area;
import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.Feedback;
import com.lawmart.bean.LFServices;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.Payment;
import com.lawmart.bean.Services;
import com.lawmart.bean.User;

public interface LawMartDao {

	public List<Area> getArea(Connection connection) throws SQLException;

	public int putSingleUserData(Connection connection, User user) throws SQLException;

	public String putClientData(Connection connection, Client client) throws SQLException;

	public User validateUser(Connection connection, String email, String pass) throws SQLException;

	public List<Services> getServices(Connection connection) throws SQLException;

	public LawFirm getLawFirm(Connection connection, int uId) throws SQLException;

	public int putLF(Connection connection, LawFirm lawFirm) throws SQLException;

	public Services selectServices(Connection connection, String serviceId) throws SQLException;

	public String insertLFServices(Connection connection, LFServices lfServices) throws SQLException;

	public String updateServices(Connection connection, Services services) throws SQLException;

	public String deleteServices(Connection connection, int serviceId) throws SQLException;

	public List<Client> getClients(Connection connection) throws SQLException;

	public Client selectClientDetails(Connection connection, String clientId) throws SQLException;

	public String updateClientDetails(Connection connection, Client client) throws SQLException;

	public String deleteClientDetails(Connection connection, int clientId) throws SQLException;

	public List<LawFirm> getLawfirm(Connection connection) throws SQLException;

	public LawFirm selectLawfirmDetails(Connection connection, String lawfirmId) throws SQLException;

	public String updateLawfirmDetails(Connection connection, LawFirm lawFirm) throws SQLException;

	public String deleteLawfirmDetails(Connection connection, int lawfirmId) throws SQLException;

	public String updateAdminDetails(Connection connection, User user) throws SQLException;

	public String changePass(Connection connection, String email, String pass) throws SQLException;

	public String insertLegalAdvice(Connection connection, LegalAdvice la) throws SQLException;

	public List<LegalAdvice> getlaData(Connection connection, int lfId) throws Exception;

	public User getUser(Connection connection, int uId) throws SQLException;

	public LegalAdvice getLA(Connection connection, int aId) throws Exception;

	public String putResponseData(Connection connection, int advId, String rspnse, int fee) throws Exception;

	public List<LegalAdvice> getClientLAData(Connection connection, int userId) throws Exception;

	public LawFirm getSingleLF(Connection connection, int lf) throws Exception;

	public List<LFServices> getLFServices(Connection connection, int lfID) throws Exception;

	public String updateStatus(Connection connection, String odId, String status, String ordate) throws Exception;

	public String insertOrderId(Connection connection, String odId, int advId) throws Exception;

	public String deleteResponse(Connection connection, int advId) throws Exception;

	public String updateAppStatus(Connection connection, int advId) throws Exception;

	public String insertPayDetails(Connection connection, Payment payment) throws Exception;

	public LFServices getSingleLFService(Connection connection, int srno) throws Exception;

	public int insBookData(Connection connection, int cId, int srNo, int lfId, int bookAmt, int docID, String payStatus)
			throws Exception;

	public List<BookingConsultation> getBookingConsuiData(Connection connection, int clientId) throws Exception;

	public List<BookingConsultation> getLFBookingConsuiData(Connection connection, int lfId) throws Exception;

	public Client getSingleClient(Connection connection, int cId) throws Exception;
	
	public String insertAppTime(Connection connection,BookingConsultation bc) throws Exception;
	
	public String updateOrderBook(Connection connection,String odId,int bookId) throws Exception;
	
	public BookingConsultation getBookDetails(Connection connection,int bookID) throws Exception;
	
	public String insertServicePayDetails(Connection connection, Payment payment) throws Exception;
	
	public String updateBookPayStatus(Connection connection,String status,String odId) throws Exception;
	
	public Payment selectPayDetails(Connection connection,String odId) throws Exception;

	public List<BookingConsultation> getAllBookData(Connection connection) throws Exception;

	public List<LegalAdvice> getAllLaData(Connection connection) throws Exception;

	public int insertFBData(Connection connection, Feedback feedback) throws Exception;

	public int CheckFB(Connection connection, int sId) throws Exception;

	public List<Feedback> selFBData(Connection connection, int userId) throws Exception;

	public List<Feedback> selFBDataBC(Connection connection, int clientId) throws Exception;

	public List<Feedback> selFBDataLF(Connection connection, int lfId) throws Exception;

	public List<Feedback> selFBDataBCLF(Connection connection, int lfId) throws Exception;

	public List<Feedback> selectAllFBData(Connection connection) throws Exception;
	
	public String deleteFeedback(Connection connection,int fbId) throws Exception;
	
	public String countClient(Connection connection,String role) throws Exception;
	
	public String countLawfirm(Connection connection,String role) throws Exception;
	
	public String countLA(Connection connection) throws Exception;
	
	public String countBC(Connection connection) throws Exception;
	
	public String countOnlinePayment(Connection connection) throws Exception;
	
	public String countOfflinePayment(Connection connection) throws Exception;
	
	public String countSuccess(Connection connection) throws Exception;
	
	public String countFailure(Connection connection) throws Exception;
	
	public String countOnlineBC(Connection connection) throws Exception;
	
	public String countOfflineBC(Connection connection) throws Exception;
	
	public String countFailureBC(Connection connection) throws Exception;
	
	public String countVeryGoodFB(Connection connection) throws Exception;
	
	public String countFeedBack(Connection connection) throws Exception;
	
	public String countGoodFB(Connection connection) throws Exception;
	
	public String countMediocreFB(Connection connection) throws Exception;
	
	public String countBadFB(Connection connection) throws Exception;
	
	public String countVeryBadFB(Connection connection) throws Exception; 
	
	public List<String> getAllDates(Connection connection,String mon) throws Exception;
	
	public String countClientLATaken(Connection connection,int userId) throws Exception;
	
	public String countClientCases(Connection connection,int clientId) throws Exception;
	
	public String countLawfirmLA(Connection connection,int lfId) throws Exception;
	
	public String countLawfirmCases(Connection connection,int lfId) throws Exception;
	
	public String countClientFeedbackBC(Connection connection,int cId) throws Exception;
	
	public String countClientFeedbackLA(Connection connection,int uId) throws Exception;

	public String countLawfirmFeedbackBC(Connection connection,int lfId) throws Exception;
	
	public String countLawfirmFeedbackLA(Connection connection,int lfId) throws Exception;
}
