package com.lawmart.dao.impl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.lawmart.bean.Area;
import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.Encryption;
import com.lawmart.bean.Feedback;
import com.lawmart.bean.LFServices;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.Payment;
import com.lawmart.bean.Services;
import com.lawmart.bean.User;
import com.lawmart.dao.LawMartDao;

public class LawMartDaoImpl implements LawMartDao {

	@Override
	public List<Area> getArea(Connection connection) throws SQLException {

		List<Area> areaList = new ArrayList<Area>();
		String selQuery = "select * from area_table";
		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);
		ResultSet resultSet = preparedStatement.executeQuery();

		while (resultSet.next()) {
			Area area = new Area();
			area.setAreaId(resultSet.getInt("area_id"));
			area.setAreaName(resultSet.getString("area_name"));
			area.setAreaPin(resultSet.getInt("area_pincode"));

			areaList.add(area);

		}

		resultSet.close();
		connection.close();
		return areaList;
	}

	@Override
	public int putSingleUserData(Connection connection, User user) throws SQLException {

		int uId = 0;
		String insQuery = "insert into user_table(user_name,user_phone,user_address,area_id,user_email,user_password,user_role,user_isadmin,user_status) values(?,?,?,?,?,?,?,?,?)";

		PreparedStatement preparedStatement = connection.prepareStatement(insQuery, Statement.RETURN_GENERATED_KEYS);

		preparedStatement.setString(1, user.getUserName());
		preparedStatement.setLong(2, user.getUserPhone());
		preparedStatement.setString(3, user.getUserAddress());
		preparedStatement.setInt(4, user.getAreaId());
		preparedStatement.setNString(5, user.getUserEmail());
		preparedStatement.setString(6, user.getUserPassword());
		preparedStatement.setNString(7, user.getUserRole());
		preparedStatement.setBoolean(8, user.getUserIsAdmin());
		preparedStatement.setInt(9, user.getUserStatus());
		int insRows = preparedStatement.executeUpdate();
		ResultSet resultSet = preparedStatement.getGeneratedKeys();
		while (resultSet.next()) {
			uId = resultSet.getInt(1);
		}

		if (insRows > 0)
			System.out.println("insertion success");
		else
			System.out.println("insertion fail");

		preparedStatement.close();
		connection.close();
		return uId;
	}

	@Override
	public String putClientData(Connection connection, Client client) throws SQLException {
		// TODO Auto-generated method stub
		String message;
		String clientDob = client.getClientDob();
		SimpleDateFormat sdf = new SimpleDateFormat("mm - dd - yyyy");
		String insQuery = "insert into client_table(user_id,client_gender,client_dob) values(?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(insQuery);

		preparedStatement.setInt(1, client.getUserId());
		preparedStatement.setString(2, client.getClientGender());

		try {
			Date clientDate = sdf.parse(clientDob);
			java.sql.Date d = new java.sql.Date(clientDate.getTime());
			System.out.println("Client util date is:=" + clientDate);
			System.out.println("Client sql date is:=" + d);
			preparedStatement.setDate(3, d);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int insertedRows = preparedStatement.executeUpdate();

		if (insertedRows > 0) {
			message = "insertion success";
		} else {
			message = "insertion fail";
		}
		preparedStatement.close();
		connection.close();
		return message;
	}

	@Override
	public User validateUser(Connection connection, String email, String pass) throws SQLException {

		System.out.println("mail " + email + " " + " pass " + pass);
		User user = null;
		ResultSet resultSet = null;
		
		String encryptedPass=Encryption.encode(pass);
		
		String selQuery = "select * from user_table where user_email=? and user_password=?";
		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);
		preparedStatement.setString(1, email);
		preparedStatement.setString(2, encryptedPass);
		resultSet = preparedStatement.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				user = new User();
				user.setAreaId(resultSet.getInt("area_id"));
				user.setUserId(resultSet.getInt("user_id"));
				user.setUserName(resultSet.getString("user_name"));
				user.setUserPhone(resultSet.getLong("user_phone"));
				user.setUserAddress(resultSet.getString("user_address"));
				user.setUserEmail(resultSet.getString("user_email"));
				user.setUserPassword(resultSet.getString("user_password"));
				user.setUserRole(resultSet.getString("user_role"));
				user.setUserIsAdmin(resultSet.getBoolean("user_isadmin"));
			}
		}

//		System.out.println("u_id "+user.getUserId());
//		System.out.println("area id "+user.getAreaId());
//		System.out.println("u_ph "+user.getUserPhone());
//		System.out.println("u_address "+user.getUserAddress());
//		System.out.println("u_mail "+user.getUserEmail());
//		System.out.println("u_pass "+user.getUserPassword());
//		System.out.println("u_role"+user.getUserRole());
//		System.out.println("u_is admin "+user.getUserIsAdmin());
//		System.out.println("u name"+user.getUserName());

		resultSet.close();
		preparedStatement.close();
		connection.close();

		return user;

	}

	@Override
	public List<Services> getServices(Connection connection) throws SQLException {

		ResultSet resultSet = null;
		List<Services> servicesList = new ArrayList<Services>();

		String selQuery = "select * from services_table";
		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Services services = new Services();
			services.setServiceId(resultSet.getInt("service_id"));
			services.setServiceName(resultSet.getString("service_name"));
			services.setServiceDesc(resultSet.getString("service_description"));
			servicesList.add(services);

		}

		resultSet.close();
		preparedStatement.close();
		connection.close();
		return servicesList;
	}

	@Override
	public LawFirm getLawFirm(Connection connection, int uId) throws SQLException {

		LawFirm lawFirm = null;
		ResultSet resultSet = null;
		String selQuery = "select * from lawfirm_table where user_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);
		preparedStatement.setInt(1, uId);
		resultSet = preparedStatement.executeQuery();

		if (resultSet != null) {

			while (resultSet.next()) {
				lawFirm = new LawFirm();
				lawFirm.setLfId(resultSet.getInt("lf_id"));
				lawFirm.setuId(resultSet.getInt("user_id"));
				lawFirm.setLfregNo(resultSet.getString("lf_regno"));
				lawFirm.setLfDesc(resultSet.getString("lf_description"));

			}

		}

		resultSet.close();
		preparedStatement.close();
		connection.close();
		return lawFirm;
	}

	@Override
	public int putLF(Connection connection, LawFirm lawFirm) throws SQLException {

		int lfId = 0;
		String insQuery = "insert into lawfirm_table(user_id,lf_regno,lf_description) values(?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(insQuery, Statement.RETURN_GENERATED_KEYS);

		preparedStatement.setInt(1, lawFirm.getuId());
		preparedStatement.setString(2, lawFirm.getLfregNo());
		preparedStatement.setString(3, lawFirm.getLfDesc());

		preparedStatement.executeUpdate();
		ResultSet resultSet = preparedStatement.getGeneratedKeys();

		while (resultSet.next()) {
			lfId = resultSet.getInt(1);
		}
		return lfId;
	}

	@Override
	public String insertLFServices(Connection connection, LFServices lfServices) throws SQLException {

		String msg = "";
		String insQuery = "insert into lf_services_table(lf_id,service_id,service_fees,description) values(?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(insQuery);
		preparedStatement.setInt(1, lfServices.getlId());
		preparedStatement.setInt(2, lfServices.getsId());
		preparedStatement.setInt(3, lfServices.getsFees());
		preparedStatement.setString(4, lfServices.getsDesc());
		int insRows = preparedStatement.executeUpdate();
		if (insRows > 0) {
			msg = "Finally Law Firm all data inserted";

		} else {
			msg = "Final Law Firm insertion failed";
		}

		return msg;
	}

	@Override
	public Services selectServices(Connection connection, String serviceId) throws SQLException {
		// TODO Auto-generated method stub
		Services services = new Services();
		String selQuery = "select * from services_table where service_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);
		int sId = Integer.parseInt(serviceId);
		System.out.println("Service Id = " + sId);
		preparedStatement.setInt(1, sId);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			services.setServiceId(resultSet.getInt("service_id"));
			services.setServiceName(resultSet.getString("service_name"));
			services.setServiceDesc(resultSet.getString("service_description"));
		}
		preparedStatement.close();
		connection.close();
		return services;
	}

	@Override
	public String updateServices(Connection connection, Services services) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String updateQ = "update services_table set service_name=?,service_description=? where service_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(updateQ);
		preparedStatement.setString(1, services.getServiceName());
		preparedStatement.setString(2, services.getServiceDesc());
		preparedStatement.setInt(3, services.getServiceId());
		int updateServices = preparedStatement.executeUpdate();
		if (updateServices > 0) {
			msg = "Update success.";
		} else {
			msg = "Update failed";
		}
		return msg;

	}

	@Override
	public String deleteServices(Connection connection, int serviceId) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String delQuery = "delete from lf_services_table where service_id=?";
		String delQuery2 = "delete from services_table where service_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(delQuery);
		preparedStatement.setInt(1, serviceId);
		PreparedStatement preparedStatement2 = connection.prepareStatement(delQuery2);
		preparedStatement2.setInt(1, serviceId);
		int a = preparedStatement.executeUpdate();
		int b = preparedStatement2.executeUpdate();
		int delrows = a + b;
		System.out.println("Ans of prestats:" + delrows);
		if (delrows > 0) {
			msg = "Service Deleted Successfully.";
		} else {
			msg = "Service Deleting Failed.";
		}
		System.out.println("Message of delete is:" + msg);
		return msg;

	}

	@Override
	public List<Client> getClients(Connection connection) throws SQLException {
		// TODO Auto-generated method stub
		List<Client> clientList = new ArrayList<Client>();
		String selQuery = "select * from user_table join client_table on user_table.user_id=client_table.user_id and user_table.user_status=1";
		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Client client = new Client();
			client.setUserId(resultSet.getInt("user_id"));
			client.setUserName(resultSet.getString("user_name"));
			client.setUserPhone(resultSet.getLong("user_phone"));
			client.setUserAddress(resultSet.getString("user_address"));
			client.setAreaId(resultSet.getInt("area_id"));
			client.setUserEmail(resultSet.getString("user_email"));
			client.setUserPassword(resultSet.getString("user_password"));
			client.setClientGender(resultSet.getString("client_gender"));

			String dateStore = resultSet.getString("client_dob");
			String clientDOB1 = "";

			DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
			try {
				Date date1 = format.parse(dateStore);
				System.out.println("Date1;" + date1);
				clientDOB1 = new SimpleDateFormat("mm - dd - yyyy").format(date1);
				System.out.println("From dao Clientdob1:" + clientDOB1);
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			client.setClientDob(clientDOB1);
			clientList.add(client);
		}
//		for(Client c : clientList)
//		{
//			System.out.println("CLient name:"+c.getUserName());
//			System.out.println("Client password:"+c.getUserPassword());
//			System.out.println("Date:"+c.getClientDob());
//		}
//		System.out.println("hello from dao ");

		return clientList;
	}

	@Override

	public Client selectClientDetails(Connection connection, String clientId) throws SQLException {
		// TODO Auto-generated method stub
		Client client = new Client();
		String selclient = "select * from user_table join client_table on user_table.user_id=client_table.user_id where user_table.user_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(selclient);
		int cId = Integer.parseInt(clientId);
		preparedStatement.setInt(1, cId);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			// client.setClientDob(resultSet.getString("client_dob"));

			String dateStore = resultSet.getString("client_dob");
			String clientDOB1 = "";

			DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
			try {
				Date date1 = format.parse(dateStore);
				System.out.println("Date1;" + date1);
				clientDOB1 = new SimpleDateFormat("mm - dd - yyyy").format(date1);
				System.out.println("From dao Clientdob1:" + clientDOB1);
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			client.setClientId(resultSet.getInt("client_id"));
			client.setClientDob(clientDOB1);
			client.setUserId(resultSet.getInt("user_id"));
			client.setUserName(resultSet.getString("user_name"));
			client.setUserPhone(resultSet.getLong("user_phone"));
			client.setUserAddress(resultSet.getString("user_address"));
			client.setAreaId(resultSet.getInt("area_id"));
			client.setUserEmail(resultSet.getString("user_email"));
			client.setUserPassword(resultSet.getString("user_password"));
			client.setClientGender(resultSet.getString("client_gender"));

		}
		preparedStatement.close();
		connection.close();
		return client;
	}

	@Override
	public String updateClientDetails(Connection connection, Client client) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String clientDob = client.getClientDob();
		System.out.println("From dao clientdob:" + clientDob);
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		String upQuery = "update user_table join client_table on user_table.user_id=client_table.user_id set user_table.user_name=?,user_table.user_phone=?,user_table.user_address=?,user_table.area_id=?,user_table.user_email=?,user_table.user_password=?,client_table.client_gender=?,client_table.client_dob=? where user_table.user_id=?";
		System.out.println("Client dob:" + client.getClientDob());
		System.out.println("Client gender:" + client.getClientGender());
		PreparedStatement preparedStatement = connection.prepareStatement(upQuery);
		preparedStatement.setString(1, client.getUserName());
		preparedStatement.setLong(2, client.getUserPhone());
		preparedStatement.setString(3, client.getUserAddress());
		preparedStatement.setInt(4, client.getAreaId());
		preparedStatement.setString(5, client.getUserEmail());
		preparedStatement.setString(6, client.getUserPassword());
		preparedStatement.setString(7, client.getClientGender());
		// Date clientDate=null;
		try {
			Date clientDate = sdf.parse(clientDob);
			java.sql.Date d = new java.sql.Date(clientDate.getTime());
			System.out.println("Client util date is:=" + clientDate);
			System.out.println("Client sql date is:=" + d);
			preparedStatement.setDate(8, d);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		preparedStatement.setInt(9, client.getUserId());
		int updaterows = preparedStatement.executeUpdate();
		if (updaterows > 0) {
			msg = "Updated client";
		} else {
			msg = "Update failed";
		}

		return msg;
	}

	@Override
	public String deleteClientDetails(Connection connection, int clientId) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String delQuery = "update user_table set user_status=0 where user_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(delQuery);
		System.out.println("delcl:" + clientId);
		preparedStatement.setInt(1, clientId);
		int delrows = preparedStatement.executeUpdate();
		if (delrows > 0) {
			msg = "Delete success";
		} else {
			msg = "Delete failed";
		}
		return msg;
	}

	@Override
	public List<LawFirm> getLawfirm(Connection connection) throws SQLException {
		// TODO Auto-generated method stub
		List<LawFirm> lfList = new ArrayList<LawFirm>();
		String lfQuery = "select * from user_table join lawfirm_table on user_table.user_id=lawfirm_table.user_id and user_table.user_status=1";
		PreparedStatement preparedStatement = connection.prepareStatement(lfQuery);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			LawFirm lawFirm = new LawFirm();
			lawFirm.setUserId(resultSet.getInt("user_id"));
			lawFirm.setUserName(resultSet.getString("user_name"));
			lawFirm.setUserPhone(resultSet.getLong("user_phone"));
			lawFirm.setUserAddress(resultSet.getString("user_address"));
			lawFirm.setAreaId(resultSet.getInt("area_id"));
			lawFirm.setUserEmail(resultSet.getString("user_email"));
			lawFirm.setUserPassword(resultSet.getString("user_password"));
			lawFirm.setLfId(resultSet.getInt("lf_id"));
			lawFirm.setLfregNo(resultSet.getString("lf_regno"));
			lawFirm.setLfDesc(resultSet.getString("lf_description"));
			lfList.add(lawFirm);
		}
		return lfList;
	}

	@Override
	public LawFirm selectLawfirmDetails(Connection connection, String lawfirmId) throws SQLException {
		// TODO Auto-generated method stub
		LawFirm lawFirm = new LawFirm();
		String lfQuery = "select * from user_table join lawfirm_table on user_table.user_id=lawfirm_table.user_id where user_table.user_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(lfQuery);
		int lfId = Integer.parseInt(lawfirmId);
		preparedStatement.setInt(1, lfId);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			lawFirm.setUserId(resultSet.getInt("user_id"));
			lawFirm.setUserName(resultSet.getString("user_name"));
			lawFirm.setUserPhone(resultSet.getLong("user_phone"));
			lawFirm.setUserAddress(resultSet.getString("user_address"));
			lawFirm.setAreaId(resultSet.getInt("area_id"));
			lawFirm.setUserEmail(resultSet.getString("user_email"));
			lawFirm.setUserPassword(resultSet.getString("user_password"));
			lawFirm.setLfId(resultSet.getInt("lf_id"));
			lawFirm.setLfDesc(resultSet.getString("lf_description"));
			lawFirm.setLfregNo(resultSet.getString("lf_regno"));
		}
		preparedStatement.close();
		connection.close();
		return lawFirm;
	}

	@Override
	public String updateLawfirmDetails(Connection connection, LawFirm lawFirm) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String upQuery = "update user_table join lawfirm_table on user_table.user_id=lawfirm_table.user_id set user_table.user_name=?,user_table.user_phone=?,user_table.user_address=?,user_table.area_id=?,user_table.user_email=?,user_table.user_password=?,lawfirm_table.lf_regno=?,lawfirm_table.lf_description=? where user_table.user_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(upQuery);
		preparedStatement.setString(1, lawFirm.getUserName());
		preparedStatement.setLong(2, lawFirm.getUserPhone());
		preparedStatement.setString(3, lawFirm.getUserAddress());
		preparedStatement.setInt(4, lawFirm.getAreaId());
		preparedStatement.setString(5, lawFirm.getUserEmail());
		preparedStatement.setString(6, lawFirm.getUserPassword());
		preparedStatement.setString(7, lawFirm.getLfregNo());
		preparedStatement.setString(8, lawFirm.getLfDesc());
		preparedStatement.setInt(9, lawFirm.getUserId());
		int updaterows = preparedStatement.executeUpdate();
		if (updaterows > 0) {
			msg = "Updated client";
		} else {
			msg = "Update failed";
		}
		return msg;
	}

	@Override
	public String deleteLawfirmDetails(Connection connection, int lawfirmId) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String delQuery = "update user_table set user_status=0 where user_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(delQuery);
		System.out.println("dellf:" + lawfirmId);
		preparedStatement.setInt(1, lawfirmId);
		int delrows = preparedStatement.executeUpdate();
		if (delrows > 0) {
			msg = "Delete success";
		} else {
			msg = "Delete failed";
		}
		return msg;

	}

	@Override
	public String updateAdminDetails(Connection connection, User user) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String upQuery = "update user_table set user_name=?,user_phone=?,user_address=?,area_id=?,user_email=?,user_password=? where user_role='Admin'";
		PreparedStatement preparedStatement = connection.prepareStatement(upQuery);
		preparedStatement.setString(1, user.getUserName());
		preparedStatement.setLong(2, user.getUserPhone());
		preparedStatement.setString(3, user.getUserAddress());
		preparedStatement.setInt(4, user.getAreaId());
		preparedStatement.setString(5, user.getUserEmail());
		preparedStatement.setString(6, user.getUserPassword());
		// preparedStatement.setInt(7, user.getUserId());
		int updatedrows = preparedStatement.executeUpdate();
		if (updatedrows > 0) {
			msg = "Update success";
		} else {
			msg = "update failed";
		}
		return msg;
	}

	@Override
	public String changePass(Connection connection, String email, String pass) throws SQLException {

		String msg = "";

		String updQuery = "update user_table set user_password=? where user_email=?";

		PreparedStatement preparedStatement = connection.prepareStatement(updQuery);

		String encryptedpass=Encryption.encode(pass);
		
		preparedStatement.setString(1,encryptedpass);
		preparedStatement.setString(2, email);
		int updRows = preparedStatement.executeUpdate();

		if (updRows > 0) {
			msg = "password changed successfully";
		} else {
			msg = "password changing failed";
		}

		return msg;
	}

	@Override
	public String insertLegalAdvice(Connection connection, LegalAdvice la) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		String insQuery = "insert into legal_advice_table(user_id,lf_id,adv_issue,payment_status) values(?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(insQuery);
		preparedStatement.setInt(1, la.getUser_id());
		preparedStatement.setInt(2, la.getLf_id());
		preparedStatement.setString(3, la.getAdv_issue());
		preparedStatement.setString(4, la.getPayment_status());
		int insertRows = preparedStatement.executeUpdate();
		if (insertRows > 0) {
			msg = "legal advice success";
		} else {
			msg = "legal advice failed";
		}
		return msg;
	}

	@Override
	public List<LegalAdvice> getlaData(Connection connection, int lfId) throws Exception {

		List<LegalAdvice> laList = new ArrayList<LegalAdvice>();
		ResultSet resultSet = null;

		System.out.println("lf id from dao impl " + lfId);

		String selQuery = "select * from legal_advice_table where lf_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		preparedStatement.setInt(1, lfId);

		resultSet = preparedStatement.executeQuery();
		if (resultSet != null) {
			while (resultSet.next()) {
				LegalAdvice la = new LegalAdvice();
				la.setAdv_id(resultSet.getInt("adv_id"));
				la.setUser_id(resultSet.getInt("user_id"));
				la.setLf_id(resultSet.getInt("lf_id"));
				la.setAdv_issue(resultSet.getString("adv_issue"));
				la.setPayment_status(resultSet.getString("payment_status"));
				la.setFirm_responses(resultSet.getString("firm_responses"));
				la.setAdv_fees(resultSet.getInt("adv_fees"));
				la.setOrderId(resultSet.getString("order_id"));
				la.setPayDate(resultSet.getString("pay_date"));

				laList.add(la);
				System.out.println("adv id from dao impl " + la.getAdv_id());

			}

		}

		return laList;
	}

	@Override
	public User getUser(Connection connection, int uId) throws SQLException {

		User user = null;

		ResultSet resultSet = null;

		String selQuery = "select * from user_table where user_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		preparedStatement.setInt(1, uId);

		resultSet = preparedStatement.executeQuery();
		if (resultSet != null) {
			while (resultSet.next()) {
				user = new User();
				user.setAreaId(resultSet.getInt("area_id"));
				user.setUserId(resultSet.getInt("user_id"));
				user.setUserName(resultSet.getString("user_name"));
				user.setUserPhone(resultSet.getLong("user_phone"));
				user.setUserAddress(resultSet.getString("user_address"));
				user.setUserEmail(resultSet.getString("user_email"));
				user.setUserPassword(resultSet.getString("user_password"));
				user.setUserRole(resultSet.getString("user_role"));
				user.setUserIsAdmin(resultSet.getBoolean("user_isadmin"));
			}
		}

		return user;
	}

	@Override
	public LegalAdvice getLA(Connection connection, int aId) throws Exception {

		LegalAdvice la = null;

		ResultSet resultSet = null;

		String selQuery = "select * from legal_advice_table where adv_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		preparedStatement.setInt(1, aId);

		resultSet = preparedStatement.executeQuery();
		if (resultSet != null) {
			while (resultSet.next()) {
				la = new LegalAdvice();
				la.setAdv_id(resultSet.getInt("adv_id"));
				la.setUser_id(resultSet.getInt("user_id"));
				la.setLf_id(resultSet.getInt("lf_id"));
				la.setAdv_issue(resultSet.getString("adv_issue"));
				la.setPayment_status(resultSet.getString("payment_status"));
				la.setFirm_responses(resultSet.getString("firm_responses"));
				la.setAdv_fees(resultSet.getInt("adv_fees"));

				System.out.println("adv id from dao impl " + la.getAdv_id());

			}
		}

		return la;
	}

	@Override
	public String putResponseData(Connection connection, int advId, String rspnse, int fee) throws Exception {

		String msg = null;

		String updQuery = "update legal_advice_table set firm_responses=?,adv_fees=? where adv_id=?;";

		int updRows = 0;
		PreparedStatement ps = connection.prepareStatement(updQuery);

		ps.setString(1, rspnse);
		ps.setInt(2, fee);
		ps.setInt(3, advId);

		updRows = ps.executeUpdate();

		if (updRows > 0) {
			msg = "response inserted successfully";
		} else {
			msg = "response insertion failed";
		}

		return msg;
	}

	@Override
	public List<LegalAdvice> getClientLAData(Connection connection, int userId) throws Exception {

		List<LegalAdvice> laList = new ArrayList<LegalAdvice>();
		ResultSet resultSet = null;

		System.out.println("lf id from dao impl " + userId);

		String selQuery = "select * from legal_advice_table where user_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		preparedStatement.setInt(1, userId);

		resultSet = preparedStatement.executeQuery();
		if (resultSet != null) {
			while (resultSet.next()) {
				LegalAdvice la = new LegalAdvice();
				la.setAdv_id(resultSet.getInt("adv_id"));
				la.setUser_id(resultSet.getInt("user_id"));
				la.setLf_id(resultSet.getInt("lf_id"));
				la.setAdv_issue(resultSet.getString("adv_issue"));
				la.setPayment_status(resultSet.getString("payment_status"));
				la.setFirm_responses(resultSet.getString("firm_responses"));
				la.setOrderId(resultSet.getString("order_id"));
				la.setPayDate(resultSet.getString("pay_date"));
				la.setAdv_fees(resultSet.getInt("adv_fees"));

				laList.add(la);
				System.out.println("adv id from dao impl " + la.getAdv_id());

			}

		}

		return laList;
	}

	@Override
	public LawFirm getSingleLF(Connection connection, int lf) throws Exception {

		LawFirm lF = null;
		ResultSet resultSet = null;

		String selQuery = "select * from lawfirm_table join user_table where lawfirm_table.lf_id=? and lawfirm_table.user_id=user_table.user_id";

		PreparedStatement ps = connection.prepareStatement(selQuery);

		ps.setInt(1, lf);
		resultSet = ps.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				lF = new LawFirm();

				lF.setUserId(resultSet.getInt("user_id"));
				lF.setUserName(resultSet.getString("user_name"));
				lF.setUserPhone(resultSet.getLong("user_phone"));
				lF.setUserAddress(resultSet.getString("user_address"));
				lF.setAreaId(resultSet.getInt("area_id"));
				lF.setUserEmail(resultSet.getString("user_email"));
				lF.setLfId(resultSet.getInt("lf_id"));
				lF.setLfregNo(resultSet.getString("lf_regno"));
				lF.setLfDesc(resultSet.getString("lf_description"));

			}
		}

		return lF;

	}

	@Override
	public List<LFServices> getLFServices(Connection connection, int lfID) throws Exception {

		List<LFServices> lfServices = new ArrayList<LFServices>();
		ResultSet resultSet = null;
		String selQuery = "select * from lf_services_table where lf_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);
		ps.setInt(1, lfID);
		resultSet = ps.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				LFServices lfs = new LFServices();

				lfs.setSrNo(resultSet.getInt("sr_no"));
				lfs.setlId(resultSet.getInt("lf_id"));
				lfs.setsId(resultSet.getInt("service_id"));
				lfs.setsFees(resultSet.getInt("service_fees"));
				lfs.setsDesc(resultSet.getString("description"));
				lfServices.add(lfs);

			}
		} else {
			lfServices = null;
		}

		return lfServices;
	}

	@Override
	public String insertOrderId(Connection connection, String odId, int advId) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String insQ = "update legal_advice_table set order_id=? where adv_id=?";
		PreparedStatement ps = connection.prepareStatement(insQ);
		ps.setString(1, odId);
		ps.setInt(2, advId);
		int updRows = 0;
		updRows = ps.executeUpdate();

		if (updRows > 0) {
			msg = "OrderId updated successfully";
		} else {
			msg = "OrderId updation failed";
		}

		return msg;
	}

	@Override
	public String updateStatus(Connection connection, String odId, String status, String ordate) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String upq = "update legal_advice_table set payment_status=?,pay_date=? where order_id=?";
		PreparedStatement ps = connection.prepareStatement(upq);
		ps.setString(1, status);
		ps.setString(2, ordate);
		ps.setString(3, odId);
		int updRows = 0;
		updRows = ps.executeUpdate();

		if (updRows > 0) {
			msg = "Status updated successfully";
		} else {
			msg = "Status updation failed";
		}

		return msg;
	}

	@Override
	public String deleteResponse(Connection connection, int advId) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String delQuery = "delete from legal_advice_table where adv_id=?";
		PreparedStatement ps = connection.prepareStatement(delQuery);
		ps.setInt(1, advId);
		int delrows = ps.executeUpdate();
		;
		System.out.println("Ans of prestats:" + delrows);
		if (delrows > 0) {
			msg = "Response Deleted Successfully.";
		} else {
			msg = "Response Deleting Failed.";
		}
		return msg;
	}

	@Override
	public String updateAppStatus(Connection connection, int advId) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String updQuery = "update legal_advice_table set payment_status='At Appointment Time' where adv_id=?";
		PreparedStatement ps = connection.prepareStatement(updQuery);
		ps.setInt(1, advId);
		int updRows = 0;
		updRows = ps.executeUpdate();

		if (updRows > 0) {
			msg = "Appointment Status updated successfully";
		} else {
			msg = "Appointment Status updation failed";
		}

		return msg;

	}

	@Override
	public String insertPayDetails(Connection connection, Payment payment) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String insQ = "insert into payment_table(order_id,bank_name,banktxn_id,currency,gateway_name,merchant_id,pay_mode,resp_code,resp_msg,pay_status,txn_amt,txn_date,txn_id,category) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(insQ);
		preparedStatement.setString(1, payment.getOrderId());
		preparedStatement.setString(2, payment.getBankName());
		preparedStatement.setString(3, payment.getBankId());
		preparedStatement.setString(4, payment.getCurr());
		preparedStatement.setString(5, payment.getGateName());
		preparedStatement.setString(6, payment.getMerId());
		preparedStatement.setString(7, payment.getPayMode());
		preparedStatement.setInt(8, payment.getRespCode());
		preparedStatement.setString(9, payment.getRespMsg());
		preparedStatement.setString(10, payment.getPayStatus());
		preparedStatement.setInt(11, payment.getTxnAmt());
		preparedStatement.setString(12, payment.getTxnDate());
		preparedStatement.setString(13, payment.getTxnId());
		preparedStatement.setString(14, payment.getCategory());

		System.out.println(payment.getOrderId());
		System.out.println(payment.getBankName());
		System.out.println(payment.getBankId());
		System.out.println(payment.getCurr());
		System.out.println(payment.getGateName());
		System.out.println(payment.getMerId());
		System.out.println(payment.getPayMode());
		System.out.println(payment.getRespCode());
		System.out.println(payment.getRespMsg());
		System.out.println(payment.getPayStatus());
		System.out.println(payment.getTxnAmt());
		System.out.println(payment.getTxnDate());
		System.out.println(payment.getTxnId());

		int insertRows = preparedStatement.executeUpdate();

		if (insertRows > 0) {
			msg = "payment details success";
		} else {
			msg = "payment details failed";
		}
		return msg;
	}

	@Override
	public LFServices getSingleLFService(Connection connection, int srno) throws Exception {

		LFServices lfs = null;
		ResultSet resultSet = null;

		String selQuery = "select * from lf_services_table where sr_no=?";
		PreparedStatement ps = connection.prepareStatement(selQuery);

		ps.setInt(1, srno);
		resultSet = ps.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				lfs = new LFServices();
				lfs.setSrNo(resultSet.getInt("sr_no"));
				lfs.setlId(resultSet.getInt("lf_id"));
				lfs.setsId(resultSet.getInt("service_id"));
				lfs.setsFees(resultSet.getInt("service_fees"));
				lfs.setsDesc(resultSet.getString("description"));
			}

		}

		return lfs;
	}

	@Override
	public int insBookData(Connection connection, int cId, int srNo, int lfId, int bookAmt, int docID, String paystatus)
			throws Exception {

		int bookID = 0;

		String insQuery = "insert into book_table(client_id,sr_no,lf_id,book_date,book_amt,book_status,pay_status,doc_id) values(?,?,?,?,?,?,?,?)";

		PreparedStatement ps = connection.prepareStatement(insQuery, Statement.RETURN_GENERATED_KEYS);

		ps.setInt(1, cId);
		ps.setInt(2, srNo);
		ps.setInt(3, lfId);
		ps.setString(4, java.time.LocalDate.now().toString());
		ps.setInt(5, bookAmt);
		ps.setString(6, "success");
		ps.setNString(7, paystatus);
		ps.setInt(8, docID);

		int insRows = 0;
		insRows = ps.executeUpdate();

		ResultSet resultSet = null;
		resultSet = ps.getGeneratedKeys();

		if (resultSet != null) {
			while (resultSet.next()) {
				bookID = resultSet.getInt(1);
			}
		}

		if (insRows > 0) {
			System.out.println("book tbl initial insertion success");
		} else {
			System.out.println("book tbl initial insertion falied");

		}
		return bookID;
	}

	@Override
	public List<BookingConsultation> getBookingConsuiData(Connection connection, int clientId) throws Exception {

		List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
		ResultSet resultSet = null;

		String selQuery = "select * from book_table where client_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);
		ps.setInt(1, clientId);
		resultSet = ps.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				BookingConsultation bc = new BookingConsultation();

				bc.setBookId(resultSet.getInt("book_id"));
				bc.setClientId(resultSet.getInt("client_id"));
				bc.setSrNo(resultSet.getInt("sr_no"));
				bc.setLfId(resultSet.getInt("lf_id"));
				bc.setBookDate(resultSet.getString("book_date"));
				bc.setBookAmt(resultSet.getInt("book_amt"));
				bc.setBookStatus(resultSet.getString("book_status"));
				bc.setPayStatus(resultSet.getString("pay_status"));
				bc.setDocId(resultSet.getInt("doc_id"));
				bc.setOrderId(resultSet.getString("order_id"));
				bc.setBookAppointment(resultSet.getString("book_appointment"));

				bcList.add(bc);

			}
		}

		return bcList;
	}

	@Override
	public List<BookingConsultation> getLFBookingConsuiData(Connection connection, int lfId) throws Exception {
		List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
		ResultSet resultSet = null;

		String selQuery = "select * from book_table where lf_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);
		ps.setInt(1, lfId);
		resultSet = ps.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				BookingConsultation bc = new BookingConsultation();

				bc.setBookId(resultSet.getInt("book_id"));
				bc.setClientId(resultSet.getInt("client_id"));
				bc.setSrNo(resultSet.getInt("sr_no"));
				bc.setLfId(resultSet.getInt("lf_id"));
				bc.setBookDate(resultSet.getString("book_date"));
				bc.setBookAmt(resultSet.getInt("book_amt"));
				bc.setBookStatus(resultSet.getString("book_status"));
				bc.setPayStatus(resultSet.getString("pay_status"));
				bc.setDocId(resultSet.getInt("doc_id"));
				bc.setOrderId(resultSet.getString("order_id"));
				bc.setBookAppointment(resultSet.getString("book_appointment"));

				bcList.add(bc);

			}
		}

		return bcList;
	}

	@Override
	public Client getSingleClient(Connection connection, int cId) throws Exception {
		Client client = null;
		ResultSet resultSet = null;

		String selQuery = "select * from client_table join user_table where client_table.client_id=? and client_table.user_id=user_table.user_id";

		PreparedStatement ps = connection.prepareStatement(selQuery);

		ps.setInt(1, cId);
		resultSet = ps.executeQuery();

		if (resultSet != null) {
			while (resultSet.next()) {
				client = new Client();

				client.setUserId(resultSet.getInt("user_id"));
				client.setUserName(resultSet.getString("user_name"));
				client.setUserPhone(resultSet.getLong("user_phone"));
				client.setUserAddress(resultSet.getString("user_address"));
				client.setAreaId(resultSet.getInt("area_id"));
				client.setUserEmail(resultSet.getString("user_email"));
				client.setClientId(resultSet.getInt("client_id"));
				client.setClientGender(resultSet.getString("client_gender"));

			}
		}

		return client;
	}

	@Override
	public String insertAppTime(Connection connection, BookingConsultation bc) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String updateQ = "update book_table set book_appointment=? where book_id=?";
		PreparedStatement preparedStatement = connection.prepareStatement(updateQ);
		preparedStatement.setString(1, bc.getBookAppointment());
		preparedStatement.setInt(2, bc.getBookId());
		int updateServices = preparedStatement.executeUpdate();
		if (updateServices > 0) {
			msg = "Update success.";
		} else {
			msg = "Update failed";
		}
		return msg;

	}

	@Override
	public BookingConsultation getBookDetails(Connection connection, int bookID) throws Exception {
		// TODO Auto-generated method stub
		BookingConsultation bc = null;

		ResultSet resultSet = null;

		String selQuery = "select * from book_table where book_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		preparedStatement.setInt(1, bookID);

		resultSet = preparedStatement.executeQuery();
		if (resultSet != null) {
			while (resultSet.next()) {
				bc = new BookingConsultation();
				// la.setAdv_id(resultSet.getInt("adv_id"));
				bc.setBookId(resultSet.getInt("book_id"));
				bc.setBookDate(resultSet.getString("book_date"));
				bc.setBookAmt(resultSet.getInt("book_amt"));
				bc.setClientId(resultSet.getInt("client_id"));
				bc.setLfId(resultSet.getInt("lf_id"));
				bc.setSrNo(resultSet.getInt("sr_no"));
				bc.setOrderId(resultSet.getString("order_id"));
				bc.setBookAppointment(resultSet.getString("book_appointment"));
//				System.out.println("adv id from dao impl " + la.getAdv_id());

			}
		}

		return bc;
	}

	@Override
	public String updateOrderBook(Connection connection, String odId, int bookId) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String upQuery = "update book_table set order_id=? where book_id=?";
		PreparedStatement ps = connection.prepareStatement(upQuery);
		ps.setString(1, odId);
		ps.setInt(2, bookId);
		int updRows = 0;
		updRows = ps.executeUpdate();

		if (updRows > 0) {
			msg = "OrderId in book updated successfully";
		} else {
			msg = "OrderId in book updation failed";
		}

		return msg;
	}

	@Override
	public String insertServicePayDetails(Connection connection, Payment payment) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String insQ = "insert into payment_table(order_id,bank_name,banktxn_id,currency,gateway_name,merchant_id,pay_mode,resp_code,resp_msg,pay_status,txn_amt,txn_date,txn_id,category) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(insQ);
		preparedStatement.setString(1, payment.getOrderId());
		preparedStatement.setString(2, payment.getBankName());
		preparedStatement.setString(3, payment.getBankId());
		preparedStatement.setString(4, payment.getCurr());
		preparedStatement.setString(5, payment.getGateName());
		preparedStatement.setString(6, payment.getMerId());
		preparedStatement.setString(7, payment.getPayMode());
		preparedStatement.setInt(8, payment.getRespCode());
		preparedStatement.setString(9, payment.getRespMsg());
		preparedStatement.setString(10, payment.getPayStatus());
		preparedStatement.setInt(11, payment.getTxnAmt());
		preparedStatement.setString(12, payment.getTxnDate());
		preparedStatement.setString(13, payment.getTxnId());
		preparedStatement.setString(14, payment.getCategory());

		System.out.println(payment.getOrderId());
		System.out.println(payment.getBankName());
		System.out.println(payment.getBankId());
		System.out.println(payment.getCurr());
		System.out.println(payment.getGateName());
		System.out.println(payment.getMerId());
		System.out.println(payment.getPayMode());
		System.out.println(payment.getRespCode());
		System.out.println(payment.getRespMsg());
		System.out.println(payment.getPayStatus());
		System.out.println(payment.getTxnAmt());
		System.out.println(payment.getTxnDate());
		System.out.println(payment.getTxnId());

		int insertRows = preparedStatement.executeUpdate();

		if (insertRows > 0) {
			msg = "payment details success";
		} else {
			msg = "payment details failed";
		}
		return msg;
	}

	@Override
	public String updateBookPayStatus(Connection connection, String status, String odId) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String upq = "update book_table set pay_status=? where order_id=?";
		PreparedStatement ps = connection.prepareStatement(upq);
		ps.setString(1, status);
		ps.setString(2, odId);
		int updRows = 0;
		updRows = ps.executeUpdate();

		if (updRows > 0) {
			msg = "Book Status updated successfully";
		} else {
			msg = "Book Status updation failed";
		}

		return msg;
	}

	@Override
	public Payment selectPayDetails(Connection connection, String odId) throws Exception {
		// TODO Auto-generated method stub
		Payment payment = null;

		ResultSet resultSet = null;

		String selQuery = "select * from payment_table where order_id=?";

		PreparedStatement preparedStatement = connection.prepareStatement(selQuery);

		preparedStatement.setString(1, odId);

		resultSet = preparedStatement.executeQuery();
		if (resultSet != null) {
			while (resultSet.next()) {
				payment = new Payment();
				payment.setBankName(resultSet.getString("bank_name"));
				payment.setBankId(resultSet.getString("banktxn_id"));
				payment.setCurr(resultSet.getString("currency"));
				payment.setGateName(resultSet.getString("gateway_name"));
				payment.setMerId(resultSet.getString("merchant_id"));
				payment.setPayMode(resultSet.getString("pay_mode"));
				payment.setRespMsg(resultSet.getString("resp_msg"));
				payment.setPayStatus(resultSet.getString("pay_status"));
				payment.setTxnAmt(resultSet.getInt("txn_amt"));
				payment.setTxnDate(resultSet.getString("txn_date"));
				payment.setTxnId(resultSet.getString("txn_id"));
				payment.setCategory(resultSet.getString("category"));

			}

			System.out.println("bank name " + payment.getBankName());
		}

		return payment;
	}

	@Override
	public List<BookingConsultation> getAllBookData(Connection connection) throws Exception {

		List<BookingConsultation> bookList = new ArrayList<BookingConsultation>();

		String selQuery = "select * from book_table";

		ResultSet rs = null;
		PreparedStatement ps = connection.prepareStatement(selQuery);
		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				BookingConsultation bc = new BookingConsultation();

				bc.setBookId(rs.getInt("book_id"));
				bc.setClientId(rs.getInt("client_id"));
				bc.setSrNo(rs.getInt("sr_no"));
				bc.setLfId(rs.getInt("lf_id"));
				bc.setBookDate(rs.getString("book_date"));
				bc.setBookAmt(rs.getInt("book_amt"));
				bc.setBookStatus(rs.getString("book_status"));
				bc.setPayStatus(rs.getString("pay_status"));
				bc.setDocId(rs.getInt("doc_id"));
				bc.setBookAppointment(rs.getString("book_appointment"));
				bc.setOrderId(rs.getString("order_id"));

				bookList.add(bc);
			}
		}

		return bookList;
	}

	@Override
	public List<LegalAdvice> getAllLaData(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		List<LegalAdvice> laList = new ArrayList<LegalAdvice>();

		String selQuery = "select * from legal_advice_table";

		ResultSet rs = null;

		PreparedStatement ps = connection.prepareStatement(selQuery);

		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				LegalAdvice la = new LegalAdvice();

				la.setAdv_id(rs.getInt("adv_id"));
				la.setUser_id(rs.getInt("user_id"));
				la.setLf_id(rs.getInt("lf_id"));
				la.setAdv_issue(rs.getString("adv_issue"));
				la.setPayment_status(rs.getString("payment_status"));
				la.setFirm_responses(rs.getString("firm_responses"));
				la.setAdv_fees(rs.getInt("adv_fees"));
				la.setOrderId(rs.getString("order_id"));
				la.setPayDate(rs.getString("pay_date"));
				laList.add(la);
			}
		}

		return laList;

	}

	@Override
	public int insertFBData(Connection connection, Feedback feedback) throws Exception {

		int insRows = 0;

		String insQuery = "insert into feedback_table(feedback,fb_date,s_id,s_type,fb_rate) values(?,?,?,?,?);";

		PreparedStatement ps = connection.prepareStatement(insQuery);

		ps.setString(1, feedback.getFeedback());
		ps.setString(2, feedback.getfDate());
		ps.setInt(3, feedback.getsId());
		ps.setString(4, feedback.getsType());
		ps.setString(5, feedback.getFrate());

		insRows = ps.executeUpdate();

		return insRows;
	}

	@Override
	public int CheckFB(Connection connection, int sId) throws Exception {

		int present = 0;

		String selQuery = "select * from feedback_table where s_id=?";
		PreparedStatement ps = connection.prepareStatement(selQuery);
		ps.setInt(1, sId);

		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) {

			while (rs.next()) {
				present = 1;
				Feedback feedback = new Feedback();

				feedback.setsId(rs.getInt("s_id"));
				feedback.setFeedback(rs.getString("feedback"));
				feedback.setsType(rs.getString("s_type"));

				System.out.println("sid " + feedback.getsId() + " feedback " + feedback.getFeedback() + " stype "
						+ feedback.getsType());

			}

			System.out.println("feedback already present");
		}

		else {
			present = 0;
		}

		return present;
	}

	@Override
	public List<Feedback> selFBData(Connection connection, int userId) throws Exception {

		List<Feedback> fbList = new ArrayList<Feedback>();

		String selQuery = "select * from legal_advice_table join feedback_table on legal_advice_table.adv_id=feedback_table.s_id where legal_advice_table.user_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);

//		ps.setString(1, "la");
		ps.setInt(1, userId);

		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				Feedback fb = new Feedback();

				fb.setfId(rs.getInt("fb_id"));
				fb.setFeedback(rs.getString("feedback"));
				fb.setfDate(rs.getString("fb_date"));
				fb.setsId(rs.getInt("s_id"));
				fb.setsType(rs.getString("s_type"));
				fb.setFrate(rs.getString("fb_rate"));

				fbList.add(fb);

			}

			System.out.println("from dao ");
			for (Feedback f : fbList) {
				System.out.println("fbid " + f.getfId() + " fb " + f.getFeedback() + " sid " + f.getsId() + " stype "
						+ f.getsType());
			}
		}
		
		else
		{
			fbList=null;
		}

		return fbList;
	}

	@Override
	public List<Feedback> selFBDataBC(Connection connection, int clientId) throws Exception {

		List<Feedback> fbList = new ArrayList<Feedback>();

		String selQuery = "select * from book_table join feedback_table on book_table.book_id=feedback_table.s_id where feedback_table.s_type=? and book_table.client_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);

		ps.setString(1, "bc");
		ps.setInt(2, clientId);

		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				Feedback fb = new Feedback();

				fb.setfId(rs.getInt("fb_id"));
				fb.setFeedback(rs.getString("feedback"));
				fb.setfDate(rs.getString("fb_date"));
				fb.setsId(rs.getInt("s_id"));
				fb.setsType(rs.getString("s_type"));
				fb.setFrate(rs.getString("fb_rate"));

				fbList.add(fb);

			}

			System.out.println("from dao ");
			for (Feedback f : fbList) {
				System.out.println("fbid " + f.getfId() + " fb " + f.getFeedback() + " sid " + f.getsId() + " stype "
						+ f.getsType());
			}
		}

		return fbList;
	}

	@Override
	public List<Feedback> selFBDataLF(Connection connection, int lfId) throws Exception {

		
		List<Feedback> fbList = new ArrayList<Feedback>();

		String selQuery = "select * from legal_advice_table join feedback_table on legal_advice_table.adv_id=feedback_table.s_id where feedback_table.s_type=? and legal_advice_table.lf_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);

		ps.setString(1, "la");
		ps.setInt(2, lfId);

		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				Feedback fb = new Feedback();

				fb.setfId(rs.getInt("fb_id"));
				fb.setFeedback(rs.getString("feedback"));
				fb.setfDate(rs.getString("fb_date"));
				fb.setsId(rs.getInt("s_id"));
				fb.setsType(rs.getString("s_type"));
				fb.setFrate(rs.getString("fb_rate"));

				fbList.add(fb);

			}

			System.out.println("from dao ");
			for (Feedback f : fbList) {
				System.out.println("fbid " + f.getfId() + " fb " + f.getFeedback() + " sid " + f.getsId() + " stype "
						+ f.getsType());
			}
		}
		
		else
		{
			fbList=null;
		}

		return fbList;

	}

	@Override
	public List<Feedback> selFBDataBCLF(Connection connection, int lfId) throws Exception {
		
		
		List<Feedback> fbList = new ArrayList<Feedback>();

		String selQuery = "select * from book_table join feedback_table on book_table.book_id=feedback_table.s_id where feedback_table.s_type=? and book_table.lf_id=?";

		PreparedStatement ps = connection.prepareStatement(selQuery);

		ps.setString(1, "bc");
		ps.setInt(2, lfId);

		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				Feedback fb = new Feedback();

				fb.setfId(rs.getInt("fb_id"));
				fb.setFeedback(rs.getString("feedback"));
				fb.setfDate(rs.getString("fb_date"));
				fb.setsId(rs.getInt("s_id"));
				fb.setsType(rs.getString("s_type"));
				fb.setFrate(rs.getString("fb_rate"));

				fbList.add(fb);

			}

			System.out.println("from dao ");
			for (Feedback f : fbList) {
				System.out.println("fbid " + f.getfId() + " fb " + f.getFeedback() + " sid " + f.getsId() + " stype "
						+ f.getsType());
			}
		}

		return fbList;
	}

	@Override
	public List<Feedback> selectAllFBData(Connection connection) throws Exception {

		List<Feedback> fbList = new ArrayList<Feedback>();

		String selQuery = "select * from feedback_table";

		PreparedStatement ps = connection.prepareStatement(selQuery);

		
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) {
			while (rs.next()) {
				Feedback fb = new Feedback();

				fb.setfId(rs.getInt("fb_id"));
				fb.setFeedback(rs.getString("feedback"));
				fb.setfDate(rs.getString("fb_date"));
				fb.setsId(rs.getInt("s_id"));
				fb.setsType(rs.getString("s_type"));
				fb.setFrate(rs.getString("fb_rate"));

				fbList.add(fb);

			}

			System.out.println("from dao ");
			for (Feedback f : fbList) {
				System.out.println("fbid " + f.getfId() + " fb " + f.getFeedback() + " sid " + f.getsId() + " stype "
						+ f.getsType());
			}
		}

		return fbList;

	}

	@Override
	public String deleteFeedback(Connection connection, int fbId) throws Exception {
		// TODO Auto-generated method stub
		String msg = "";
		String delQuery = "delete from feedback_table where fb_id=?";
		PreparedStatement ps = connection.prepareStatement(delQuery);
		ps.setInt(1, fbId);
		int delrows = ps.executeUpdate();
		System.out.println("Ans of prestat for feedback:" + delrows);
		if (delrows > 0) {
			msg = "Response Deleted Successfully.";
		} else {
			msg = "Response Deleting Failed.";
		}
		return msg;
	}

	@Override
	public String countClient(Connection connection, String role) throws Exception{
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(user_role) from user_table where user_role=? and user_status=1";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, role);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(user_role)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countLawfirm(Connection connection, String role) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(user_role) from user_table where user_role=? and user_status=1";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, role);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(user_role)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countLA(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(adv_id) from legal_advice_table";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(adv_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countBC(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(book_id) from book_table;";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(book_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
		
	}

	@Override
	public String countOnlinePayment(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String success="SUCCESS";
		String cntQuery="select count(payment_status) from legal_advice_table where payment_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, success);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(payment_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
		
		
	}

	@Override
	public String countOfflinePayment(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String aptime="At Appointment Time";
		String cntQuery="select count(payment_status) from legal_advice_table where payment_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, aptime);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(payment_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countSuccess(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String success="SUCCESS";
		String cntQuery="select count(payment_status) from legal_advice_table where payment_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, success);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(payment_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public String countFailure(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fail="FAILURE";
		String cntQuery="select count(payment_status) from legal_advice_table where payment_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fail);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(payment_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public String countOnlineBC(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String success="success";
		String cntQuery="select count(pay_status) from book_table where pay_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, success);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(pay_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
		

	}

	@Override
	public String countOfflineBC(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String of="at Firm";
		String cntQuery="select count(pay_status) from book_table where pay_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, of);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(pay_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
		
	}

	@Override
	public String countFailureBC(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fail="failure";
		String cntQuery="select count(pay_status) from book_table where pay_status=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fail);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(pay_status)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public String countVeryGoodFB(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fb="very good";
		String cntQuery="select count(fb_rate) from feedback_table where fb_rate=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fb);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(fb_rate)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

		
	}

	@Override
	public String countFeedBack(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(fb_id) from feedback_table";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(fb_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countGoodFB(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fb="good";
		String cntQuery="select count(fb_rate) from feedback_table where fb_rate=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fb);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(fb_rate)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countMediocreFB(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fb="mediocre";
		String cntQuery="select count(fb_rate) from feedback_table where fb_rate=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fb);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(fb_rate)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public String countBadFB(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fb="bad";
		String cntQuery="select count(fb_rate) from feedback_table where fb_rate=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fb);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(fb_rate)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public String countVeryBadFB(Connection connection) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String fb="very bad";
		String cntQuery="select count(fb_rate) from feedback_table where fb_rate=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setString(1, fb);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(fb_rate)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public List<String> getAllDates(Connection connection, String mon) throws Exception {
		// TODO Auto-generated method stub
		List<String> monthList = new ArrayList<String>();
		String query="select book_date from book_table";
		PreparedStatement ps = connection.prepareStatement(query);
		ResultSet rs = null;
		String m = "-"+mon+"-";
		System.out.println("M hyphen:"+m);
		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				String m1="";
				m1=rs.getString("book_date");
				if(m1!=null)
				{
					if(m1.contains(m))
					{
						monthList.add(m1);
					}
				}
			}
		}
		for(String s : monthList)
		{
				System.out.println("Book Date:"+s);
		}
		return monthList;
	}

	@Override
	public String countClientLATaken(Connection connection, int userId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(user_id) from legal_advice_table where user_id=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, userId);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(user_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countClientCases(Connection connection, int clientId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(client_id) from book_table where client_id=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, clientId);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(client_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;

	}

	@Override
	public String countLawfirmLA(Connection connection, int lfId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(lf_id) from legal_advice_table where lf_id=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, lfId);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(lf_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countLawfirmCases(Connection connection, int lfId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String cntQuery="select count(lf_id) from book_table where lf_id=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, lfId);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(lf_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}


	@Override
	public String countClientFeedbackLA(Connection connection, int uId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String legal = "la";
		String cntQuery="select count(user_id) from legal_advice_table join feedback_table where legal_advice_table.user_id=? and legal_advice_table.adv_id=feedback_table.s_id and feedback_table.s_type=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, uId);
		ps.setString(2, legal);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(user_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countClientFeedbackBC(Connection connection, int cId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String book = "bc";
		String cntQuery="select count(client_id) from book_table join feedback_table where book_table.client_id=? and book_table.book_id=feedback_table.s_id and feedback_table.s_type=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, cId);
		ps.setString(2, book);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(client_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countLawfirmFeedbackLA(Connection connection, int lfId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String legal = "la";
		String cntQuery="select count(lf_id) from legal_advice_table join feedback_table where legal_advice_table.lf_id=? and legal_advice_table.adv_id=feedback_table.s_id and feedback_table.s_type=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, lfId);
		ps.setString(2, legal);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(lf_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}

	@Override
	public String countLawfirmFeedbackBC(Connection connection, int lfId) throws Exception {
		// TODO Auto-generated method stub
		String ans1="";
		String book = "bc";
		String cntQuery="select count(lf_id) from book_table join feedback_table where book_table.lf_id=? and book_table.book_id=feedback_table.s_id and feedback_table.s_type=?";
		PreparedStatement ps = connection.prepareStatement(cntQuery);
		ps.setInt(1, lfId);
		ps.setString(2, book);
		ResultSet rs = null;

		rs = ps.executeQuery();

		if (rs != null) 
		{
			while (rs.next()) 
			{
				ans1 = rs.getString("count(lf_id)");
			}
		}
		else
		{
			ans1="Cannot find!";
		}

		return ans1;
	}


	
	

}

	

