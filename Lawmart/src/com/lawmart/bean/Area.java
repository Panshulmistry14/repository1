package com.lawmart.bean;

public class Area {
	private int areaId,areaPin;
	private String areaName;
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public int getAreaPin() {
		return areaPin;
	}
	public void setAreaPin(int areaPin) {
		this.areaPin = areaPin;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	

}
