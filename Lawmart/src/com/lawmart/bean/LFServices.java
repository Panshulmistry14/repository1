package com.lawmart.bean;

public class LFServices {
	private int srNo,lId,sId,sFees;
	private String sDesc;
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public int getlId() {
		return lId;
	}
	public void setlId(int lId) {
		this.lId = lId;
	}
	public int getsId() {
		return sId;
	}
	public void setsId(int sId) {
		this.sId = sId;
	}
	public int getsFees() {
		return sFees;
	}
	public void setsFees(int sFees) {
		this.sFees = sFees;
	}
	public String getsDesc() {
		return sDesc;
	}
	public void setsDesc(String sDesc) {
		this.sDesc = sDesc;
	}

}
