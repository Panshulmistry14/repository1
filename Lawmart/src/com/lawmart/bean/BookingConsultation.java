package com.lawmart.bean;

public class BookingConsultation {

	private int bookId,clientId,srNo,lfId,bookAmt,docId;
	private String bookDate,bookStatus,payStatus,bookAppointment,orderId;
	
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public int getLfId() {
		return lfId;
	}
	public void setLfId(int lfId) {
		this.lfId = lfId;
	}
	public int getBookAmt() {
		return bookAmt;
	}
	public void setBookAmt(int bookAmt) {
		this.bookAmt = bookAmt;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public String getBookDate() {
		return bookDate;
	}
	public void setBookDate(String bookDate) {
		this.bookDate = bookDate;
	}
	public String getBookStatus() {
		return bookStatus;
	}
	public void setBookStatus(String bookStatus) {
		this.bookStatus = bookStatus;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getBookAppointment() {
		return bookAppointment;
	}
	public void setBookAppointment(String bookAppointment) {
		this.bookAppointment = bookAppointment;
	}
	
}
