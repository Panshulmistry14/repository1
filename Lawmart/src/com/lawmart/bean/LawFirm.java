package com.lawmart.bean;

public class LawFirm extends User {
	
	private int lfId,uId;
	private String lfregNo,lfDesc;
	public int getLfId() {
		return lfId;
	}
	public void setLfId(int lfId) {
		this.lfId = lfId;
	}
	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}
	public String getLfregNo() {
		return lfregNo;
	}
	public void setLfregNo(String lfregNo) {
		this.lfregNo = lfregNo;
	}
	public String getLfDesc() {
		return lfDesc;
	}
	public void setLfDesc(String lfDesc) {
		this.lfDesc = lfDesc;
	}

}
