package com.lawmart.bean;

import com.email.durgesh.Email;

public class SendEmail extends Thread {

	public String email, subject, content;

	public SendEmail(String email, String subject, String content) {
		this.email = email;
		this.subject = subject;
		this.content = content;

	}

	public void run() {
		try {
			Email email1 = new Email("lawmartportal@gmail.com", "LawMartPortal03");
			email1.setFrom("lawmartportal@gmail.com", "LawMart");
			email1.setSubject(subject);
			email1.setContent("<h2 align='center'><strong>LAWMART</strong></h2>" + "   <h4 style='text-align:center;'>"
					+ content + "</h4>", "text/html");
			email1.addRecipient(email);
			email1.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
