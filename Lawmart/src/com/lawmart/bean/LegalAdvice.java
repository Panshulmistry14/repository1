package com.lawmart.bean;

public class LegalAdvice {
	
	private int adv_id;
	private int user_id;
	private int lf_id;
	private int adv_fees;
	private String adv_issue;
	private String payment_status;
	private String firm_responses;
	private String orderId;
	private String payDate;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public int getAdv_id() {
		return adv_id;
	}
	public void setAdv_id(int adv_id) {
		this.adv_id = adv_id;
	}
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getLf_id() {
		return lf_id;
	}
	public void setLf_id(int lf_id) {
		this.lf_id = lf_id;
	}
	public String getAdv_issue() {
		return adv_issue;
	}
	public void setAdv_issue(String adv_issue) {
		this.adv_issue = adv_issue;
	}
	public String getPayment_status() {
		return payment_status;
	}
	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}
	public String getFirm_responses() {
		return firm_responses;
	}
	public void setFirm_responses(String firm_responses) {
		this.firm_responses = firm_responses;
	}
	public Object getLA(int aId) {
		// TODO Auto-generated method stub
		return null;
	}
	public int getAdv_fees() {
		return adv_fees;
	}
	public void setAdv_fees(int adv_fees) {
		this.adv_fees = adv_fees;
	}
	
	

}
