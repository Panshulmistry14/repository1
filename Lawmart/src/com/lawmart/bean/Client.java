package com.lawmart.bean;

import java.util.Date;

public class Client extends User{
	
private int clientId;
private int userId;
private String clientGender;
private String clientDob;

public int getClientId() {
	return clientId;
}
public String getClientDob() {
	return clientDob;
}
public void setClientDob(String clientDob) {
	this.clientDob = clientDob;
}
public void setClientId(int clientId) {
	this.clientId = clientId;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public String getClientGender() {
	return clientGender;
}
public void setClientGender(String clientGender) {
	this.clientGender = clientGender;
}

}
