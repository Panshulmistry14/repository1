package com.lawmart.service.impl;

import java.awt.print.Book;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lawmart.bean.Area;
import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.Feedback;
import com.lawmart.bean.LFServices;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.Payment;
import com.lawmart.bean.Services;
import com.lawmart.bean.User;
import com.lawmart.dao.LawMartDao;
import com.lawmart.dao.impl.LawMartDaoImpl;
import com.lawmart.service.LawMartService;

public class LawMartServiceImpl implements LawMartService {

	LawMartDao ld = new LawMartDaoImpl();

	public Connection getTheConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lawmart", "root", "root");
			if (connection != null)
				System.out.println("connection successful");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;

	}

	@Override
	public List<Area> getArea() throws SQLException {

		Connection connection = getTheConnection();

		List<Area> areaList = new ArrayList<Area>();
		areaList = ld.getArea(connection);
		return areaList;
	}

	@Override
	public int putUserData(User user) throws SQLException {

		Connection connection = getTheConnection();

		int msg = ld.putSingleUserData(connection, user);

		return msg;

	}

	@Override
	public String putClientData(Client client) throws SQLException {
		// TODO Auto-generated method stub

		Connection connection = getTheConnection();
		String message = ld.putClientData(connection, client);
		return message;
	}

	@Override
	public User userLogin(String email, String pass) throws SQLException {
		User user;
		Connection connection = getTheConnection();
		user = ld.validateUser(connection, email, pass);

		return user;
	}

	@Override
	public List<Services> getServices() throws SQLException {

		Connection connection = getTheConnection();
		List<Services> servicesList = new ArrayList<Services>();
		servicesList = ld.getServices(connection);

		return servicesList;
	}

	@Override
	public LawFirm getLawFirm(int uId) throws SQLException {

		LawFirm lawFirm = null;

		Connection connection = getTheConnection();
		lawFirm = ld.getLawFirm(connection, uId);

		return lawFirm;
	}

	@Override
	public int putLF(LawFirm lawFirm) throws SQLException {

		int lId = 0;
		Connection connection = getTheConnection();
		lId = ld.putLF(connection, lawFirm);

		return lId;
	}

	@Override
	public String insertLFServices(LFServices lfServices) throws SQLException {

		Connection connection = getTheConnection();
		String msg = ld.insertLFServices(connection, lfServices);

		return msg;
	}

	@Override
	public Services fetchServices(String serviceId) throws SQLException {
		// TODO Auto-generated method stub
		Services services = new Services();
		Connection connection = getTheConnection();
		services = ld.selectServices(connection, serviceId);
		return services;
	}

	@Override
	public String modifyServices(Services services) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		msg = ld.updateServices(connection, services);
		return msg;
	}

	@Override
	public String removeServices(String serviceId) throws SQLException {
		// TODO Auto-generated method stub
		String msg = null;
		Connection connection = getTheConnection();
		int sId = Integer.parseInt(serviceId);
		msg = ld.deleteServices(connection, sId);
		return msg;
	}

	@Override
	public List<Client> getClients() throws SQLException {
		// TODO Auto-generated method stub
		List<Client> clientList = new ArrayList<Client>();
		Connection connection = getTheConnection();
		clientList = ld.getClients(connection);
		return clientList;
	}

	@Override
	public Client fetchClientDetails(String clientId) throws SQLException {
		Client client = new Client();
		Connection connection = getTheConnection();
		client = ld.selectClientDetails(connection, clientId);
		return client;
	}

	@Override
	public String updateClient(Client client) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		msg = ld.updateClientDetails(connection, client);
		return msg;
	}

	@Override
	public String deleteClient(String clientId) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		int cId = Integer.parseInt(clientId);
		System.out.println("dletecid:" + cId);
		msg = ld.deleteClientDetails(connection, cId);
		return msg;
	}

	@Override
	public List<LawFirm> getLawfirms() throws SQLException {
		// TODO Auto-generated method stub
		List<LawFirm> lfList = new ArrayList<LawFirm>();
		Connection connection = getTheConnection();
		lfList = ld.getLawfirm(connection);
		return lfList;
	}

	@Override
	public LawFirm fetchLawfirmDetails(String lawfirmId) throws SQLException {
		// TODO Auto-generated method stub
		LawFirm lawFirm = new LawFirm();
		Connection connection = getTheConnection();
		lawFirm = ld.selectLawfirmDetails(connection, lawfirmId);
		return lawFirm;
	}

	@Override
	public String updateLawfirm(LawFirm lawFirm) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		msg = ld.updateLawfirmDetails(connection, lawFirm);
		return msg;
	}

	@Override
	public String deleteLawfirm(String lawfirmId) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		int lfId = Integer.parseInt(lawfirmId);
		System.out.println("dletelfid:" + lfId);
		msg = ld.deleteLawfirmDetails(connection, lfId);
		return msg;
	}

	@Override
	public String updateAdminDetails(User user) throws SQLException {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		msg = ld.updateAdminDetails(connection, user);
		return msg;
	}

	@Override
	public String generateOtp() {

		int randomNo = (int) (Math.random() * 9000) + 1000;
		String otp = String.valueOf(randomNo);

		return otp;
	}

	@Override
	public String changePass(String email, String pass) throws SQLException {

		String msg = "";

		Connection connection = getTheConnection();
		msg = ld.changePass(connection, email, pass);

		return msg;

	}

	@Override
	public String encrypt(String text) {

		int key1 = 3;
		int key2 = -7;
//        String text="4587";

		char ch[] = text.toCharArray();

		for (int i = 0; i < ch.length; i++) {
			char ce = ch[i];
			if (i % 2 == 0)
				ce = (char) (ce + key1);
			else
				ce = (char) (ce + key2);
			System.out.print(ce);
			ch[i] = ce;
		}

		for (char c : ch) {
			System.out.print(c);
		}

		String ans = String.valueOf(ch);

		System.out.println("\nThe encryted key of " + text + " is " + ans);

		return ans;
	}

	@Override
	public String decrypt(String ans) {

		int key1 = 3;
		int key2 = -7;

		System.out.println("decryption ");

		char ch1[] = ans.toCharArray();

		for (int i = 0; i < ch1.length; i++) {
			char ce = ch1[i];
			if (i % 2 == 0)
				ce = (char) (ce - key1);
			else
				ce = (char) (ce - key2);
			System.out.print(ce);
			ch1[i] = ce;

		}

		for (char c : ch1) {
			System.out.print(c);
		}

		String ans1 = String.valueOf(ch1);

		System.out.println("\nThe decryption of " + ans + " is " + ans1);

		return ans1;
	}

	@Override
	public String putLegalAdvice(LegalAdvice la) throws SQLException {
		String msg = "";
		Connection connection = getTheConnection();
		msg = ld.insertLegalAdvice(connection, la);
		// TODO Auto-generated method stub
		return msg;
	}

	@Override
	public List<LegalAdvice> getLAData(int lfId) {

		List<LegalAdvice> laList = new ArrayList<LegalAdvice>();
		Connection connection = getTheConnection();

		try {
			laList = ld.getlaData(connection, lfId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return laList;

	}

	@Override
	public User getUser(int uId) throws SQLException {

		User user = new User();
		Connection connection = getTheConnection();
		user = ld.getUser(connection, uId);
		return user;
	}

	@Override
	public LegalAdvice getLA(int aId) throws Exception {

		LegalAdvice la = null;
		Connection connection = getTheConnection();
		la = ld.getLA(connection, aId);

		return la;
	}

	@Override
	public String putResponseData(int advId, String rspnse, int fee) throws Exception {

		String msg = null;
		Connection connection = getTheConnection();
		msg = ld.putResponseData(connection, advId, rspnse, fee);

		return msg;
	}

	@Override
	public List<LegalAdvice> getClientLAData(int userId) {

		List<LegalAdvice> laList = new ArrayList<LegalAdvice>();
		Connection connection = getTheConnection();

		try {
			laList = ld.getClientLAData(connection, userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return laList;
	}

	@Override
	public LawFirm getSingleLF(int lf) {

		LawFirm lF = null;
		Connection connection = getTheConnection();

		try {
			lF = ld.getSingleLF(connection, lf);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lF;
	}

	@Override
	public List<LFServices> getLFServices(int lfID) {

		List<LFServices> lfServices = new ArrayList<LFServices>();
		Connection connection = getTheConnection();

		try {
			lfServices = ld.getLFServices(connection, lfID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lfServices;
	}

	@Override
	public String insertOrderId(int advId, String odId) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.insertOrderId(connection, odId, advId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public String updateStatus(String odId, String status, String ordate) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.updateStatus(connection, odId, status, ordate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public String deleteResponse(int advId) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.deleteResponse(connection, advId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public String updateAppStatus(int advId) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.updateAppStatus(connection, advId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;

	}

	@Override
	public String insertPayDetails(Payment payment) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.insertPayDetails(connection, payment);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public LFServices getSingleLFService(int srno) {

		LFServices lfs = null;
		Connection connection = getTheConnection();
		try {
			lfs = ld.getSingleLFService(connection, srno);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lfs;
	}

	@Override
	public int insBookData(int cId, int srNo, int lfId, int bookAmt, int docID, String PayStatus) {
		int bookID = 0;

		Connection connection = getTheConnection();
		try {
			bookID = ld.insBookData(connection, cId, srNo, lfId, bookAmt, docID, PayStatus);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bookID;
	}

	@Override
	public List<BookingConsultation> getBookingConsuiData(int clientId) {

		List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
		Connection connection = getTheConnection();

		try {
			bcList = ld.getBookingConsuiData(connection, clientId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bcList;
	}

	@Override
	public List<BookingConsultation> getLFBookingConsuiData(int lfId) {

		List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
		Connection connection = getTheConnection();

		try {
			bcList = ld.getLFBookingConsuiData(connection, lfId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bcList;
	}

	@Override
	public Client getSingleClient(int cId) {
		Client client = null;
		Connection connection = getTheConnection();

		try {
			client = ld.getSingleClient(connection, cId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return client;
	}

	@Override
	public String insertApptime(BookingConsultation bc) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.insertAppTime(connection, bc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public BookingConsultation getBookDetails(int bookId) {
		// TODO Auto-generated method stub
		BookingConsultation bc = null;
		Connection connection = getTheConnection();
		try {
			bc = ld.getBookDetails(connection, bookId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bc;
	}

	@Override
	public String updateOrderBook(String odId, int bookId) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.updateOrderBook(connection, odId, bookId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public String insertServicePayDetails(Payment payment) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.insertServicePayDetails(connection, payment);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public String updateBookPayStatus(String status, String odId) {
		// TODO Auto-generated method stub
		String msg = "";
		Connection connection = getTheConnection();
		try {
			msg = ld.updateBookPayStatus(connection, status, odId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public Payment getPaymentDetails(String odId) {
		// TODO Auto-generated method stub
		Payment payment = new Payment();
		Connection connection = getTheConnection();
		try {
			payment = ld.selectPayDetails(connection, odId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return payment;
	}

	@Override
	public List<BookingConsultation> getAllBookData() {

		List<BookingConsultation> bookList = new ArrayList<BookingConsultation>();
		Connection connection = getTheConnection();
		try {
			bookList = ld.getAllBookData(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bookList;
	}

	@Override
	public List<LegalAdvice> getAllLaData() {
		// TODO Auto-generated method stub
		List<LegalAdvice> laList = new ArrayList<LegalAdvice>();
		Connection connection = getTheConnection();
		try {
			laList = ld.getAllLaData(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return laList;
	}

	@Override
	public int insertFBData(Feedback feedback) {
		int insRows = 0;
		Connection connection = getTheConnection();
		try {
			insRows = ld.insertFBData(connection, feedback);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return insRows;

	}

	@Override
	public int CheckFB(int sId) {
		int present = 0;
		Connection connection = getTheConnection();

		try {
			present = ld.CheckFB(connection, sId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return present;
	}

	@Override
	public List<Feedback> selFBData(int userId) {

		List<Feedback> fbList = new ArrayList<Feedback>();

		Connection connection = getTheConnection();

		try {
			fbList = ld.selFBData(connection, userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fbList;
	}

	@Override
	public List<Feedback> selFBDataBC(int clientId) {

		List<Feedback> fbList = new ArrayList<Feedback>();

		Connection connection = getTheConnection();
		try {
			fbList = ld.selFBDataBC(connection, clientId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fbList;
	}

	@Override
	public List<Feedback> selFBDataLF(int lfId) {

		List<Feedback> fbList = new ArrayList<Feedback>();

		Connection connection = getTheConnection();

		try {
			fbList = ld.selFBDataLF(connection, lfId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fbList;
	}

	@Override
	public List<Feedback> selFBDataBCLF(int lfId) {
		List<Feedback> fbList = new ArrayList<Feedback>();

		Connection connection = getTheConnection();
		try {
			fbList = ld.selFBDataBCLF(connection, lfId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fbList;
	}

	@Override
	public List<Feedback> selectAllFBData() {

		List<Feedback> fbList = new ArrayList<Feedback>();

		Connection connection = getTheConnection();
		try {
			fbList = ld.selectAllFBData(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fbList;
	}

	@Override
	public String deleteFeedback(int fbId) {
		// TODO Auto-generated method stub
		String msg="";
		Connection connection = getTheConnection();
		try {
			msg=ld.deleteFeedback(connection, fbId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public String countClients(String role) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countClient(connection, role);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countLawfirm(String role) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countLawfirm(connection, role);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countLA() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countLA(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countBC() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countBC(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countOnlinePayment() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countOnlinePayment(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countOfflinePayment() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countOfflinePayment(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countSuccess() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countSuccess(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countFailure() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countFailure(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countOnlineBC() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countOnlineBC(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countOfflineBC() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countOfflineBC(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countFailureBC() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countFailureBC(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countVeryGoodFB() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countVeryGoodFB(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;		
	}

	@Override
	public String countFeedBack() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countFeedBack(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countGoodFB() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countGoodFB(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countMediocreFB() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countMediocreFB(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countBadFB() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countBadFB(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countVeryBadFB() {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countVeryBadFB(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public List<String> getBookDates(String m) {
		// TODO Auto-generated method stub
		List<String> monthList = new ArrayList<String>();
		Connection connection = getTheConnection();
		try {
			monthList = ld.getAllDates(connection, m);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return monthList;
	}

	@Override
	public String countClientLATaken(int userId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countClientLATaken(connection, userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countClientCases(int clientId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countClientCases(connection, clientId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countLawfirmLA(int lfId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countLawfirmLA(connection, lfId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countLawfirmCases(int lfId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countLawfirmCases(connection, lfId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countClientFeedbackBC(int cId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countClientFeedbackBC(connection, cId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countClientFeedbackLA(int uId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countClientFeedbackLA(connection, uId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countLawfirmFeedbackBC(int lfId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countLawfirmFeedbackBC(connection, lfId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	@Override
	public String countLawfirmFeedbackLA(int lfId) {
		// TODO Auto-generated method stub
		String ans="";
		Connection connection = getTheConnection();
		try {
			ans = ld.countLawfirmFeedbackLA(connection, lfId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ans;
	}

	
	

	

}
