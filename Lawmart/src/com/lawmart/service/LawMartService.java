package com.lawmart.service;

import java.sql.SQLException;
import java.util.List;

import com.lawmart.bean.Area;
import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.Feedback;
import com.lawmart.bean.LFServices;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.Payment;
import com.lawmart.bean.Services;
import com.lawmart.bean.User;

public interface LawMartService {
	
	public List<Area> getArea() throws SQLException;

	public int putUserData(User user) throws SQLException;
	
	public String putClientData(Client client) throws SQLException;

	public User userLogin(String email, String pass) throws SQLException;
	
	public List<Services> getServices() throws SQLException;
	
	public LawFirm getLawFirm(int uId) throws SQLException;

	public int putLF(LawFirm lawFirm) throws SQLException;

	public Services fetchServices(String serviceId) throws SQLException;
	
	public String insertLFServices(LFServices lfServices) throws SQLException;
	
	public String modifyServices(Services services) throws SQLException; 

	public String removeServices(String serviceId) throws SQLException;

	public List<Client> getClients() throws SQLException;
	
	public Client fetchClientDetails(String clientId) throws SQLException;
	
	public String updateClient(Client client) throws SQLException;
	
	public String deleteClient(String clientId) throws SQLException;

	public List<LawFirm> getLawfirms() throws SQLException;
	
	public LawFirm fetchLawfirmDetails(String lawfirmId) throws SQLException;
	
	public String updateLawfirm(LawFirm lawFirm) throws SQLException;
	
	public String deleteLawfirm(String lawfirmId) throws SQLException;
	
	public String updateAdminDetails(User user) throws SQLException;
	
	public String generateOtp();

	public String changePass(String email, String pass) throws SQLException;
	
	public String encrypt(String text);
	
	public String decrypt(String ans);
	
	public String putLegalAdvice(LegalAdvice la) throws SQLException;

	public List<LegalAdvice> getLAData(int lfId);
	
	public User getUser(int uId) throws SQLException;

	public LegalAdvice getLA(int aId) throws Exception;

	public String putResponseData(int advId, String rspnse, int fee) throws Exception;

	public List<LegalAdvice> getClientLAData(int userId);
	
	public LawFirm getSingleLF(int lf);
	
	public List<LFServices> getLFServices(int lfID);
	
	public String updateStatus(String odId,String status,String ordate);
	
	public String insertOrderId(int advId,String odId);
	
	public String deleteResponse(int advId);
	
	public String updateAppStatus(int advId);
	
	public String insertPayDetails(Payment payment);
	
	public LFServices getSingleLFService(int srno);

	public int insBookData(int cId, int srNo, int lfId, int bookAmt, int docID, String payStatus);

	public List<BookingConsultation> getBookingConsuiData(int clientId);

	public List<BookingConsultation> getLFBookingConsuiData(int lfId);
	
	public Client getSingleClient(int cId);
	
	public String insertApptime(BookingConsultation bc);
	
	public BookingConsultation getBookDetails(int bookId);
	
	public String updateOrderBook(String odId,int bookId);
	
	public String insertServicePayDetails(Payment payment);
	
	public String updateBookPayStatus(String status,String odId);
	
	public Payment getPaymentDetails(String odId);

	public List<BookingConsultation> getAllBookData();
	
	public List<LegalAdvice> getAllLaData();

	public int insertFBData(Feedback feedback);
	
	public int CheckFB(int sId);

	public List<Feedback> selFBData(int userId);

	public List<Feedback> selFBDataBC(int clientId);

	public List<Feedback> selFBDataLF(int lfId);

	public List<Feedback> selFBDataBCLF(int lfId);

	public List<Feedback> selectAllFBData();
	
	public String deleteFeedback(int fbId);
	
	public String countClients(String role);
	
	public String countLawfirm(String role);
	
	public String countLA();
	
	public String countBC();
	
	public String countOnlinePayment();
	
	public String countOfflinePayment();
	
	public String countSuccess();
	
	public String countFailure();
	
	public String countOnlineBC();
	
	public String countOfflineBC();
	
	public String countFailureBC();
	
	public String countVeryGoodFB();
	
	public String countFeedBack();
	
	public String countGoodFB();
	
	public String countMediocreFB();
	
	public String countBadFB();
	
	public String countVeryBadFB();
	
	public List<String> getBookDates(String m);
 	
	public String countClientLATaken(int userId);
	
	public String countClientCases(int clientId);
	
	public String countLawfirmLA(int lfId);
	
	public String countLawfirmCases(int lfId);
	
	public String countClientFeedbackBC(int cId);
	
	public String countClientFeedbackLA(int uId);
	
	public String countLawfirmFeedbackBC(int lfId);
	
	public String countLawfirmFeedbackLA(int lfId);
}

