package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class LFBookingDetails
 */
public class LFBookingDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls = new LawMartServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LFBookingDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userislawfirm");

		if (user == null) {
			response.sendRedirect("Login.jsp");
		} else {

			List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();

			LawFirm lf = new LawFirm();
			String uId = String.valueOf(user.getUserId());
			try {
				lf = ls.fetchLawfirmDetails(uId);
				bcList = ls.getLFBookingConsuiData(lf.getLfId());
				request.setAttribute("bcDetails", bcList);
				RequestDispatcher dispatcher = request.getRequestDispatcher("BookingClientDetails.jsp");
				dispatcher.forward(request, response);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
