package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.LFServices;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.Services;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class getLFExtraDetails
 */
public class getLFExtraDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getLFExtraDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int lId=0;
		LawMartService ls=new LawMartServiceImpl();
		
		String regNo=request.getParameter("reg_no");
		String lfDesc=request.getParameter("lfdesc");
		
		HttpSession session=request.getSession(false);
		User user=(User)session.getAttribute("userislawfirm");
		
		System.out.println(regNo+" "+lfDesc);
		System.out.println(user.getUserId());
		
		LawFirm lawFirm=new LawFirm();
		lawFirm.setuId(user.getUserId());
		lawFirm.setLfregNo(regNo);
		lawFirm.setLfDesc(lfDesc);
		
		try {
			 lId=ls.putLF(lawFirm);
			System.out.println("LF id "+lId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String []services=request.getParameterValues("services");
		String []fees=request.getParameterValues("sfees");
		String []desc=request.getParameterValues("sdesc");


		for(String s:services)
		{
			System.out.println("Service choosed by lf "+s);
		}
		
		System.out.println(services.length+" "+fees.length+" "+desc.length);
		
		int checkIns=0;
		for(int i=0;i<services.length;i++)
		{
			System.out.println(services[i]+" "+fees[i]+" "+desc[i]);
			LFServices lfServices=new LFServices();
			lfServices.setlId(lId);
			lfServices.setsId(Integer.parseInt(services[i]));
			lfServices.setsFees(Integer.parseInt(fees[i]));
			lfServices.setsDesc(desc[i]);
			
			
			try {
				String msg=ls.insertLFServices(lfServices);
				System.out.println(msg);
				
				if(msg.equals("Final Law Firm insertion failed"))
				{
//					HttpSession lfSession=request.getSession();
//					lfSession.setAttribute("lfdetails", lawFirm);
//					
//					HttpSession lfServiceSession=request.getSession();
//					lfServiceSession.setAttribute("lfservicesdetails", lfServices);
					
					checkIns=1;
//					RequestDispatcher dispatcher=request.getRequestDispatcher("LawFirmHomePage.jsp");
//					dispatcher.forward(request, response);
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
		
		if(checkIns==0)
		{
			RequestDispatcher dispatcher=request.getRequestDispatcher("LawFirmHomePage.jsp");
			dispatcher.forward(request, response);
		}
		else if(checkIns==1)
		{
			System.out.println("Final Law Firm insertion failed");
		}
		
//		
//		for(String s:services)
//		{
//			for(String s1:fees)
//			{
//				for(String s2:desc)
//				{
//				System.out.println(s+" "+s1+" "+s2);
//				}
//				
//			}
//			
//		}
		
//		for(String s:fees)
//		{
//			System.out.println(s);
//		}
//		for(String s:desc)
//		{
//			System.out.println(s);
//		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
