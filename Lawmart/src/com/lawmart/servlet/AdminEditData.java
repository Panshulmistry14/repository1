package com.lawmart.servlet;

import java.io.IOException;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.Area;
import com.lawmart.bean.Client;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.Services;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class AdminEditData
 */
public class AdminEditData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls=new LawMartServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminEditData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String check=request.getParameter("Page1");	
		List<Area> areaList=new ArrayList<Area>();
		if(check.equals("serv"))
		{
		//Select
			String serviceId=request.getParameter("serviceId");
			System.out.println("Service id = "+serviceId);
			Services services = null;
			try {
				services = ls.fetchServices(serviceId);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("services", services);
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("UpdateService.jsp");
			requestDispatcher.forward(request, response);
		 
		}
		else if(check.equals("client"))
		{
			try {
				areaList=ls.getArea();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			request.setAttribute("areas", areaList);
			
			String clientId = request.getParameter("clientId");
			Client client = new Client();
			try {
				client=ls.fetchClientDetails(clientId);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("clients", client);
			RequestDispatcher dispatcher=request.getRequestDispatcher("UpdateClient.jsp");
			dispatcher.forward(request, response);
		}
		else if(check.equals("lawfirm"))
		{
			try {
				areaList=ls.getArea();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			request.setAttribute("areas", areaList);
			
			String lawfirmId=request.getParameter("lawfirmId");
			LawFirm lawFirm= new LawFirm();
			try {
				lawFirm=ls.fetchLawfirmDetails(lawfirmId);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("lawfirm", lawFirm);
			
			RequestDispatcher dispatcher=request.getRequestDispatcher("UpdateLawfirm.jsp");
			dispatcher.forward(request, response);
		}



		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String check=request.getParameter("Page2");
		if(check.equals("service"))
		{
			String serviceName=request.getParameter("service_name");
			String serviceDesc=request.getParameter("service_desc");
			int serviceId=Integer.parseInt(request.getParameter("service_id"));
		
			System.out.println("service Id of servllet:"+serviceId);
			Services services=new Services();
			services.setServiceName(serviceName);
			services.setServiceDesc(serviceDesc);
			services.setServiceId(serviceId);
			//System.out.println(services.getServiceId());   
			String message="";
			try {
				message=ls.modifyServices(services);
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect("AdminManage?Page1=services");
		}
		else if(check.equals("client"))
		{
			//SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
			String cId=request.getParameter("client_id");
			int clientId=Integer.parseInt(cId);
			String userName=request.getParameter("client_name");
			String uPhone=request.getParameter("phone_number");
			Long userPhone=Long.parseLong(uPhone);
			String userPassword=request.getParameter("password");
			String userEmail=request.getParameter("email");
			String userAddress=request.getParameter("address");
			String area=request.getParameter("area");
			int areaId=Integer.parseInt(area);
			String clientDOB=request.getParameter("birth_date");
			String clientGender=request.getParameter("gender");

//			Date date11 = null;
			String clientDOB1="";
//			java.sql.Date dat = null;
//			try {
//				 date11 = sdf.parse(clientDOB);
//				 dat=new java.sql.Date(date11.getTime());
//				 clientDOB1=sdf.format(dat);			
//			} catch (ParseException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}  
		    DateFormat format = new SimpleDateFormat("mm - dd - yyyy", Locale.ENGLISH);
		    try {
				 Date date1 = format.parse(clientDOB);
				 System.out.println("Date1;"+date1);
				 clientDOB1 = new SimpleDateFormat("MM-dd-yyyy").format(date1);
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		    System.out.println("DOB:"+clientDOB);
//		    try {
//				date1 = new SimpleDateFormat("MM-dd-yyyy").parse(clientDOB);
//			} catch (ParseException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}   
		    //
		    System.out.println("final format ClientDOB1:"+clientDOB1);
			
			//System.out.println("date11:"+date11);
			//System.out.println("clientDOB1:"+clientDOB1);
			System.out.println("gender:"+clientGender);
			
			Client client = new Client();
			client.setUserId(clientId);
			client.setUserName(userName);
			client.setUserPhone(userPhone);
			client.setUserPassword(userPassword);
			client.setUserEmail(userEmail);
			client.setUserAddress(userAddress);
			client.setAreaId(areaId);
			client.setClientDob(clientDOB1);
			client.setClientGender(clientGender);
			String msg="";
			
			try {
				msg=ls.updateClient(client);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect("AdminManage?Page1=client");
		}
		else if(check.equals("lawfirm"))
		{
			String lfId=request.getParameter("lawfirm_id");
			int lawfirmId=Integer.parseInt(lfId);
			String userName=request.getParameter("firm_name");
			String uPhone=request.getParameter("phone_number");
			Long userPhone=Long.parseLong(uPhone);
			String userPassword=request.getParameter("password");
			String userEmail=request.getParameter("email");
			String userAddress=request.getParameter("address");
			String area=request.getParameter("area");
			int areaId=Integer.parseInt(area);
			String lfregNo=request.getParameter("reg_no");
			String lfDesc=request.getParameter("lfdesc");
			
			LawFirm lawFirm = new LawFirm();
			lawFirm.setUserId(lawfirmId);
			lawFirm.setUserName(userName);
			lawFirm.setUserPhone(userPhone);
			lawFirm.setUserPassword(userPassword);
			lawFirm.setUserEmail(userEmail);
			lawFirm.setUserAddress(userAddress);
			lawFirm.setAreaId(areaId);
			lawFirm.setLfregNo(lfregNo);
			lawFirm.setLfDesc(lfDesc);
			String msg="";
			
			try {
				msg=ls.updateLawfirm(lawFirm);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect("AdminManage?Page1=lawfirm");
		}
	}

}
