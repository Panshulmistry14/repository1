package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetBookingConsultationdata
 */
public class GetBookingConsultationdata extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetBookingConsultationdata() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userisclient");

		if (user == null) {
			response.sendRedirect("Login.jsp");
		} else {

			LawMartService ls = new LawMartServiceImpl();
			String uId = String.valueOf(user.getUserId());
			Client client = new Client();
			try {
				client = ls.fetchClientDetails(uId);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
			bcList = ls.getBookingConsuiData(client.getClientId());

			if (bcList != null) {
				for (BookingConsultation bc : bcList) {
					System.out.println(
							bc.getBookId() + " " + bc.getClientId() + " " + bc.getLfId() + " " + bc.getBookAmt());
				}

				request.setAttribute("bcdata", bcList);

				String str = (String) request.getAttribute("rply1");

				if (str != null)
					request.setAttribute("rply2", str);

				RequestDispatcher dispatcher = request.getRequestDispatcher("BookingConsultation.jsp");

				dispatcher.forward(request, response);
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
