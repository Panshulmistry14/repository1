package com.lawmart.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class CheckEmail
 */
public class CheckEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckEmail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
System.out.println("Server called");
		
		String str=request.getParameter("email");
		
		System.out.println("Email id :- "+str);
		
		
		
		////
		
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lawmart", "root", "root");
			if (connection != null)
			{
				System.out.println("connection successful");
				
				String query="select * from user_table where user_email = '"+str+"'";
				
				PreparedStatement preparedStatement = connection.prepareStatement(query);
				
				ResultSet resultSet=preparedStatement.executeQuery();
				
				String stdemail=null;
				
				while(resultSet.next())
				{
					User user=new User();
					stdemail=resultSet.getString("user_email");
					
					user.setUserEmail(stdemail);
				}
				if(str.equalsIgnoreCase(stdemail)) {
					
					response.getWriter().append("true");			
				}
				else
				{
					response.getWriter().append("false");
				}

				
			}
			else
			{
				System.out.println("connection failed");
			}
			

		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		
		////
		
	

	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
