package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.email.durgesh.Email;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.SendEmail;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class UserLogin
 */
public class UserLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String email=request.getParameter("email");
		String pass=request.getParameter("pass");
		System.out.println("entered details "+email+" "+pass);
		User user=new User();
		
		LawMartService ls=new LawMartServiceImpl();
		try {
			user=ls.userLogin(email,pass);
			
			
			if(user!=null)
			{
//					response.getWriter().append("welcome User");
				System.out.println(user.getUserEmail());
				
				
//				try {
//					Email email1=new Email("lawmartportal@gmail.com", "LawMartPortal03");
//					email1.setFrom("lawmartportal@gmail.com", "LawMart");
//					email1.setSubject("Login successful");
//					email1.setContent("<p>Welcome "+user.getUserName()+"! You are successfully logged in to the LawMart system.</p>", "text/html");
//					email1.addRecipient(user.getUserEmail());
//					email1.send();
//					}
//					catch(Exception e)
//					{
//						e.printStackTrace();
//					}
				
				String emailPass=user.getUserEmail();
				String subject="Login successful";
				String content="Welcome "+user.getUserName()+"! You are successfully logged in to the LawMart system.";
				
				
				SendEmail se=new SendEmail(emailPass,subject,content);
				se.start();
					
				
					if(user.getUserRole().equals("Client"))
					{
//						response.getWriter().append("welcome Client");
						HttpSession httpSession=request.getSession();
						httpSession.setAttribute("userisclient", user);
						httpSession.setAttribute("loginBean", user);

						RequestDispatcher dispatcher=request.getRequestDispatcher("ClientHomePage.jsp");
						dispatcher.forward(request, response);
						
					}
					else if(user.getUserRole().equals("Law Firm"))
					{
						response.getWriter().append("welcome Law Firm");
						LawFirm lawFirm=ls.getLawFirm(user.getUserId());
						
						HttpSession session=request.getSession();
						session.setAttribute("userislawfirm", user);
						session.setAttribute("loginBean", user);
						
						if(lawFirm==null)
						{
//							response.getWriter().append("extra details ");
//							request.setAttribute("user", user);
							
							RequestDispatcher dispatcher=request.getRequestDispatcher("getServices");
							dispatcher.forward(request, response);
							
						}
						else
						{
							response.getWriter().append("home page of law firm");
							RequestDispatcher dispatcher=request.getRequestDispatcher("LawFirmHomePage.jsp");
							dispatcher.forward(request, response);
							
						}
//						httpSession.setAttribute("userislawfirm", user);
//						RequestDispatcher dispatcher=request.getRequestDispatcher("ClientHomePage.jsp");
//						dispatcher.forward(request, response);
					}
					else if(user.getUserRole().equals("Admin"))
					{
						response.getWriter().append("Welcome Admin");
						HttpSession httpSession=request.getSession();
						httpSession.setAttribute("userisadmin", user);
						httpSession.setAttribute("loginBean", user);

						RequestDispatcher dispatcher=request.getRequestDispatcher("AdminHomePage.jsp");
						dispatcher.forward(request, response);
						
					}
					else
					{
						response.getWriter().append("Unable to find the proper role for the user:) ");
						
					}
			}
			else
			{
//					response.getWriter().append("invalid user details");
					request.setAttribute("invalidlogin", "invalid user details");
					RequestDispatcher dispatcher=request.getRequestDispatcher("Login.jsp");
					dispatcher.forward(request, response);
			}
			
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
