package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.User;
import com.lawmart.dao.LawMartDao;
import com.lawmart.dao.impl.LawMartDaoImpl;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetLAData
 */
public class GetLAData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetLAData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userislawfirm");

		if (user == null) {
			response.sendRedirect("Login.jsp");
		} else {

			LawFirm lawFirm = new LawFirm();
			LawMartService ls = new LawMartServiceImpl();
			List<LegalAdvice> laList = new ArrayList<LegalAdvice>();

			try {
				lawFirm = ls.getLawFirm(user.getUserId());
				System.out.println("the law firm id is " + lawFirm.getLfId());

				laList = ls.getLAData(lawFirm.getLfId());

				if (laList != null) {

					for (LegalAdvice la : laList) {
						System.out.println("adv id " + la.getAdv_id() + " user id " + la.getUser_id() + " lf id "
								+ la.getLf_id() + " issue " + la.getAdv_issue() + " payment " + la.getPayment_status());

					}
				} else {
					System.out.println("lalist is null");

				}
				request.setAttribute("ladata", laList);
				RequestDispatcher dispatcher = request.getRequestDispatcher("LawFirmLA.jsp");
				dispatcher.forward(request, response);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
