package com.lawmart.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class RemoveFeedback
 */
public class RemoveFeedback extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls = new LawMartServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveFeedback() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String fId=request.getParameter("fbId");
		int fbId=Integer.parseInt(fId);
		String delfb="";
		delfb=ls.deleteFeedback(fbId);
		response.sendRedirect("AdminManage?Page1=feedback");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
