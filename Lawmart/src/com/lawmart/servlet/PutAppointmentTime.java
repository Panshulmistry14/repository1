package com.lawmart.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class PutAppointmentTime
 */
public class PutAppointmentTime extends HttpServlet {
	private static final long serialVersionUID = 1L;
       LawMartService ls = new LawMartServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutAppointmentTime() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String appTime=request.getParameter("appoint_time");
		String bId=request.getParameter("bookid");
		System.out.println("Book id in servlet is:"+bId);
		if(appTime!=null && bId!=null)
		{
				int bookId=Integer.parseInt(bId);
				
				BookingConsultation bc = new BookingConsultation();
				bc.setBookId(bookId);
				bc.setBookAppointment(appTime);
				
				String msg=ls.insertApptime(bc);
				if(msg!=null)
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("GetLFBookingConsultationData");

					dispatcher.forward(request, response);
				}
		}
	}

}
