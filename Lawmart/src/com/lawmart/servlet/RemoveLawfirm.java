package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class RemoveLawfirm
 */
public class RemoveLawfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls=new LawMartServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveLawfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String lawfirmId=request.getParameter("lawfirmId");
		try {
			String delmsg=ls.deleteLawfirm(lawfirmId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect("AdminManage?Page1=lawfirm");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
