package com.lawmart.servlet;

import java.io.IOException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.Feedback;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.Services;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class AdminManage
 */
public class AdminManage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls = new LawMartServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminManage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String check = request.getParameter("Page1");
		if (check.equals("client")) {

			// response.getWriter().append("manage client");
			// call the ManageClient.jsp
			List<Client> clientList = new ArrayList<Client>();
			try {
				clientList = ls.getClients();
				request.setAttribute("clients", clientList);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("ManageClient.jsp");
			dispatcher.forward(request, response);

		} else if (check.equals("lawfirm")) {
			List<LawFirm> lfList = new ArrayList<LawFirm>();
			try {
				lfList = ls.getLawfirms();
				request.setAttribute("lawfirm", lfList);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher("ManageLawfirm.jsp");
			dispatcher.forward(request, response);

		} else if (check.equals("services")) {
			// For Manage Services
			List<Services> servicesList = new ArrayList<Services>();
			try {
				servicesList = ls.getServices();
				request.setAttribute("Services", servicesList);

				RequestDispatcher dispatcher = request.getRequestDispatcher("ManageService.jsp");
				dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if (check.equals("book")) {

			List<BookingConsultation> bookList = new ArrayList<BookingConsultation>();
			
				bookList = ls.getAllBookData();
				if(bookList!=null)
				{
				request.setAttribute("book", bookList);
				
				for(BookingConsultation b: bookList)
				{
					System.out.println("bid "+b.getBookId()+" cid "+b.getClientId()+" lfid "+b.getLfId());
				}
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("ViewBookAdmin.jsp");
				dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("booking list is null");
				}

		} else if (check.equals("la")) {

			List<LegalAdvice> laList = new ArrayList<LegalAdvice>();
			laList = ls.getAllLaData();
			if(laList!=null)
			{
				request.setAttribute("laDataAdmin", laList);
				for(LegalAdvice la : laList)
				{
					System.out.println(la.getAdv_issue());
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("ViewLaAdmin.jsp");
				dispatcher.forward(request, response);
			}
			else
			{
				System.out.println("Legal Advice List is NULL.");
			}
			
			
		}
		else if(check.equals("feedback"))
		{
			List<Feedback> fbList=new ArrayList<Feedback>();
			fbList = ls.selectAllFBData();
			if(fbList!=null)
			{
				request.setAttribute("feedback", fbList);
				RequestDispatcher dispatcher = request.getRequestDispatcher("ManageFeedback.jsp");
				dispatcher.forward(request, response);
						
			}
			else
			{
				System.out.println("FeedBack List is NULL");
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
