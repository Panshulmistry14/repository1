package com.lawmart.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.Feedback;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetFeedbackData
 */
public class GetFeedbackData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFeedbackData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LawMartService ls=new LawMartServiceImpl();
		String sId=request.getParameter("sid");
		String ratingChoice=request.getParameter("feedback");
		String feedMsg=request.getParameter("feedmsg");
		String sType=request.getParameter("stype");
		
		int bId=Integer.parseInt(sId);
		
		System.out.println("booking id "+sId+" rating "+ratingChoice+" feedback "+feedMsg+" stype "+sType);
		
		
		Feedback feedback=new Feedback();
		
		feedback.setFeedback(feedMsg);
		feedback.setfDate(java.time.LocalDate.now().toString());
		feedback.setsId(bId);
		feedback.setsType(sType);
		feedback.setFrate(ratingChoice);
		
		int insRows=0;
		insRows=ls.insertFBData(feedback);
		
		if(insRows>0)
		{
			System.out.println("Feedback insertion success");
			
			if(sType.equals("bc"))
			{
			response.sendRedirect("GetBookingConsultationdata");
			}
			else if(sType.equals("la"))
			{
				response.sendRedirect("ClientLAData");

			}
		}
		else
		{
			System.out.println("Feedback insertion failed");
			response.getWriter().append("<h1>Your Feedback not recorded properly!Please try again</h1>");
		}
		
		
		
	}

}
