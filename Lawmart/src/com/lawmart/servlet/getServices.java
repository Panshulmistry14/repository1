package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.Services;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class getServices
 */
public class getServices extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getServices() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LawMartService ls=new LawMartServiceImpl();
		List<Services> servicesList=new ArrayList<Services>();
		
		try {
			servicesList=ls.getServices();
			for(Services s:servicesList)
			{
				System.out.println(s.getServiceId()+" "+s.getServiceName()+" "+s.getServiceDesc());
			}
//			User user=(User)request.getAttribute("user");
//			request.setAttribute("user", user);
			request.setAttribute("services",servicesList);
			RequestDispatcher dispatcher=request.getRequestDispatcher("ExtraDetailslf.jsp");
			dispatcher.forward(request, response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
