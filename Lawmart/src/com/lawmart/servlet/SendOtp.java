package com.lawmart.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.email.durgesh.Email;
import com.lawmart.bean.SendEmail;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class SendOtp
 */
public class SendOtp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendOtp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    LawMartService ls=new LawMartServiceImpl();
	String otp;
	String email;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		String o=request.getParameter("checkotp");
		String originalOtp=request.getParameter("originalotp");
		
		String finalOtp=ls.decrypt(originalOtp);
//		String email=request.getParameter("email");
		System.out.println("from do get"+email);
		if(o.equals(finalOtp))
		{
			System.out.println("Otp matched");
			//request.setAttribute("msg", "otp verified successfully!");
			request.setAttribute("email", email);
			RequestDispatcher dispatcher=request.getRequestDispatcher("ChangePassword.jsp");
			dispatcher.forward(request, response);


		}
		else
		{
			System.out.println("Otp does not match");
			request.setAttribute("msg", "otp verification failed!");
			RequestDispatcher dispatcher=request.getRequestDispatcher("VerifyOtp.jsp");
			dispatcher.forward(request, response);

		}
		
		
		
		

	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		otp=ls.generateOtp();
		email=request.getParameter("email");
		System.out.println("email from forgot password servlet "+email);
		
		System.out.println("otp is "+otp);
		
//		try {
//			Email email1=new Email("lawmartportal@gmail.com", "LawMartPortal03");
//			email1.setFrom("lawmartportal@gmail.com", "LawMart");
//			email1.setSubject("Forgot Password");
//			email1.setContent("<p>Your OTP is "+otp+"</p>", "text/html");
//			email1.addRecipient(email);
//			email1.send();
//			
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
		
			SendEmail se=new SendEmail(email,"Forgot Password","Your OTP for resetting the password is "+otp);
	
			se.start();
			
			request.setAttribute("otp", otp);
			request.setAttribute("email", email);
			RequestDispatcher dispatcher=request.getRequestDispatcher("VerifyOtp.jsp");
			dispatcher.forward(request, response);

			
	}

}
