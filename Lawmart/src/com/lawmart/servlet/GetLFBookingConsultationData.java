package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.Client;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetLFBookingConsultationData
 */
public class GetLFBookingConsultationData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetLFBookingConsultationData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userislawfirm");

		if (user == null) {
			response.sendRedirect("Login.jsp");
		} else {

			LawMartService ls = new LawMartServiceImpl();
			System.out.println("uid "+user.getUserId());
			LawFirm lawFirm = new LawFirm();
			try {
				
				lawFirm=ls.getLawFirm(user.getUserId());
				System.out.println("the law firm id is "+lawFirm.getLfId());
				
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}

			List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
			System.out.println("lfId "+lawFirm.getLfId());
			bcList = ls.getLFBookingConsuiData(lawFirm.getLfId());

			if (bcList != null) {
				for (BookingConsultation bc : bcList) {
					System.out.println(
							bc.getBookId() + " " + bc.getClientId() + " " + bc.getLfId() + " " + bc.getBookAmt());
				}

				request.setAttribute("lfbc", bcList);

				RequestDispatcher dispatcher = request.getRequestDispatcher("LFBookingConsultation.jsp");

				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
