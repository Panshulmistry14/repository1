package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GiveAppointmentTime
 */
public class GiveAppointmentTime extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls = new LawMartServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GiveAppointmentTime() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		/*HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userislawfirm");

		if (user == null) {
			response.sendRedirect("Login.jsp");
		} else {

			
			System.out.println("uid "+user.getUserId());
			LawFirm lawFirm = new LawFirm();
			try {
				
				lawFirm=ls.getLawFirm(user.getUserId());
				System.out.println("the law firm id is "+lawFirm.getLfId());
				
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}

			List<BookingConsultation> bcList = new ArrayList<BookingConsultation>();
			System.out.println("lfId "+lawFirm.getLfId());
			bcList = ls.getLFBookingConsuiData(lawFirm.getLfId());

			if (bcList != null) {
				for (BookingConsultation bc : bcList) {
					System.out.println(
							bc.getBookId() + " " + bc.getClientId() + " " + bc.getLfId() + " " + bc.getBookAmt());
				}

				request.setAttribute("lfbookconsult", bcList);*/
				String bId=request.getParameter("bookId");
				if(bId!=null)
				{
					int bookId=Integer.parseInt(bId);
					BookingConsultation bc = new BookingConsultation();
					
					bc=ls.getBookDetails(bookId);
					if(bc!=null)
					{
						
						request.setAttribute("bookconsult", bc);
						
						RequestDispatcher dispatcher = request.getRequestDispatcher("LFAppointmentTimeResponse.jsp");

						dispatcher.forward(request, response);
					}
				}
				
				
				
				
				
				
				
				

				
		//	}
		//}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
