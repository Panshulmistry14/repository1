package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.Client;
import com.lawmart.bean.Feedback;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class SelectFeedbackData
 */
public class SelectFeedbackData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectFeedbackData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LawMartService ls=new LawMartServiceImpl();
		List<Feedback> fbListLA=new ArrayList<Feedback>();
		List<Feedback> fbListBC=new ArrayList<Feedback>();

		String fbUser=request.getParameter("fbuser");
		if(fbUser.equals("client"))
		{
			System.out.println("Client's Feedback");
			
			
			HttpSession httpSession=request.getSession(false);
			  User user=(User)httpSession.getAttribute("userisclient");	
			  
			if(user==null)
			{ 
				response.sendRedirect("Login.jsp");
			}
			else
			{
				fbListLA=ls.selFBData(user.getUserId());
				
				String clientId=String.valueOf(user.getUserId());
				try {
					
					
					Client client= ls.fetchClientDetails(clientId);
					fbListBC=ls.selFBDataBC(client.getClientId());
					
					request.setAttribute("laclientfb",fbListLA);
					request.setAttribute("laclientbc",fbListBC);
					
					RequestDispatcher dispatcher=request.getRequestDispatcher("ShowFeedBackData.jsp?fbuser=client");
					dispatcher.forward(request, response);

					
					
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			}

			
			
		}
		
		
		else if(fbUser.equals("lawfirm"))
		{
			System.out.println("LawFirm's Feedback");
			
			
			HttpSession httpSession=request.getSession(false);
			  User user=(User)httpSession.getAttribute("userislawfirm");	
			  
			if(user==null)
			{ 
				response.sendRedirect("Login.jsp");
			}
			else
			{
				LawFirm lf=new LawFirm();
				String lawfirmId=String.valueOf(user.getUserId());
				try {
					lf=ls.fetchLawfirmDetails(lawfirmId);
					
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				fbListLA=ls.selFBDataLF(lf.getLfId());
				
				try {
					
					
					fbListBC=ls.selFBDataBCLF(lf.getLfId());
					
					request.setAttribute("laclientfb",fbListLA);
					request.setAttribute("laclientbc",fbListBC);
					
					RequestDispatcher dispatcher=request.getRequestDispatcher("ShowFeedBackData.jsp?fbuser=lawfirm");
					dispatcher.forward(request, response);

					
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			}

			
			
		}else if(fbUser.equals("visitor"))
		{
			List<Feedback> fbList=new ArrayList<Feedback>();
			fbList= ls.selectAllFBData();
			
			if(fbList!=null)
			{
				request.setAttribute("fbvisitor",fbList);
				
				RequestDispatcher dispatcher=request.getRequestDispatcher("ShowFeedBackData.jsp?fbuser=visitor");
				dispatcher.forward(request, response);

				
				
			}
			else
			{
				System.out.println("fblist for visitor is null");
			}
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
