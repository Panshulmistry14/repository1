package com.lawmart.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.email.durgesh.Email;
import com.lawmart.bean.SendEmail;

/**
 * Servlet implementation class GetContactData
 */
public class GetContactData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetContactData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name=request.getParameter("name");
		String mail=request.getParameter("email");
		String sub=request.getParameter("subject");
		String msg=request.getParameter("message");
		System.out.println(name+" "+mail+" "+sub+" "+msg);
		
		
//		try {
//			Email email1=new Email("lawmartportal@gmail.com", "LawMartPortal03");
//			email1.setFrom("lawmartportal@gmail.com", "LawMart");
//			email1.setSubject(sub);
//			email1.setContent("<h4>This person is trying to contact you through the lawmart system<br>name:"+name+"<br>message:"+msg+"<br>mail:"+mail+"</h4>", "text/html");
//			email1.addRecipient("shailjdave72000@gmail.com");
//			email1.send();
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
		
		String content="This person is trying to contact you through the lawmart system<br>name:"+name+"<br>message:"+msg+"<br>mail:"+mail;
		
		
		SendEmail se=new SendEmail("shailjdave72000@gmail.com",sub,content);
		se.start();
		
		
		request.setAttribute("msg", "Your contact request is sent successfully to HIREN RAVAL & ASSOCIATES!They will revert back to you as soon as possible");
		RequestDispatcher dispatcher=request.getRequestDispatcher("Contact.jsp");
		dispatcher.forward(request, response);
	}

}
