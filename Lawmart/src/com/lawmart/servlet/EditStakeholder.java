package com.lawmart.servlet;

import java.awt.Label;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.Area;
import com.lawmart.bean.Client;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class EditStakeholder
 */
public class EditStakeholder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls = new LawMartServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditStakeholder() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());

		List<Area> areaList = new ArrayList<Area>();
		String check = request.getParameter("Page1");
		if (check.equals("client")) {
			HttpSession httpSession = request.getSession(false);
			User user = (User) httpSession.getAttribute("userisclient");
			if (user == null) {
				response.sendRedirect("Login.jsp");
			} else {

				System.out.println("editsk userID:" + user.getUserId());
				System.out.println("editsk user name:" + user.getUserName());
				int cId = user.getUserId();

				try {
					areaList = ls.getArea();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				request.setAttribute("areas", areaList);

				String clientId = String.valueOf(cId);
				System.out.println("clientID editsk:" + clientId);
				Client client = new Client();
				try {
					client = ls.fetchClientDetails(clientId);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("edit stakeholder client bean " + client.getUserId() + " " + client.getAreaId() + " "
						+ client.getUserName());

				request.setAttribute("clients", client);
				RequestDispatcher dispatcher = request.getRequestDispatcher("ClientProfile.jsp");
				dispatcher.forward(request, response);
			}
		} else if (check.equals("lawfirm")) {
			HttpSession httpSession = request.getSession(false);
			User user = (User) httpSession.getAttribute("userislawfirm");
			
			if (user == null) {
				response.sendRedirect("Login.jsp");
			} else {
			
			System.out.println("editsk lawfirm userID:" + user.getUserId());
			System.out.println("editsk lawfirm user name:" + user.getUserName());
			int lfId = user.getUserId();

			try {
				areaList = ls.getArea();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			request.setAttribute("areas", areaList);

			String lawfirmId = String.valueOf(lfId);
			LawFirm lawFirm = new LawFirm();
			try {
				lawFirm = ls.fetchLawfirmDetails(lawfirmId);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("edit stakeholder lawfirm bean " + lawFirm.getUserId() + " " + lawFirm.getAreaId() + " "
					+ lawFirm.getUserName());

			request.setAttribute("lawfirm", lawFirm);
			RequestDispatcher dispatcher = request.getRequestDispatcher("LawfirmProfile.jsp");
			dispatcher.forward(request, response);
			}
		} else if (check.equals("admin")) {
			HttpSession httpSession = request.getSession(false);
			User user = (User) httpSession.getAttribute("userisadmin");
			System.out.println("admin stakeholder elseif called");
			if (user == null) {
				response.sendRedirect("Login.jsp");
			} else {

				try {
					areaList = ls.getArea();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				request.setAttribute("areas", areaList);
				request.setAttribute("user", user);

				RequestDispatcher dispatcher = request.getRequestDispatcher("AdminProfile.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		String check = request.getParameter("Page1");
		if (check.equals("client")) {
			String cId = request.getParameter("client_id");
			int clientId = Integer.parseInt(cId);
			String fname = request.getParameter("first_name");
			String lname = request.getParameter("last_name");
			String userName = fname + " " + lname;
			String uPhone = request.getParameter("phone");
			Long userPhone = Long.parseLong(uPhone);
			String userPassword = request.getParameter("password1");
			String userEmail = request.getParameter("email");
			String userAddress = request.getParameter("address");
			String area = request.getParameter("areapin");
			int areaId = Integer.parseInt(area);
			String clientDOB = request.getParameter("birth_date");
			String clientGender = request.getParameter("gender");

			System.out.println("userid:" + clientId);
			System.out.println("Fullname editsk:" + userName);
			System.out.println("userphone:" + userPhone);
			System.out.println("pass:" + userPassword);
			System.out.println("email:" + userEmail);
			System.out.println("address:" + userAddress);
			System.out.println("areaid:" + areaId);
			System.out.println("gender:" + clientGender);

			Client client = new Client();
			client.setUserId(clientId);
			client.setUserName(userName);
			client.setUserPhone(userPhone);
			client.setUserPassword(userPassword);
			client.setUserEmail(userEmail);
			client.setUserAddress(userAddress);
			client.setAreaId(areaId);

			String dateStore = clientDOB;
			String clientDOB1 = "";
			DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
			try {
				Date date1 = format.parse(dateStore);
				System.out.println("Date1;" + date1);
				clientDOB1 = new SimpleDateFormat("MM-dd-yyyy").format(date1);
				System.out.println("From editsk Clientdob1:" + clientDOB1);
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			client.setClientDob(clientDOB1);
			client.setClientGender(clientGender);
			String msg = "";

			try {
				msg = ls.updateClient(client);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect("EditStakeholder?Page1=client");
		} else if (check.equals("lawfirm")) {
			String lfId = request.getParameter("lawfirm_id");
			int lawfirmId = Integer.parseInt(lfId);
			String userName = request.getParameter("firm_name");
			String uPhone = request.getParameter("phone");
			Long userPhone = Long.parseLong(uPhone);
			String userPassword = request.getParameter("password1");
			String userEmail = request.getParameter("email");
			String userAddress = request.getParameter("address");
			String area = request.getParameter("areapin");
			int areaId = Integer.parseInt(area);
			String lfregNo = request.getParameter("regno");
			String lfDesc = request.getParameter("lf_desc");

			LawFirm lawFirm = new LawFirm();
			lawFirm.setUserId(lawfirmId);
			lawFirm.setUserName(userName);
			lawFirm.setUserPhone(userPhone);
			lawFirm.setUserPassword(userPassword);
			lawFirm.setUserEmail(userEmail);
			lawFirm.setUserAddress(userAddress);
			lawFirm.setAreaId(areaId);
			lawFirm.setLfregNo(lfregNo);
			lawFirm.setLfDesc(lfDesc);
			String msg = "";

			try {
				msg = ls.updateLawfirm(lawFirm);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect("EditStakeholder?Page1=lawfirm");
		} else if (check.equals("admin")) {
			String aId = request.getParameter("admin_id");
			int adminId = Integer.parseInt(aId);
			String fname = request.getParameter("first_name");
			String lname = request.getParameter("last_name");
			String userName = fname + " " + lname;
			String uPhone = request.getParameter("phone");
			Long userPhone = Long.parseLong(uPhone);
			String userPassword = request.getParameter("password1");
			String userEmail = request.getParameter("email");
			String userAddress = request.getParameter("address");
			String area = request.getParameter("areapin");
			int areaId = Integer.parseInt(area);

			System.out.println("userid1:" + adminId);
			System.out.println("Fullname1 editsk:" + userName);
			System.out.println("userphone1:" + userPhone);
			System.out.println("pass1:" + userPassword);
			System.out.println("email1:" + userEmail);
			System.out.println("address1:" + userAddress);
			System.out.println("areaid1:" + areaId);

			User user = new User();
			user.setUserId(adminId);
			user.setUserName(userName);
			user.setUserPhone(userPhone);
			user.setUserPassword(userPassword);
			user.setUserEmail(userEmail);
			user.setUserAddress(userAddress);
			user.setAreaId(areaId);
			String msg = "";
			try {
				msg = ls.updateAdminDetails(user);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			HttpSession httpSession = request.getSession(false);
			User userSession = (User) httpSession.getAttribute("userisadmin");
			userSession.setUserName(userName);
			userSession.setUserPhone(userPhone);
			userSession.setUserPassword(userPassword);
			userSession.setUserEmail(userEmail);
			userSession.setUserAddress(userAddress);
			userSession.setAreaId(areaId);

			response.sendRedirect("EditStakeholder?Page1=admin");
		}
	}

}
