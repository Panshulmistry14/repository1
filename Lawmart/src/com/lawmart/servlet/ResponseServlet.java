package com.lawmart.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class ResponseServlet
 */
public class ResponseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      LawMartService ls= new LawMartServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResponseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
//		HttpSession httpSession=request.getSession(false);
//		User user=(User)httpSession.getAttribute("userisclient");
		
		String adviceId=request.getParameter("advId");
		int advId=Integer.parseInt(adviceId);
		System.out.println("Advice Id:"+advId);
		LegalAdvice la=null;
		try {
			la = ls.getLA(advId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		List<LegalAdvice> laList=new ArrayList<LegalAdvice>();
//		laList= ls.getClientLAData(user.getUserId());
		
		request.setAttribute("legaladvice", la);
		
//		request.setAttribute("ladvdata", laList);
		
		
		RequestDispatcher dispatcher=request.getRequestDispatcher("ResponseClientDetails.jsp");
		dispatcher.forward(request, response);

		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
