package com.lawmart.servlet;

import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.email.durgesh.Email;
import com.lawmart.bean.Client;
import com.lawmart.bean.Encryption;
import com.lawmart.bean.SendEmail;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class ClientRegistration
 */
public class ClientRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls=new LawMartServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientRegistration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Client reg ");
//		response.setContentType("text/html;charset=UTF-8");
//		String areaName=request.getParameter("area");
//		System.out.println("The name of the area is "+areaName);
		
		String firstName=request.getParameter("first_name");
		String lastName=request.getParameter("last_name");
		StringBuffer name=new StringBuffer(firstName);
		name.append(" ");
		name.append(lastName);
		String fullname=name.toString();
		String clientDob=request.getParameter("birth_date");
		String clientGender=request.getParameter("gender");
		String phoneNo=request.getParameter("phone_number");
		long clientPhoneNumber=Long.parseLong(phoneNo);
		String clientPass=request.getParameter("password");
		String clientEmail=request.getParameter("email");
		String clientAddress=request.getParameter("address");
		String area=request.getParameter("area");
		int areaId=Integer.parseInt(area);
		
		System.out.println("Full name is:="+fullname);
		System.out.println("Client area:="+areaId);
		System.out.println("date1:="+clientDob);
		System.out.println("Client address:="+clientAddress);
		System.out.println("Client email:="+clientEmail);
		System.out.println("Client password :="+clientPass);
		System.out.println("Client phone no:="+clientPhoneNumber);
		System.out.println("Client gender is:="+clientGender);
		System.out.println("First name is:="+firstName);
		System.out.println("Last Name is:="+lastName);
		System.out.println("String buffer name is:="+name);
		
		int uId=0;
		String clientRegMsg=null;
		
		User user=new User();
		user.setUserName(fullname);
		user.setUserPhone(clientPhoneNumber);
		
		String encryptedPass=Encryption.encode(clientPass);
		
		user.setUserPassword(encryptedPass);
		
		user.setUserRole("Client");
		user.setUserIsAdmin(false);
		user.setAreaId(areaId);
		user.setUserEmail(clientEmail);
		user.setUserAddress(clientAddress);
		user.setUserStatus(1);

		
		try {
			uId=ls.putUserData(user);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		if(uId!=0)
		{
			Client client = new Client();
			client.setClientDob(clientDob);
			client.setClientGender(clientGender);
			client.setUserId(uId);
			try {
				clientRegMsg=ls.putClientData(client);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(uId!=0 && clientRegMsg!=null)
		{
//			try {
//				Email email1=new Email("lawmartportal@gmail.com", "LawMartPortal03");
//				email1.setFrom("lawmartportal@gmail.com", "LawMart");
//				email1.setSubject("Registration successful");
//				email1.setContent("<p>Welcome "+fullname+"! You are successfully registered into the LawMart system.</p>", "text/html");
//				email1.addRecipient(clientEmail);
//				email1.send();
//				}
//				catch(Exception e)
//				{
//					e.printStackTrace();
//				}
			
			
			String emailPass=clientEmail;
			String subject="Registration successful";
			String content="Welcome "+fullname+"! You are successfully registered into the LawMart system.";
			
			
			SendEmail se=new SendEmail(emailPass,subject,content);
			se.start();


		}
//		response.getWriter().append(clientRegMsg);
		response.sendRedirect("Login.jsp");

		
				

	}

}
