package com.lawmart.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class PutResponseData
 */
public class PutResponseData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutResponseData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LawMartService ls=new LawMartServiceImpl();
		String aId=request.getParameter("advid");
		int advId=Integer.parseInt(aId);
		
		String name=request.getParameter("name");
		String phn=request.getParameter("phone_number");
		String issue=request.getParameter("issue");
		String rspnse=request.getParameter("response");
		String fees=request.getParameter("fees");
		int fee=Integer.parseInt(fees);
		
		System.out.println(name+" "+advId+" "+phn+" "+issue+" "+rspnse+" "+fees);
		
		try {
			String msg=ls.putResponseData(advId,rspnse,fee);
			
			if(msg.equals("response inserted successfully"))
			{
				System.out.println("response inserted !");
				response.sendRedirect("GetLAData");
			}
			else if(msg.equals("response insertion failed"))
			{
				System.out.println("response insertion failed!");
			}
			else
			{
				System.out.println("msg returned null");
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
