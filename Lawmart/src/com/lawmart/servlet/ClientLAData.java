package com.lawmart.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lawmart.bean.Client;
import com.lawmart.bean.LawFirm;
import com.lawmart.bean.LegalAdvice;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class ClientLAData
 */
public class ClientLAData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ClientLAData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userisclient");

		if (user == null) {
			response.sendRedirect("Login.jsp");
		} else {

			Client client = new Client();
			LawMartService ls = new LawMartServiceImpl();
			List<LegalAdvice> laList = new ArrayList<LegalAdvice>();

			try {

				System.out.println("The user id is " + user.getUserId());

				laList = ls.getClientLAData(user.getUserId());

				if (laList != null) {

					for (LegalAdvice la : laList) {
						System.out.println("adv id " + la.getAdv_id() + " user id " + la.getUser_id() + " lf id "
								+ la.getLf_id() + " issue " + la.getAdv_issue() + " payment " + la.getPayment_status()
								+ " firm response " + la.getFirm_responses() + " fees " + la.getAdv_fees());

					}
				} else {
					System.out.println("lalist is null");

				}
				request.setAttribute("ladata", laList);
				RequestDispatcher dispatcher = request.getRequestDispatcher("ClientResponse.jsp");
				dispatcher.forward(request, response);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
