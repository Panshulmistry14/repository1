package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.email.durgesh.Email;
import com.lawmart.bean.Encryption;
import com.lawmart.bean.SendEmail;
import com.lawmart.bean.User;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetLawFirmReg
 */
public class GetLawFirmReg extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetLawFirmReg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		LawMartService ls=new LawMartServiceImpl();
		String firmName=request.getParameter("firm_name");
		
		String area=request.getParameter("area");
		int areaId=Integer.parseInt(area);
		
		String pNo=request.getParameter("phone_number");
		long phNo=Long.parseLong(pNo);
		
		String pass=request.getParameter("password");
		String email=request.getParameter("email");
		String address=request.getParameter("address");
		
		
		User user=new User();
		
		user.setUserName(firmName);
		user.setUserPhone(phNo);
		user.setAreaId(areaId);
		
		String encryptedPass=Encryption.encode(pass);
		
		user.setUserPassword(encryptedPass);		
		
		
		user.setUserEmail(email);
		user.setUserAddress(address);
		user.setUserRole("Law Firm");
		user.setUserIsAdmin(false);
		user.setUserStatus(1);
		
		
//		System.out.println(user.getUserName()+" "+user.getUserPhone()+" "+user.getAreaId()+" "+user.getUserPassword()+
//				" "+user.getUserEmail()+" "+user.getUserAddress()+" "+user.getUserRole()+" "+user.getUserIsAdmin());
		
		int uId=0;
		try {
			uId = ls.putUserData(user);
			System.out.println(uId);

			if(uId!=0)
			{
				
//				try {
//					Email email1=new Email("lawmartportal@gmail.com", "LawMartPortal03");
//					email1.setFrom("lawmartportal@gmail.com", "LawMart");
//					email1.setSubject("Registration successful");
//					email1.setContent("<p>Welcome "+firmName+"! You are successfully registered into the LawMart system.Now you need to login to the system</p>", "text/html");
//					email1.addRecipient(email);
//					email1.send();
//					}
//					catch(Exception e)
//					{
//						e.printStackTrace();
//					}
				
				String emailPass=email;
				String subject="Registration successful";
				String content="Welcome "+firmName+"! You are successfully registered into the LawMart system.Now you need to login to the system";
				
				
				SendEmail se=new SendEmail(emailPass,subject,content);
				se.start();

				
				response.sendRedirect("Login.jsp");

				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
