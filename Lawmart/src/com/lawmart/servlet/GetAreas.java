package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.Area;
import com.lawmart.dao.LawMartDao;
import com.lawmart.dao.impl.LawMartDaoImpl;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetAreas
 */
public class GetAreas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAreas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LawMartService ls=new LawMartServiceImpl();
		List<Area> areaList=new ArrayList<Area>();
		
		try {
			areaList=ls.getArea();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
//		for(Area a:areaList)
//		{
//			System.out.println(a.getAreaId()+" "+a.getAreaName()+" "+ a.getAreaPin());
//		}
		
		String clientPage=request.getParameter("Page1");
		String lawFirmPage=request.getParameter("Page2");
		
		System.out.println(lawFirmPage+" "+clientPage);
		
		if(clientPage==null && lawFirmPage!=null)
		{
			request.setAttribute("areas", areaList);
			RequestDispatcher dispatcher=request.getRequestDispatcher("LawFirmSignUp.jsp");
			dispatcher.forward(request, response);			
		}
		else if(clientPage!=null && lawFirmPage==null)
		{
			request.setAttribute("areas", areaList);
			RequestDispatcher dispatcher=request.getRequestDispatcher("ClientSignUp.jsp");
			dispatcher.forward(request, response);			
			
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
