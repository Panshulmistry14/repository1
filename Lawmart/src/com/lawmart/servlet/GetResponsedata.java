package com.lawmart.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.LegalAdvice;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class GetResponsedata
 */
public class GetResponsedata extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetResponsedata() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		LawMartService ls=new LawMartServiceImpl();
		String advId= request.getParameter("LAadvId");
		System.out.println("advice id is "+advId);
		LegalAdvice la=null;
		
		if(advId!=null)
		{
			int aId=Integer.parseInt(advId);
			try {
				la=ls.getLA(aId);
				
				System.out.println(la.getAdv_id()+" "+la.getAdv_issue()+" "+la.getPayment_status());
				
				request.setAttribute("laresponsedata", la);
				
				RequestDispatcher dispatcher=request.getRequestDispatcher("LAResponse.jsp");
				dispatcher.forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		else
		{
			System.out.println("advice id is null!");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
