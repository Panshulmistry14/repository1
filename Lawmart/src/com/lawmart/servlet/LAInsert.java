package com.lawmart.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lawmart.bean.LegalAdvice;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;

/**
 * Servlet implementation class LAInsert
 */
public class LAInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LawMartService ls=new LawMartServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LAInsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String userId=request.getParameter("user_id");
		int uId=Integer.parseInt(userId);
		String lawfirmId=request.getParameter("lawfirmselction");
		int lfId=Integer.parseInt(lawfirmId);
		String advIssue=request.getParameter("issue");
		String payStatus="pending";
		
		System.out.println("userId:"+uId);
		System.out.println("laId:"+lfId);
		System.out.println("adv Issue:"+advIssue);
		System.out.println("pay Status:"+payStatus);
		
		LegalAdvice la=new LegalAdvice();
		la.setUser_id(uId);
		la.setLf_id(lfId);
		la.setAdv_issue(advIssue);
		la.setPayment_status(payStatus);
		
		String msg="";
		try {
			msg=ls.putLegalAdvice(la);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String status="";
		if(msg.equals("legal advice success"))
		{
			status="Your request for legal advice is successfully registered!";
			
		}
		else if(msg.equals("legal advice failed"))
		{
			status="Something went wrong please try again!";
		}
		
		request.setAttribute("lastatus", status);
		RequestDispatcher dispatcher=request.getRequestDispatcher("LegalAdvice.jsp");
		dispatcher.forward(request, response);
		
		
		response.getWriter().append(msg);
		
	}

}
