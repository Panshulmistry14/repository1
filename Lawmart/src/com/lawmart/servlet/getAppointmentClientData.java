package com.lawmart.servlet;

import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.lawmart.bean.BookingConsultation;
import com.lawmart.service.LawMartService;
import com.lawmart.service.impl.LawMartServiceImpl;
import com.mysql.jdbc.Statement;

@MultipartConfig(maxFileSize = 209999999)
//@WebServlet("/FileUpload")
//@MultipartConfig

/**
 * Servlet implementation class getAppointmentClientData
 */
public class getAppointmentClientData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public getAppointmentClientData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");

		LawMartService ls = new LawMartServiceImpl();

		String cid = request.getParameter("cid");
		String srno = request.getParameter("srno");
		String lfid = request.getParameter("lfid");
		String bookamt = request.getParameter("bookamt");
		String payMode=request.getParameter("payment");

		int cId = Integer.parseInt(cid);
		int srNo = Integer.parseInt(srno);
		int lfId = Integer.parseInt(lfid);
		int bookAmt = Integer.parseInt(bookamt);
		int docID = 0;

		System.out.println("cid " + cId + " srno " + srNo + " ifid " + lfId + " bookamt " + bookAmt+" pay mode "+payMode);

		/////////////

		final Part filePart = request.getPart("file");

		InputStream pdfFileBytes = null;
		//final PrintWriter writer = response.getWriter();

		try {

			if (!filePart.getContentType().equals("application/pdf")) {
				
				request.setAttribute("error"," Invalid File! Please Upload the Documents in the form of a PDF File only.");
				RequestDispatcher dispatcher=request.getRequestDispatcher("ClientServicesPage.jsp");
				dispatcher.forward(request, response);
				System.out.println("<br/> Invalid File");
				return;
			}

			else if (filePart.getSize() > 5048576) { // 5mb
				{
					String msg=" File size is more than 5MB!";
					request.setAttribute("error",msg);
					RequestDispatcher dispatcher=request.getRequestDispatcher("ClientServicesPage.jsp");
					dispatcher.forward(request, response);
					System.out.println("<br/> File size too big");
					return;
				}
			}

			pdfFileBytes = filePart.getInputStream(); // to get the body of the request as binary data

			final byte[] bytes = new byte[pdfFileBytes.available()];
			pdfFileBytes.read(bytes); // Storing the binary data in bytes array.

			///

			Connection con = null;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			try {
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/lawmart", "root", "root");
				if (con != null)
					System.out.println("connection successful");

			} catch (SQLException e) {
				e.printStackTrace();
			}

			///

			java.sql.Statement stmt = null;

			int success = 0;

			PreparedStatement pstmt = con.prepareStatement(
					"INSERT INTO documents_table(client_id,documents) VALUES(?,?)",
					java.sql.Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, cId);
			pstmt.setBytes(2, bytes); // Storing binary data in blob field.

			success = pstmt.executeUpdate();

			ResultSet resultSet = null;
			resultSet = pstmt.getGeneratedKeys();

			if (resultSet != null) {
				while (resultSet.next()) {
					docID = resultSet.getInt(1);
				}
			}
			System.out.println("Doc id we got is " + docID);
			if (success >= 1)
				System.out.println("pdf Stored");
			con.close();

			System.out.println("<br/> Document Uploaded Successfully ");

		} catch (FileNotFoundException fnf) {
			System.out.println("You  did not specify a file to upload");
			System.out.println("<br/> ERROR: " + fnf.getMessage());

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (pdfFileBytes != null) {
				pdfFileBytes.close();
			}
//			if (writer != null) {
//				writer.close();
//			}
		}

		if(docID==0)
		{
			request.setAttribute("error","Something went Wrong. Please try again!");
			RequestDispatcher dispatcher=request.getRequestDispatcher("ClientServicesPage.jsp");
			dispatcher.forward(request, response);
			System.out.println("Insertion of document not done properly.Unable to get the doc id!");
			System.out.println("Something went Wrong!");
		}
		else
		{
			int bookId=0;
			String payStatus="";
			
			if(payMode.equals("Pay now"))
			{
				payStatus="pending";	
			}
			else if(payMode.equals("Pay Offline"))
			{
				payStatus="at Firm";
			}
			
			bookId=ls.insBookData(cId,srNo,lfId,bookAmt,docID,payStatus);
			System.out.println("Payment mode of servlet is:"+payMode);
			if(bookId!=0)
			{
				System.out.println("the bookid we got is "+bookId);
				
				if(payMode.equals("Pay Offline"))
				{
					String msg="Your Request for Appointment is SuccessFully Registered!";
					request.setAttribute("rply1","Your Request for Appointment is SuccessFully Registered!");
					RequestDispatcher dispatcher=request.getRequestDispatcher("GetBookingConsultationdata");
					dispatcher.forward(request, response);
					
					System.out.println("Your Request for Appointment is SuccessFully Registered!");
					System.out.println("pay mode at firm appointment successfully registered");
					
				}
				else if(payMode.equals("Pay now"))
				{
					//Payment redirect here
					BookingConsultation bc = new BookingConsultation();
					bc=ls.getBookDetails(bookId);
					if(bc!=null)
					{
						request.setAttribute("bookservice", bc);
						RequestDispatcher dispatcher = request.getRequestDispatcher("servicePaymentCheckout.jsp");
						dispatcher.forward(request, response);
					}
				}
				
			}
			else if(bookId==0)
			{
				String msg="something went Wrong!Please try again";
				request.setAttribute("error",msg);
				RequestDispatcher dispatcher=request.getRequestDispatcher("ClientServicesPage.jsp");
				dispatcher.forward(request, response);
			}
		}
	}
}
